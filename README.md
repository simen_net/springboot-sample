# springboot-sample

#### 介绍

springboot简单示例 [跳转到发行版](../../tree/5.使用JWT进行授权认证.md) [查看发行版说明](./doc/md/5.使用JWT进行授权认证.md)

#### 软件架构（当前发行版使用）

1. springboot
2. hutool-all 非常好的常用java工具库 [官网](https://www.hutool.cn/) [maven](https://mvnrepository.com/artifact/cn.hutool/hutool-all)
3. bcprov-jdk18on 一些加密算法的实现 [官网](https://www.bouncycastle.org/java.html) [maven](https://mvnrepository.com/artifact/org.bouncycastle/bcpkix-jdk18on)
4. h2 纯java的数据库，支持内存、文件、网络数据库 [官网](   https://h2database.com ) [maven](https://mvnrepository.com/artifact/com.h2database/h2)

#### 安装教程

    git clone --branch 6.使用加密数据源并配置日志 git@gitee.com:simen_net/springboot-sample.git

#### 功能说明

1. 配置logback
2. 配置了数据源，对数据库账号密码进行加密
3. 增加了系统启动配置

#### 发行版说明

1. 完成基本WEB服务 [跳转到发行版](../../tree/1.基本WEB服务)
2. 完成了KEY初始化功能和全局错误处理 [跳转到发行版](../../tree/2.KEY初始化功能和全局错误处理)
3. 完成了基本登录验证 [跳转到发行版](../../tree/3.基本登录验证)
4. 完成了自定义加密进行登录验证 [跳转到发行版](../../tree/4.自定义加密进行登录验证)
5. 完成了使用JWT进行授权认证 [跳转到发行版](../../tree/5.使用JWT进行授权认证)
6. 完成了使用加密数据源并配置日志 [跳转到发行版](../../tree/6.使用加密数据源并配置日志) [查看发行版说明](./doc/md/6.使用加密数据源并配置日志.md)