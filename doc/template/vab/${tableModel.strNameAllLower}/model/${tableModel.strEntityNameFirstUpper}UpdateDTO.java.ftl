package ${strPackage}.model;

import lombok.*;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.strong.utils.annotation.StrongRemark;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

import static ${strPackage}.${tableModel.strEntityNameFirstUpper}Constants.*;
import static com.strong.utils.constants.MessageConstants.*;

/**
 * ${tableModel.strComment?default("")} 修改模型类
 *
 * @author ${strAuthor}
 * @date ${.now?string("yyyy-MM-dd HH:mm:ss")}
 */
@Data
public class ${tableModel.strEntityNameFirstUpper}UpdateDTO{

    /**
    *
    <pre>
        *     @Null 被注释的元素必须为null
        *     @NotNull 被注释的元素不能为null
        *     @AssertTrue 被注释的元素必须为true
        *     @AssertFalse 被注释的元素必须为false
        *     @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
        *     @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
        *     @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
        *     @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
        *     @Size(max,min) 被注释的元素的大小必须在指定的范围内。
        *     @Digits(integer,fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
        *     @Past 被注释的元素必须是一个过去的日期
        *     @Future 被注释的元素必须是一个将来的日期
        *     @Pattern(value) 被注释的元素必须符合指定的正则表达式。
        *     @Email 被注释的元素必须是电子邮件地址
        *     @Length 被注释的字符串的大小必须在指定的范围内
        *     @NotEmpty 被注释的字符串必须非空
        *     @Range 被注释的元素必须在合适的范围内
        * </pre>
    */

<#-- 遍历表模型中的字段队里生成字段实体 -->
<#list tableModel.listColumnModel as columnModel>

        /**
        * ${columnModel.strComment?default("")} 字段
        */
    <#-- 根据isNotNull增加注解 -->
        <#if columnModel.isNotNull>
            @NotNull(message = ERROR_NOT_NULL)
        </#if>
    <#-- 根据字段日期类型，增加Temporal注解 -->
        <#if columnModel.strType?upper_case == "DATE">
            @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
        <#elseif columnModel.strType?upper_case == "DATETIME">
            @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
        <#elseif columnModel.strType?upper_case == "TIME">
            @JsonFormat(pattern = DatePattern.NORM_TIME_PATTERN)
        </#if>
    <#-- 根据intLength增加长度限制注解 -->
        <#if columnModel.classEntity.simpleName == 'String' && columnModel.intLength gt 0>
            @Size(message = ERROR_SIZE <#if columnModel.isNotNull>,min = 1</#if>, max = ${columnModel.intLength?c})
        <#else>
        </#if>
        @StrongRemark(${columnModel.strConstantsName}_COMMENT)
        private ${columnModel.classEntity.simpleName} ${columnModel.strEntityName};
</#list>

    // ===================扩展=======================

}