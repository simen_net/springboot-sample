package ${strPackage}.model;

import ${strPackage}.${tableModel.strEntityNameFirstUpper}Constants;
import ${strPackage}.jpa.${tableModel.strEntityNameFirstUpper}DO;
import com.strong.utils.CodeUtils;
import com.strong.utils.StrongUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ${tableModel.strComment?default("")} 显示模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ${tableModel.strEntityNameFirstUpper}VO extends ${tableModel.strEntityNameFirstUpper}AO {

    /**
     * 序列化包含的字段数组
     */
    public final static String[] STRS_INCLUDE_PROPERTIES = {
    <#-- 遍历表模型中的字段队里生成常量 -->
    <#list tableModel.listColumnModel as columnModel>
        // ${columnModel.strComment?default("")} 字段实体名
        ${tableModel.strEntityNameFirstUpper}Constants.${columnModel.strConstantsName}_ENTITY,
    </#list>
    };

    /**
     * 自动生成序列化不包含的字段数组
     */
    public final static String[] STRS_EXCLUSION_PROPERTIES = StrongUtils.getExclusionFieldNames(${tableModel.strEntityNameFirstUpper}VO.class);

    /**
     * 以实体类实例化
     *
     * @param ${tableModel.strEntityName}DO 实体类
     */
    public ${tableModel.strEntityNameFirstUpper}VO(final ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO) {
        this(${tableModel.strEntityName}DO, STRS_INCLUDE_PROPERTIES);
    }

    /**
     * 以实体类实例化 仅包含strsIncludeProperties的属性
     *
     * @param ${tableModel.strEntityName}DO           实体类
     * @param strsIncludeProperties 包含的字段数组
     */
    public ${tableModel.strEntityNameFirstUpper}VO(final ${tableModel.strEntityNameFirstUpper}DO ${tableModel.strEntityName}DO, final String[] strsIncludeProperties) {
        StrongUtils.copyProperties(${tableModel.strEntityName}DO, this, strsIncludeProperties);
    }

    /**
     * 主入口
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        String strClassName = Thread.currentThread().getStackTrace()[1].getClassName();
        CodeUtils.printTsModelCode(strClassName, STRS_INCLUDE_PROPERTIES);
    }

    // ===================扩展=======================

}
