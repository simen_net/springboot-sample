package ${strPackage}.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import ${strPackage}.${tableModel.strEntityNameFirstUpper}Constants;
import ${strPackage}.jpa.${tableModel.strEntityNameFirstUpper}DAO;
import ${strPackage}.jpa.${tableModel.strEntityNameFirstUpper}DO;
import ${strPackage}.jpa.${tableModel.strEntityNameFirstUpper}DO_;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}CreateDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}RetrieveDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}UpdateDTO;
import ${strPackage}.model.${tableModel.strEntityNameFirstUpper}VO;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.jpa.service.StrongServiceImpl;
import com.strong.utils.message.StrongMessageSource;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.strong.utils.jpa.JpaConstants.*;

/**
 * ${tableModel.strComment?default("")} 数据处理Service实现类
 *
 * @author Simen
 * @date 2022-02-07 20:11:23
 */
@Slf4j
@Service
@CacheConfig(cacheNames = {${tableModel.strEntityNameFirstUpper}Constants.TABLE_ENTITY})
public class ${tableModel.strEntityNameFirstUpper}ServiceImpl extends StrongServiceImpl<${tableModel.strEntityNameFirstUpper}DO, ${tableModel.strEntityNameFirstUpper}VO> implements ${tableModel.strEntityNameFirstUpper}Service {

    /**
     * 注入的消息处理类
     */
    private final StrongMessageSource messageSource;

    /**
     * 注入的DAO类
     */
    private final ${tableModel.strEntityNameFirstUpper}DAO ${tableModel.strEntityName}DAO;

    /**
     * 实例化
     *
     * @param messageSource 注入的消息处理类
     * @param ${tableModel.strEntityName}DAO  注入的DAO类
     */
    @Autowired
    public ${tableModel.strEntityNameFirstUpper}ServiceImpl(StrongMessageSource messageSource, ${tableModel.strEntityNameFirstUpper}DAO ${tableModel.strEntityName}DAO) {
        this.messageSource = messageSource;
        this.${tableModel.strEntityName}DAO = ${tableModel.strEntityName}DAO;
    }

    @Override
    @Cacheable(key = "#root.target + '_' + #root.methodName + '_' + #intId")
    public ${tableModel.strEntityNameFirstUpper}DO getRecord(final Integer intId) {
        log.debug(intId.toString());
        Assert.notNull(intId, MSG_QUERY_ID_EMPTY);
        return ${tableModel.strEntityName}DAO.getReferenceById(intId);
    }

    @Override
    @CacheEvict(allEntries = true)
    public ${tableModel.strEntityNameFirstUpper}DO getCreateAction(final ${tableModel.strEntityNameFirstUpper}CreateDTO modelCreate) {
        log.debug(JSON.toJSONString(modelCreate));
        Assert.notNull(modelCreate, MSG_CONTENT_EMPTY);

        ${tableModel.strEntityNameFirstUpper}DO entity = BeanUtil.toBean(modelCreate, ${tableModel.strEntityNameFirstUpper}DO.class, CopyOptions.create().setIgnoreNullValue(true));
<#-- 遍历表模型中的字段队里生成字段实体 -->
<#list tableModel.listColumnModel as columnModel>
    <#if columnModel.isPrimary>
        // 初始化实体类ID
        entity.set${columnModel.strEntityNameFirstUpper}(StrongUtils.getNoRepeatId(${tableModel.strEntityName}DAO::getCountById));
    </#if>
</#list>
        ${tableModel.strEntityName}DAO.save(entity);
        return entity;
    }

    @Override
    @Cacheable(keyGenerator = "vabKeyGenerator")
    public List<${tableModel.strEntityNameFirstUpper}DO> getAllList(final ${tableModel.strEntityNameFirstUpper}RetrieveDTO modelRetrieve) {
        log.debug(JSON.toJSONString(modelRetrieve));
        Assert.notNull(modelRetrieve, MSG_SEARCH_CONDITION_EMPTY);

        // 以表达式生成一个查询条件对象
        Specification<${tableModel.strEntityNameFirstUpper}DO> specification = getSpecification(modelRetrieve);
        return ${tableModel.strEntityName}DAO.findAll(specification, modelRetrieve.getSort());
    }

    @Override
    @Cacheable(keyGenerator = "vabKeyGenerator")
    public Page<${tableModel.strEntityNameFirstUpper}DO> getPageList(final ${tableModel.strEntityNameFirstUpper}RetrieveDTO modelRetrieve) {
        log.debug(JSON.toJSONString(modelRetrieve, true));
        Assert.notNull(modelRetrieve, MSG_SEARCH_CONDITION_EMPTY);

        // 以表达式生成一个查询条件对象
        Specification<${tableModel.strEntityNameFirstUpper}DO> specification = getSpecification(modelRetrieve);
        return ${tableModel.strEntityName}DAO.findAll(specification, modelRetrieve.getPageable());
    }

    @Override
    @CacheEvict(allEntries = true)
    public ${tableModel.strEntityNameFirstUpper}DO getUpdateAction(final ${tableModel.strEntityNameFirstUpper}UpdateDTO modelUpdate) {
        log.debug(JSON.toJSONString(modelUpdate));
        Assert.notNull(modelUpdate, MSG_CONTENT_EMPTY);

<#-- 遍历表模型中的字段队里生成字段实体 -->
<#list tableModel.listColumnModel as columnModel>
    <#if columnModel.isPrimary>
        // 通过id字段获取待修改的实体类（id字段不存在，需修改为对应字段）
        ${tableModel.strEntityNameFirstUpper}DO entity = ${tableModel.strEntityName}DAO.getReferenceById(modelUpdate.get${columnModel.strEntityNameFirstUpper}());
    </#if>
</#list>
        Assert.notNull(entity, MSG_RECORD_NONENTITY);
        BeanUtil.copyProperties(modelUpdate, entity, CopyOptions.create().setIgnoreNullValue(true));
        ${tableModel.strEntityName}DAO.save(entity);
        return entity;
    }

    @Override
    @Cacheable(key = "#root.target + '_' + #root.methodName + '_' + #intId")
    public ${tableModel.strEntityNameFirstUpper}DO getUpdateRecord(final Integer intId) {
        log.debug(intId.toString());
        Assert.notNull(intId, MSG_QUERY_ID_EMPTY);
        return ${tableModel.strEntityName}DAO.getReferenceById(intId);
    }

    @Override
    @CacheEvict(allEntries = true)
    public Integer[] getDeleteAction(final Integer... intsId) {
        log.debug(ArrayUtil.toString(intsId));
        Assert.notEmpty(intsId, MSG_DELETE_RECORD_EMPTY);

        int[] intsTemp = new int[intsId.length];
        for (int i = 0; i < intsId.length; i++) {
            Assert.notNull(intsId[i], MSG_DELETE_RECORD_EMPTY);
            intsTemp[i] = intsId[i];
        }

        Integer intCount = ${tableModel.strEntityName}DAO.getCountByIdIn(intsTemp);
        Assert.isTrue(intCount == intsId.length, MSG_DELETE_RECORD_BEEN_NULL);

        ${tableModel.strEntityName}DAO.deleteAllByIdInBatch(Arrays.asList(intsId));
        return intsId;
    }

    // ===================内部=======================

    /**
     * 获取查询范式
     *
     * @return {@link Specification}<{@link ${tableModel.strEntityNameFirstUpper}DO}>
     */
    protected Specification<${tableModel.strEntityNameFirstUpper}DO> getSpecification(final ${tableModel.strEntityNameFirstUpper}RetrieveDTO modelRetrieve) {
        return (root, query, criteriaBuilder) -> {
//            Map<String, String> mapSearch = modelRetrieve.getMapSearch();
//            if (MapUtil.isNotEmpty(mapSearch)) {
//                List<Predicate> listPredicate = new ArrayList<>();
//
//                String strLike = MapUtil.getStr(mapSearch, ${tableModel.strEntityNameFirstUpper}Constants.TEST_STR_ENTITY);
//                if (StrUtil.isNotBlank(strLike)) {
//                    listPredicate.add(criteriaBuilder.like(root.get(${tableModel.strEntityNameFirstUpper}DO_.testStr), strLike));
//                }
//
//                return criteriaBuilder.and(ArrayUtil.toArray(listPredicate, Predicate.class));
//            }
            return null;
        };
    }

    @Override
    @CacheEvict(value = ${tableModel.strEntityNameFirstUpper}Constants.TABLE_ENTITY, allEntries = true)
    public void cacheEvict() {

    }

    // ===================扩展=======================
}