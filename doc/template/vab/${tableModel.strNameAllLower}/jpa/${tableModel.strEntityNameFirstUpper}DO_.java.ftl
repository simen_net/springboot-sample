package ${strPackage}.jpa;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.util.Date;
import java.math.BigDecimal;

/**
* ${tableModel.strComment?default("")} 托管类
*
* @author ${strAuthor}
* @date ${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
@StaticMetamodel(${tableModel.strEntityNameFirstUpper}DO.class)
public class ${tableModel.strEntityNameFirstUpper}DO_ {
<#-- 遍历表模型中的字段队里生成字段实体 -->
<#list tableModel.listColumnModel as columnModel>
    /**
    * ${columnModel.strComment?default("")}
    */
    public static volatile SingularAttribute<${tableModel.strEntityNameFirstUpper}DO, ${columnModel.classEntity.simpleName}> ${columnModel.strEntityName};

</#list>
}