<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>登录ftl页面</title>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="/js/base64.js"></script>
</head>
<body>
<table>
    <tr>
        <td style="vertical-align: top">
            Ajax登录操作<br/>
            <label for="username_simen">登录名</label>
            <input type="text" id="username_simen" name="username_simen" value="simen"/>&nbsp;
            <label for="password_simen">密 码</label>
            <input type="password" id="password_simen" name="password_simen" value="123456"/>&nbsp;
            <input type="button" id="button_simen" value="Ajax登录simen read权限"/><br/>

            <label for="username_admin">登录名</label>
            <input type="text" id="username_admin" name="username_admin" value="admin"/>&nbsp;
            <label for="password_admin">密 码</label>
            <input type="password" id="password_admin" name="password_admin" value="123456"/>&nbsp;
            <input type="button" id="button_admin" value="Ajax登录admin write权限"/><br/>

            <hr/>
            <label for="token">获取到的JWT Token</label>
            <textarea id="token" name="token" rows="5" style="width: 100%;" readonly></textarea>
            <input type="button" id="button_getPersonJsonRead" value="Ajax访问 getPersonJsonRead"/>
            <input type="button" id="button_getPersonJsonWrite" value="Ajax访问 getPersonJsonWrite"/>
            <input type="button" id="button_getPersonJsonError" value="Ajax访问 getPersonJsonError"/>
            <input type="button" id="button_logout" value="Ajax访问 logout 退出登录"/>
        </td>
        <td style="vertical-align: top;padding-left: 20px">
            <label for="console">Ajax返回消息</label><br/>
            <textarea id="console" rows="50" cols="100"></textarea>
        </td>
    </tr>
</table>
</body>
<script type="text/javascript">
    function postLogin(user) {
        $.ajax({
            url: "/login",
            method: "POST",
            data: {
                username: $("#username_" + user).val(),
                password: $("#password_" + user).val()
            }
        }).done((data) => {
            $("#console").val(JSON.stringify(data, null, "    "));
            if (data && data.data && data.data.token) {
                $("#token").val(data.data.token);
                const strDecode = Base64.decode(data.data.token.split(".")[1]);
                $("#console").val(
                    $("#console").val() +
                    "\n=========获取到的JWT Token解码=========\n" +
                    JSON.stringify(JSON.parse(strDecode), null, "    ")
                )
            } else {
                $("#token").val("");
            }
        });
    }


    // function getPerson(url) {
    //     $.ajax({
    //         url: url,
    //         method: "POST",
    //         headers: {
    //             Authorization: "Bearer " + $("#token").val()
    //         }
    //     }).done((data) => {
    //         $("#console").val(JSON.stringify(data, null, "    "));
    //     });
    // }

    function getPerson(url) {
        $.ajax({
            url: "/tableTest/create_action",
            method: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: {
                "testStr": "fgjn7O1rpt",
                "testDate": "2062-08-07",
                "testDatetime": 2574983962566,
                "testFloat": 0.4456421,
                "testText": "J3InmYeWYqgWJ9CXf1ReL4QSZvljOQ5rfnoGCDFMpPYWsNtxiRYc5o8EswC9yKTLViOAb5oaW8DkGGeo37WmIMXcrXsSCRFjcJDhvKbYtMXxJo77bDSnQCR50VeRf5itro1lGJEqIox5j4Xmrbia5SdWzM36yPgHizR310FhfMPIFizfnq9ZaCox3EdopN6LzWeKDFfPXucdV3wSlc5bPbv7Fay6k91CriLUhTMgYHnTCvBbktlYhTsmzrv9Ueq9ffL0dp1D2deNCrpUC7lTmB8BNUCtKRFJNQWSFfG45Vz1okxcOiS4RvJLGQIwIFv9DN5k6ZX7Yv43GoC2GRrXx4Fddl99OD753Ft2Tb4lvdCbdoZn4yO3T7FlpUzv5mrEvg46WNAF5NjsTbIPwNIxqcu6wBZE90fZB1IexsHNNlP6Kep0pbWPmLRcxEzvvxGbMXwSaR1BUJ7WMNwln5lZddtIM0H1zMDyGQym0B2UlClIiLbdV87hScTACPLpUySLl9ebf9N9khUDUHzG6NUCbaTlb41Og1hOHoKTAXBt15VIqrvmjgTDsv6dJeT9UFuHCMSXbtPa3AjkANyP0l4xhlfyIDzTsj93ZFPjd3kuusAqOiNw8xHjBls57Uq8wqnrCOJwXdmgYmrbz6UxJLa7xi6vqZCclgmI99pD5wghmoRsjZrvMEpBoFhDFSOpuBZn0rpDOj2Y9qkpp49M6ar92cRcL77V8MLfFaXWpe1VGTheo9d49e7Rs0odG8KElR2cJ0MsEoyGf1FTqZb37Oodu2vxIkMBmqcBqN3oDxMfD9d8EewBu8HGI1sWT6pqPYVA0oV1DgjlxLy5h2UD5nRbYLi7Yz8g2qEkLL7icccV0Tgxwe9oLN9lAwORyyulS4F54fUKCkCFOFjcruCi60YmEIYBOZcj9QbzxaVbv7l4chfOC0lbwtYGOf42MyoyY9OlwtPg7zXRfP2VKAtAINZ7163hPXIHYiHRvabrHa6Q40ztm3lVrel2O3V16yBmw3MmCIPh5PX5c3VeoBlcm9ET5kpmYt6MeOijxpK34AnMSDgTmfoEFgjozPUmxGXUBwSjHpIZG1vBy84l9fFEK6u0dSNUrNdJXk43O9zoEgnQfMG1YycYNBsGvIRgt3rS5fvOi2k1Itl4d0DwHW6WUVUcxNaDqh5ZyRuk9vNOu4l2C7MO3P9HWeG054ilQWMTWsPQP09pzQhtnoqOeCyMiP08KV9brSfCvPhRyUUvfdC1Q1iQfxYmj3n3cv7HNWdKdSWJfhyc5cvMRHADO2MHnG7xoas7eNFutCwHxvNAU0SyNrNp4YbIZtLmfU0sWe3QHiggDWztGlmb2tx44eAAiJQPoUleJAVqgyaf8XUREnGtVEzznv3fiIG2mM0Y4Ktb1k7CdpZNzx1ohXOH7minWct20nMlkGBJUiWDEPAcfh3NKu9B8BMCo1Z8gLleqpf61ga87KRPzbsUq6e9XlhpV4vQD3N2JnLaVWAf1VtMjDmZzaJQaYfCZHo2YcalksICBEPlw5KTctUlcSakidow7KTH7BzU67Pk9oq61IL56ut1MgkOnCzu3dgVY1UDu7E1Av3MzZK8o9wAhGNZm9cfdOQjLYs0JIFgQJRVL7MT6g6Fu5bCrgCvvUahcYb6HiNVAZFD8VmREgSctYW4k3jhJyJo9IfLFOSymJYxjc8Qlpt80mqwxoyj0TyOUYY2pca2tUK6Pa8Fq9HQ8KmPlFXK0KhcGtxFevdTNe4M7cojs3cMfRx7B3gPYxx5FuUyk2gqFK1nYj9XiTZ1oS8oefnMrlfIbyKzji9S1oeZ5QDlt1c020jKe3ckU9uA5z58CxSBjmWFAPm7EXJm2QkLuzDBZbXp0wyNR2SBirKRzIyKFVYyt1jXNGWp9rT15ir1Vd2Gxv5fVWalh8lw8z5xmwf5n0ZHS21FOfUivYyf88JwUQd2CrObVsNHYz1IWIXEFckoPe5coOFU70DinsrybcZizU24GcTc23VchfHxU4iKtpE7kjy9GysXyO0wKrJPVSF1rETwdkLcFySkVcy5RgGKhnN2lRaVoxND4r8nV8l8ez2LMMnoZi6aMIDzyEUfMeREZ3PlWzKs02HApgwKN91rr3Mzs05fE1cj6G8QDygIFblBFagXwxlLsomQAklPDkNf737x5JdRRLkUQiAIeKCQQ7TjFbdRkLoBxs9uZHbXDBKz0zJDrO56tzbRSMLioujwqRKXjH07n7mmC4pw8KLmF6EBcRWvltP60R9M5EBwVRSZftjtowfcoPxRD2XMOa3Qs5rgNcMf4pgnRFNa8fysKFlVxXNLwk1jEhvJqK07yTinTK9goIMgSlHSRnSfqGr4rpy5SC9OFJdj5TD0RmsSEUlbCDL0cOGgVQwxBarO1svZT0Vte1eNIj1ovunfzyfNWhvPVEVXh0EAzdRaMDvlCRCHzxQV4rfFc9MtjmN3JJzYyaYBR4PZeqre69sK4QEGXKTUovYa1WYItesj6mBemPTF5miotyO6i23zrCsBJfWHRy09oYWz4njBJxeiiYthp6XgdmTqrriz9tzAJIc5p6UVCjO19c1aRlHcbkmMfKoBQJ1dZfBeGTabKaDNqT9Qgw8DsAcrcmaLW78tO1Cg6htfMoO9HKLV1FgP1NARQogRJT8lfiGpnYAYw5V9eGatVFIZuiUu9hWI6NItTdzsIs5hVuDq6BONFEoO5SOPjAo7NcgYzDeIMKtAo2fMpDyFgurb3oahJ74FbckmPsJPUONe2BmRts2cwpfcTXQ7yhtvdykJWZxWoYj4POnkGe6A61aWGeeanoxLQrF4zxZSjGbivRCeQD5iHKiu6Kg6AyGr32LfQX3uip3vRwmjgaRtUCcwclWrSxNEYXaD2C1F5ePc7nvFHzhIFeOIEJjX5HVXW1tmwejyYQLFN9Om84FypjlK3K6ooEGIGVdBRhKr31wH1oowT4nH0FR8Jx7CADuNiM6rOJZEzpdH4pvrb3EYzH40PhCI9LZXQ24T1LZMgyrN1NmUW4Rm627Vdt1URyBs7GESLbUMSRE1t0lfNhDa5G5dTs9CrkzKmeujSHp5shWu0HzZGTzrv0EWq98kWIwVO5J0CHvLXcOP6cgmjwnfbNPQuk88ZHRhoNFibnh4IeqmC7XdcDyNRxLH2pF8YtLy0jiq3aiYtr9EEGfsFmjhSAWtHly4Vz9ydCHIlITORJV7FxT3Fxu7r9NmG3bcnr8MbSKTLUEtywZPqw6aGafVNgu9ndyFRx23ILosuBqjENvl5IdOiE1nTiKwYVn7dRCWUk9OWQjxdB4J77cnPsamF4NABrgUTQSEy88nAXZTGapyPcAQ4LLrLL7Ohzf6T00kQgVvkasPd1VhYe65uyzYCAexcy8bSJ3Hz22xvmDxExOQwcgcP0HBak57D3KQwiwCylsT0qRPdij866DS2L1XvdlfhxmqaA2Q4p6f6vekThimGncx0skBG69xO8XC4AA80wUyzAqQIISialYUKwjxH0TrusZhAOyGgCvRikeKooMwcU1iXizPFUqAwXzQnDyThtNNnpvG95VZn3fJSUZhTlbed91b0Uurn2aDwGlWHYY7yY5u6whCIuEe9EpXwTteCV3L1kV2RRfzyFJDpoRVLFUK9InvhqsXXMbD3Qzk5QX6iwakAOstVtDLqRJUllU2GN74H8YveBwfAk6PViVfMVgePBQOspqqncPNBHFhybOzYOx8fFEoyFe5UoGJVb2RJiygKVoTiLD7xhSBSirPHGEA5BVrsxMgsQlNvfSe51MxCgE6sAVFQYKLTPvIVjQ2s4cUyGky564sFqgggfjmKudyvvHdXjwMpLiiuzL6AayyqsCcwxjk7w4xZ6wxaHESEH3g9jd40xNIDjjxvVOzn263g4BEagyT1sWpmCvXHSBcSmh4Km3AxBn336Dq62OFSeWkj7UQsh9OTdwKzSZhUbdSYZdNl4hEF1LXz8rOd7d6S6IB0iEY5HS9b2fgEWd3z3TZuXze8fYZ5n8Hqz2v23f6KVtvoHXtLxXlEbE5PfPgaCQJ7OXSVxHaIL0jzgY4ug63l9zYBUeUdsPBx2d3gKr6St1qRkHu3tfrfuozEXtw0lmOe5u2JwNgP4LBdzMpApuviJmQpoB2tQnBh6ivL0J6PUQkUSGmRxUeG1zOX3H9RSw8CXRlLoLqamVJ4P2fwtwASIf0a2fTx9JJsRT1dyFmKE6fDRi2Z1uqmbJfRoqJQTX3TaPdxYkFR8QZcFQREBzgDI0FugPZmR7EPcxTNgsNBiznnVcG1xa6t2UsG9iaPbJ7rmcaQDulXadmlksaj4xg48d7GY5t5SWicVuzgGKRWj9js1g2dlowuNcAaII2kZSGm1kSPIKi4BVdA2wdhTKd3heIWpNmxtNuVMsVy06JVk0prvhOfhtxuvvaeXGaOmEkE9ZLuzpx2d15iRdmB3mlEwBbx2gGlNMqdGq50gJ9vfhSCM2MwWf8yCplUJLmrAwdIN1hcvU9MusbT05sgd2Mn1d1abPJyUvW0bD9xTuaKQJA5Un2qQSMx0WEOKOhCVCKJFNfmixrrM5ecGMysGesnf24YxVjFUsnv6TqpdpMHTfKYw67LE7F0YpzebG3sPxrYt8VpR3zYsponUXbOfzh25VIzUzXEc5v8Bqs5b5jS3C3i1Qs9qmYUcwrdZpbIpaPONb5u9Cc17YAR5DXhUq81K9I3uQStZSm6Mv2kdltq6HdFEQHZLcclduSkkzPb5M0nE3DPIXYYAPMAkNyaLD8xShBioPSSCL6Hvl9Str2TWceuVd70cQw0IhuCbtJ0psFr6QGZxZKPJmIee5h3sWjdo0uExX54NOX9MPsozif3k1TuJxArTqV53xCG6edYhbC0xdJf6oyHRXfYSJSk2c705ni3ZKVf7SIZ30u5un1plpo7jOxrSbegImhA8JyNfOTlxtPWDp9ej90RQR0UL8VGhXlorPmevbJ1Fg483cxP0EYPBR7RRsEfgiIZy2bDzYWSrhGfVl3K1HVq5kw2wEsHJ2qNqcDcchnRMjZ8PrU8LayKtSI6NaMQNhKVUhKRA3FF4RE4wXejxHmF6ATkEEcN12vdKHSUgj1DtAQWEmNN3VqmoxC9H8otkYbDqvWNA1ziWuyz41INNwkL1V8ndfwfBoD7levgrrUUYROXrNopkLRuPMxhbFBCN3q1b5g5LQeXjzw1MbDjraZV5Sh77Sur6TxvfOjUUtJRAXmRGEkA8MqEQImPJgQPMQsrqBp0IxG7Pfw5wv2QMQqa94XLeVWormCkin8Hj5E2yZk16MdgFF2Rlag3UdINlWFT7B4jS2CRVwf3zZhWUxwdAUJCJCYyGlbEg2rVT4q0efJFmy2LH7cEWKSnvgKO1BCXrI1TPkuWHsIq5cpHsu6Scja9FxUVUsAqd2EzzeGnchG5DUx9gGXomp03kmaF2PYakHn5CaxoougGRaGJNANaD4nMouVmIHVFzn5rsvlATP7ftsUIkw7StiwTYZHCbaFmTirGdrmKVPiav3GQZMPtxmi78tBV1UegDbMJZOhs5hQB3D3fytJtwzoLvK2Xhrfh1o7A28TX5L2CCPJ9n1gwiqqbIFzlXUpcoUipW5DUmXX7cAgJ3SsK7t9KGBvXxgEFS48ifahTRm7OQQaUdkjwT3qDmNZGinSIqurl9nzsDQ9DbO2DHSxUC8iQkJZV9HB7lEeIdLDRkw7uJokEWz7I9eDURlWwWi0NMvdbDrXgXviVLKcL6idVwKv1aniSjjsJvmiDtCbXAynv2LeJFr6TPTZp2AbNbfM5m9bk5uKUiaIo7j5JKoh9JyJvAmRcIH04NFhEpMz4OslCPe3J7xdzuJDo18ayrPgjJKYB4hizRknqXpduheiQTkHEZoidPC1fYkD27tJtDfciEy3DeqRA4boqzKdEHBZTJd3FAlNcaHOCWzZnU6Do2mhjUrzlfw0hjb9quOiQknQMu3T27yG1IxLHzSM7uBsHQuj5UlQgOvBbRdDvwryLf6EEqci0DqtkrLqiKmORksbUHeBko3ukw5ucQAcUbzUPpz1ABYnkSIGsOCudOwJmHd8Ra2gaEhFI3YnylWLQe5xPF6fFQC87Picbc1jIssAaQt7CZIvxWb2xadO1cvCkBofmWGpflFz7eB6xO8kwd3Nu2qFrIlBVHsGHbqFW8OaUHAGxnSDFshVUZ69icgEngFG6fEp3F63JjcbSYu6pEun3LRVJaiAA0k7RcGAq3UhdC4RQCSFMVmCEydgOogM5O6qyUoOYkEs9KrsAji4pk9ywc8X47xIFh6K9zjpG5m5No1BpRIpx63RO89c8wbKlW0cphYw1dhZ9zJslHWGraxvAnjigRR1UuFOsLBKwfoUlGZRsJqfVXYtBPZ2Vq7O7RtixFrsJebyJXteJ6LoVNY9XYrlEdEy4i4dkP8QfA4RiwoyoIErBweKkkmN1ejYcqWqLPda6hT2HhqarQwX2jgjVUN2wRYwvq931k84w1s1wwK6mLiv5ndbBW9jlVVWAUzhpvTcnXQJurAppaEwn3Ykj2VAcb7Kl0uzk5exty37rdBraV4X2T6d0M42BfZBabwDH0QI6wQgPGNNU2TB53VjAyajz4jPmGMcsomVBuq2r8Iyza14nfsvsDu1XdoHZyF4Yr0jkBisvRLmOufqciaP7cml3WtXKPqBfpfO8xzSQtVZTsN6Jxx0Evu0kxGhC4ocYfE4hDjzaxHdDORqSIwwMeJmjovtKyVy9jDm8HhpOXkTnihOYghYjWDZ3VoXDFukMJS7EW1sYwzVAYdPU7fSYVCI6OeSM6TEHHDweLH9gTIL3a24jk5jxNlZLG6xyb8MTFRizakBpNhCdM6okGqYzXgrRhos8aOLu70lBhG6T34f3rv2HboHIb6g8ZjQnkk6fNcXgqDSOPMHNj9IJgDqEL2ysxzawCFa4QgbvxJxZwPl6jkSebt99WtapMVJXGM5HkZROTEljAw0hIyPzDqVxmd7GVDBX1oiiotYfEo1pNUShBwMAJthFFvMFscvsFdy2CSiIlshboePzFz8JzpAL3znIubuP5NIVFFntjh5a3fTXeFXhxPUoi7w1mKLQV610n2fNvXrgSFd3abvUPJNtqT47pVuxyqzIJT3wWPY9GtfYbUbvq4G6ydbl2KUho2bdMx5bF6IYkEM5FOiCkuN2GlqI9BQc5zY11eBvLzaiDJ2MPuEkyaCSZNpQfiOxHnFw17WUuSOeM1q8o8aJZdbqSpNMcbSX8Yp1Lf63285gJJl0LaHywUhqz73SLc3ndJaFOcGs2fy3aRPIkC7WkubeueOLK8GgrTMJZoiwj8sVSWeiEoYxUKuRub0J9s22rRpNIG5rvRkeCvctw0yj6RTxcozK84dZCGjZ35DcDKVPVWhJJzjg5k8wSg5jY3YS6p17lxljWyEAdnzopxD204xPhapa4kTBH7KfYhCgcfT7hxr5xAJfxGz4vMHYLmQl4ellbZlJPkl0GL8j0Tfk2jegPRPHdpF3YPo7Y5lptOIzpYMRjk1z11LZd6gyaUbQpepkVNNwivyejG8BdSZwE6dozrFd1JG7TPek1HRQfviusHE9AjVcu3QsvkTlSo3L1HrtBKbS8CICDaA4b2qnu0fo3SINSCcFPvGZkUgSz8kCOtHrQm723yXspFodb7U9S8yUGFtQHf9ecKw47848oPgkti4SLPfJTIYvGWdN7RplHQ5cKaXgl4d96fDF2Nwxze6AmmzQfcQhrKx3EA98krycIhpNWruEXHdoDFHkKy3ZPoGyKswyUBwe36N2YZ1ky3bZPKduczgpDioF0dOv7Em5pSKERrXjoZ7NFHBIm5KnwJZCtHM9j5w5ZLs72AqXhvGMmddqoxmaZXCeTCON0nsNoapULnDB6Wk6LKBspnx5N2xgTv9jfbFaSar1GOV9P0GH4N1yhbTcXznIoRn2QnRpzVbnALJ9zOX3NIicMlaKUPpWoS5xKE4avyVZ1YBqdE2LORRWucHHwDnFK4QuyU5ysVPzExwy7RzfrZZSY3bHYnJ9JIxcGqUPvG2CNm4DSTfDEXenOA6CUWGhREkStgYACMmIdbepKV3185jCT56b0V1PFgKwp8lxypkCGh9DH74XWLchcNjGIPfxpkg7rYNKowyMARlC5CvvTNMFvzUjQCL08K6vXChRo7YRwFtXfS5qD5JKx8wRKZhIdwMLuEBNhRtRdt1mJCw6FzYrcmNZqRMo8wPbDXutJIIaab5AEiDGMmnMVVQgSH2dZ2KLYhp3dRFjpWHEpJqWvkygu2NXifpaoweMdNYSHM80MUWsLK6hDpCT2rcRxevnPEBkyNWnH7fKgI6N57ouODkYCAYzqhlK8kk9im3G1wNOsUPXgOlgjwPy30QfkHhZjcVOB2cKQ2zMvWUcO3nR8fGJlkBaUTpb9kYhd3p9g8EZJBqjRZABAdpuDo84oSQnDFElovhPGGCq1dhgqXjR1rdntQ3uZVpf4W8JV5KmvmUJXqQiW258dHxAaVPJ0YNWWmaDd1YJtgSfxrH0nnEowft5BII8o21U5hBtDbY7BAQAZaVQM0S5qlrg2tuUM05XY369PCbq3eXrmGlItsYLhirm8KeE0lkZVYBJJ814Q0OCLfOmCuzjlBjHuWCyvvg8BsCgxdLheqiWcUxqyQewmAxI6nzJ9z8YUEu14NMxgKdX0XcObwA2se6HKfx61nRkknhME9N0lTYxCSFqW5gtVOzuhmqErPJi1XQ0q03DFQ1aM5I7bqQDUKntv7MrY5wS7iZ0ro8SAFbxq2YbeEzGUt8msOkFVHkdCaE5YmTowwHU4kvQ9oMLJdzqjLQFnLcd1MbtQnffXBEKtRqhUlPo3zKYmcj5kyBZK4MOD18oUkjsIrP53n9H6ETaD3ElwByY2o3orDMEaet6JaTpn0c0wLWRd9Uz4jSm0tsHwzr6uOKI80fucriBLLzYJ9Givfy1wkGWMdAlqlOKBNuahz2M75Qlt9Dn6WrVxZJml5kd2Hx9hIu0eH0tTu2E1lWK0PGPQZHxl8K7aYzw732Rp91dUQqmtlipE4aMhW4ty5BkhIxagtxTvNm4XJAZec6Q4UY8vcz8qQoJCMhtKx9oI3xKxFQwlnhxgZwtRbwJ8BTHGbXQHKdUS3caOmoBOVRGMY6JNaMSWCP51ZWAdZb3twMqjOfmLuOlYgzOernAj1ODTa3IaKGsLSfvWIG2swUJTQHXHaBPUCPvsaNxZiUMoO3TdDIrGBNfKc7kHtcOphBTfBqXSXVfA7ivMB5xi0wNvdaxJbmGbaJ6BIlAUxGbSvDDxVChC9Ii92eyT16dKNdYl0Al7lnFJmOCjFl5lo9VTgGLWtcbc5TEygQ8RgdcV5OvIou9w11MI7MIOrn5IKyeLTPJQZX7qN9JIG3SnXhwhX6xZjnqQgZCTkS9vzpSCTeQvNNFBXm0xI3Wc2k3igeYk7zbSYmqCqCleQvpHIoMw2YBkXHAquT8Di0e1fEjqx7Vb7RkxXWN6ir7iP9wHONk0kDydJeeDIzmhEz7sQtW32CnCQlPn15tzOCAl0EvJrSLyVipN6OJcp71sPywIJD2sq0RL6Bo6XXd7rLfnTwd9gKGZAz0nXNhdr6vqvkEFXMsV5BNu8aq80BUnc7p3FOPudpEPnqY3r8h4IOTAds6WhEU8hUBISFqU2SrZqAiRxpEmCeHZM90pNmdwxQzA08noaHAkVBU83nwlTWwjOzukNdG00pOi8AWOwfaHP8dUoXXnghKV138FiVYurjDThKkJ5yLy0zgrlbgh1m1OAL8big2p2aGEC9afY9vXuwiL0bY1CTTLWZaCjFbGbw6AjSIyqR6xHDAvHydJQFmMlcwi4SbxapnpnmgSqK8OIujGOhvXZwRBb4GcKZwU4OLrLHMaLvtjLiZDsPkRxlJ259lRH9ksA1udYTgux85lP5wyehIX6H3DtMPACGEvCmAZjmKmio0hjCa39n6ZodmrAH"
            },
            headers: {
                Authorization: "Bearer " + $("#token").val()
            }
        }).done((data) => {
            $("#console").val(JSON.stringify(data, null, "    "));
        });
    }

    $("#button_simen").click(function () {
        postLogin("simen")
    });
    $("#button_admin").click(function () {
        postLogin("admin")
    });
    $("#button_getPersonJsonRead").click(function () {
        getPerson("/getPersonJsonRead")
    });
    $("#button_getPersonJsonWrite").click(function () {
        getPerson("/getPersonJsonWrite")
    });
    $("#button_getPersonJsonError").click(function () {
        getPerson("/getPersonJsonError")
    });
    $("#button_logout").click(function () {
        $.ajax({
            url: "/logout",
            method: "POST",
            headers: {
                Authorization: "Bearer " + $("#token").val()
            }
        }).done((data) => {
            if (data) {
                $("#console").val(JSON.stringify(data, null, "    "));
                $("#token").val("");
            } else {
                $("#console").val("");
                $("#token").val("");
            }
        });
    });
</script>
</html>