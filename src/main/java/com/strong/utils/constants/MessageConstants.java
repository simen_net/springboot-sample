package com.strong.utils.constants;

public class MessageConstants {
    public final static String ERROR_NOT_NULL = "内容不能为空";
    public final static String ERROR_SIZE = "必须在{min}和{max}之间";

    public final static String ERROR_PASSWORD_NO_STANDARD = "密码不符合规范，必须使用包含字母和数字的8-32位字符";
    public final static String ERROR_ID_NUMBER_NO_STANDARD = "证件号码不符合证件类型规范";
    public final static String ERROR_MOBILE_NO_STANDARD = "手机号码格式不规范";
    public final static String ERROR_EMAIL_NO_STANDARD = "电子邮件格式不规范";
    public final static String ERROR_TELEPHONE_NO_STANDARD = "固定电话格式不规范";
    public final static String ERROR_QQ_NO_STANDARD = "QQ号码格式不规范";
    public final static String ERROR_WX_NO_STANDARD = "微信号码格式不规范";
    public final static String ERROR_NAME_NO_STANDARD = "用户名不符合规范：名字必须位中文";
    public final static String ERROR_LOGIN_NAME_NO_STANDARD = "登录名不符合规范：以小写字母开头开头，仅包含小写字母、数字、下划线、减号的6到16位字符串";
    public final static String ERROR_LOGIN_NAME_EXISTS = "登录名已存在";
    public final static String ERROR_AUTHORITY_FORMAT = "用户权限格式错误";

    public final static String ERROR_UNIT_ERROR = "所在单位为空或不存在";
}
