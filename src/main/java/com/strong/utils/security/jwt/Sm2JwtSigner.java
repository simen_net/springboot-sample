package com.strong.utils.security.jwt;

import cn.hutool.jwt.signers.JWTSigner;
import com.strong.utils.security.SecurityUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * sm2的JWT登录验证类
 *
 * @author Administrator
 * @date 2022/05/26
 */
@Slf4j
public class Sm2JwtSigner implements JWTSigner {

    /**
     * 签名分割字符串
     */
    public final static String STR_JWT_SIGN_SPLIT = "#|#";

    /**
     * 返回签名的Base64代码
     *
     * @param headerBase64  JWT头的JSON字符串的Base64表示
     * @param payloadBase64 JWT载荷的JSON字符串Base64表示
     * @return 签名结果Base64，即JWT的第三部分
     */
    @Override
    public String sign(String headerBase64, String payloadBase64) {
        // 将headerBase64和payloadBase64使用STR_JWT_SIGN_SPLIT组合在一起之后进行签名
        return SecurityUtils.signByUUID(headerBase64 + STR_JWT_SIGN_SPLIT + payloadBase64);
    }

    /**
     * 验签
     *
     * @param headerBase64  JWT头的JSON字符串Base64表示
     * @param payloadBase64 JWT载荷的JSON字符串Base64表示
     * @param signBase64    被验证的签名Base64表示
     * @return 签名是否一致
     */
    @Override
    public boolean verify(String headerBase64, String payloadBase64, String signBase64) {
        // 将headerBase64和payloadBase64使用STR_JWT_SIGN_SPLIT组合在一起之后进行签名校验
        return SecurityUtils.verifyByUUID(headerBase64 + STR_JWT_SIGN_SPLIT + payloadBase64, signBase64);
    }

    /**
     * 获取算法
     *
     * @return 算法
     */
    @Override
    public String getAlgorithm() {
        return "国密SM2非对称算法，基于BC库";
    }
}
