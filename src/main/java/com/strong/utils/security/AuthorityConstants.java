package com.strong.utils.security;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 权限常量
 *
 * @author Simen
 * @date 2022/02/10
 */
public class AuthorityConstants {

    /**
     * 登录成功Map
     */
    public static Map<String, String> MAP_LOGIN_SUCCESS = new ConcurrentHashMap<>(16);

    /**
     * 身份验证的前缀
     */
    public static final String AUTHENTICATION_PREFIX = "Bearer ";

    /**
     * JWT的format模板
     */
    public static final String AUTHENTICATION_TEMPLATE = AUTHENTICATION_PREFIX + "%s";

    /**
     * 超级管理员 权限
     */
    public static final String STR_AUTHORITY_SUPER = "AUTHORITY_SUPER_ADMINISTRATOR";

    /**
     * 普通管理员 权限
     */
    public static final String STR_AUTHORITY_GENERAL = "AUTHORITY_GENERAL_ADMINISTRATOR";

    /**
     * 权限数组
     */
    public static final String[] STRS_AUTHORITY = {STR_AUTHORITY_SUPER, STR_AUTHORITY_GENERAL};

    /**
     * 超级管理员 权限控制字符串
     */
    public static final String STR_HAS_AUTHORITY_SUPER = "hasAuthority('" + STR_AUTHORITY_SUPER + "')";

    /**
     * 普通管理员 权限控制字符串
     */
    public static final String STR_HAS_AUTHORITY_GENERAL = "hasAuthority('" + STR_AUTHORITY_GENERAL + "')";

}
