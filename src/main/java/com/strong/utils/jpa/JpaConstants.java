package com.strong.utils.jpa;

public class JpaConstants {
    public final static String MSG_QUERY_ID_EMPTY = "查询的ID不能为空";
    public final static String MSG_CONTENT_EMPTY = "提交的内容不能为空";
    public final static String MSG_SEARCH_CONDITION_EMPTY = "查询条件不能为空";
    public final static String MSG_RECORD_NONENTITY = "未查询到该记录";
    public final static String MSG_DELETE_RECORD_EMPTY = "待删除的记录集合不能为空";
    public final static String MSG_DELETE_RECORD_BEEN_NULL = "待删除的记录可能不存在";
    public final static String MSG_DELETE_SUCCESS = "删除操作成功";


    /**
     * 序列化包含的字段数组 常量名
     */
    public final static String STR_INCLUDE_PROPERTIES_NAME = "STRS_INCLUDE_PROPERTIES";
}
