package com.strong.utils;

import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;

import java.sql.SQLException;

/**
 * H2数据库工具类
 *
 * @author simen
 * @date 2021/09/03
 */
@Slf4j
public class H2Utils {

    /**
     * 数据库链接字符串
     * <p>
     * <em>jdbc:h2:tcp://localhost:9092/C:/aaa;DATABASE_TO_LOWER=true</em>
     * <em>DATABASE_TO_LOWER 强制表格等名称为小写，则都为大写</em>
     */

    public static Server H2_SERVER = null;

    public static void main(String[] args) {
        H2Utils.startH2Server();
    }

    /**
     * 启动H2数据库服务器
     */
    public static void startH2Server() {
        try {
            if (H2_SERVER == null) {
                // 创建tcp模式的h2服务器，ifNotExists 表示数据库文件不存在，则创建
                H2_SERVER = Server.createTcpServer("-ifNotExists");
            }
            H2_SERVER.start();
            if (H2_SERVER.isRunning(true)) {
                log.info("H2数据库服务器[{}]已启动", H2_SERVER.getURL());
            } else {
                throw new RuntimeException("H2数据库服务器未启动。");
            }
        } catch (SQLException e) {
            throw new RuntimeException("H2数据库启动失败，错误代码：\n", e);
        }
    }

    /**
     * 关闭H2数据库服务器
     */
    public static void stopH2Server() {
        if (H2_SERVER != null) {
            // 停止服务器
            H2_SERVER.stop();
            // 获取当前时间long
            long longNow = System.currentTimeMillis();
            // 如果服务器运行状态，则等待10秒后强制退出
            while (H2_SERVER.isRunning(false)) {
                if (System.currentTimeMillis() - longNow > 10 * 1000) {
                    break;
                }
            }
            if (H2_SERVER.isRunning(false)) {
                log.info("H2数据库服务器[{}]被强制关闭。 ", H2_SERVER.getURL());
            } else {
                log.info("H2数据库服务器[{}]已关闭。 ", H2_SERVER.getURL());
            }
        }
    }

}
