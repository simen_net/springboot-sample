package com.strong.utils.enum_;

import lombok.Getter;

@Getter
public enum IdTypeEnum {
    /**
     * 身份证
     */
    IDENTIFICATION_CARD("身份证", "IDENTIFICATION_CARD"),

    /**
     * 港澳居民来往内地通行证
     */
    HONG_KONG_MACAO_PERMIT("港澳居民来往内地通行证", "HONG_KONG_MACAO_PERMIT"),

    /**
     * 台湾居民来往大陆通行证
     */
    TAIWAN_PERMIT("港澳居民来往内地通行证", "TAIWAN_PERMIT"),

    /**
     * 外国护照
     */
    FOREIGN_PASSPORT("外国护照", "FOREIGN_PASSPORT");

    /**
     * 名称
     */
    private String name;

    /**
     * 代码
     */
    private String code;

    /**
     * 实例化方法
     *
     * @param name 名称
     * @param code 代码
     */
    IdTypeEnum(String name, String code) {
        this.name = code;
        this.code = code;
    }
}
