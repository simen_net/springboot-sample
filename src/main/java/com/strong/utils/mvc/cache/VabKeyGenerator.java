package com.strong.utils.mvc.cache;

import com.strong.utils.JSON;
import com.strong.utils.mvc.pojo.retrieve.VabDTO;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Spring cache key构造器
 *
 * @author Simen
 * @version 1.0
 */
@Component
public class VabKeyGenerator implements KeyGenerator {

    /**
     * 生成方法
     *
     * @param target 目标
     * @param method 方法
     * @param params 参数个数
     * @return {@link Object }
     */
    @Override
    @NonNull
    public Object generate(final Object target, final Method method, final Object... params) {
        StringBuilder sbTemp = new StringBuilder();
        // 将方法的基本信息加入StringBuilder
        sbTemp.append(target.getClass().getName()).append("|");
        sbTemp.append(method.getName()).append("|");

        // 遍历所有参数
        for (Object param : params) {
            // 去自身、父级、父父级的类型，如果是VueDTO类型，则取值组成关键字
            if (param instanceof VabDTO) {
                sbTemp.append(JSON.toJSONString(param));
            }
        }
        return sbTemp.toString();
    }
}
