package com.strong.utils.mvc.pojo.view.vab;

import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用于与vab前端交互的统一泛型对象
 *
 * @param <M> 装载的数据类型
 * @author Simen
 * @version 1.0
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ReplyVabListVO<M> extends ReplyVO<VabList<M>> {

    /**
     * 实例化
     *
     * @param list 列表
     */
    public ReplyVabListVO(List<M> list) {
        this(list, -1L);
    }

    /**
     * 实例化
     *
     * @param list      列表
     * @param replyEnum 回复枚举
     */
    public ReplyVabListVO(List<M> list, ReplyEnum replyEnum) {
//        super(VabList.<M>builder().list(list).build(), replyEnum);
        super(new VabList<>(list, -1L), replyEnum);
    }

    /**
     * 实例化
     *
     * @param list  列表
     * @param total 总计
     */
    public ReplyVabListVO(List<M> list, Long total) {
//        super(VabList.<M>builder().list(list).total(total).build());
        super(new VabList<>(list, total));
    }

    /**
     * 实例化
     *
     * @param list  列表
     * @param total 总计
     */
    public ReplyVabListVO(List<M> list, Integer total) {
//        super(VabList.<M>builder().list(list).total(total.longValue()).build());
        super(new VabList<>(list, total.longValue()));
    }

    /**
     * 实例化
     *
     * @param list      列表
     * @param total     总计
     * @param replyEnum 回复枚举
     */
    public ReplyVabListVO(List<M> list, Long total, ReplyEnum replyEnum) {
//        super(VabList.<M>builder().list(list).total(total).build(), replyEnum);
        super(new VabList<>(list, total), replyEnum);
    }

    /**
     * 实例化
     *
     * @param list  列表
     * @param total 总计
     * @param msg   消息
     * @param code  代码
     */
    public ReplyVabListVO(List<M> list, Long total, String msg, Integer code) {
//        super(VabList.<M>builder().list(list).total(total).build(), replyEnum);
        super(new VabList<>(list, total), msg, code);
    }
}
