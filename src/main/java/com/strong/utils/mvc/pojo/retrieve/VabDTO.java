package com.strong.utils.mvc.pojo.retrieve;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.strong.utils.JSON;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Vab查询基本DTO
 *
 * @author simen
 * @date 2022/02/23
 */
@Setter
@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
@JsonPropertyOrder(alphabetic = true)
public class VabDTO<M> extends RetrieveDTO<M> {

    /**
     * 正序排列字符串
     */
    @JsonIgnore
    public final static String STR_ORDER_ASC = "ascending";

    /**
     * 反序排列字符串
     */
    @JsonIgnore
    public final static String STR_ORDER_DESC = "descending";

    /**
     * 默认页面大小
     */
    public static final int INT_DEFAULT_PAGE_SIZE = 10;

    /**
     * 当前页码
     */
    private Integer intPageNo;

    /**
     * 页面大小
     */
    private Integer intPageSize;

    /**
     * 实例化
     */
    public VabDTO() {
        this(0, INT_DEFAULT_PAGE_SIZE);
    }

    /**
     * 实例化
     *
     * @param intPageNo   int页面没有
     * @param intPageSize int页面大小
     */
    public VabDTO(Integer intPageNo, Integer intPageSize) {
        this.intPageNo = intPageNo;
        this.intPageSize = intPageSize;
        // 如果分页信息为空 则赋予默认值
        if (ObjectUtil.isNull(intPageNo) || intPageNo < 0) {
            this.intPageNo = 0;
            this.intPageSize = INT_DEFAULT_PAGE_SIZE;
        } else {
            this.intPageNo = intPageNo;
        }

        // 如果页面大小为空 则赋予默认值
        if (ObjectUtil.isNull(intPageSize) || intPageSize <= 0) {
            this.intPageSize = INT_DEFAULT_PAGE_SIZE;
        } else {
            this.intPageSize = intPageSize;
        }
    }

    @Override
    public Pageable getPageable() {
        // 如有排序条件则加入
        if (ObjectUtil.isNotNull(super.getSort())) {
            return PageRequest.of(this.intPageNo, this.intPageSize, super.getSort());
        } else {
            return PageRequest.of(this.intPageNo, this.intPageSize);
        }
    }

    @Override
    public void addSort(Sort.Direction direction, String... strsSortProp) {
        direction = ObjectUtil.defaultIfNull(direction, Sort.Direction.ASC);
        if (ArrayUtil.isNotEmpty(strsSortProp)) {
            super.setSort(super.getSort().and(Sort.by(direction, strsSortProp)));
        }
    }

    @Override
    public void addSearch(String strKey, String strValue) {
        super.getMapSearch().put(strKey, strValue);
    }

    @Override
    @JsonIgnore
    public String getAscString() {
        return STR_ORDER_ASC;
    }

    @Override
    @JsonIgnore
    public String getDescString() {
        return STR_ORDER_DESC;
    }

    /**
     * 获取json字符串
     *
     * @return {@link String}
     */
    public String getJsonString() {
        Map<String, Object> mapJson = new LinkedHashMap<>();
        mapJson.put("intPageNo", this.intPageNo);
        mapJson.put("intPageSize", this.intPageSize);
        mapJson.put("mapSearch", this.getMapSearch());
        mapJson.put("mapSort", this.getMapSort());
        return JSON.toJSONString(mapJson);
    }
//
//    /**
//     * 归一化处理当前页码（VAB提交过来的页码从1开始 JPA的页码从0开始）
//     *
//     * @param intPageNo 页码
//     */
//    public void setPageNo(Integer intPageNo) {
//        if (intPageNo != null && intPageNo >= 1) {
//            this.intPageNo = intPageNo - 1;
//        } else {
//            this.intPageNo = intPageNo;
//        }
//    }
}
