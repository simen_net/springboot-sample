package com.strong.utils.mvc;

import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static com.strong.utils.mvc.MvcConstants.*;

/**
 * Json控制器接口
 *
 * @author simen
 * @date 2022/02/09
 */
public interface ModuleJsonController<VO, CDTO, UDTO, RDTO> {

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return {@link ReplyVO}<{@link VO}>
     */
    @PostMapping(value = URL_RECORD_VIEW_INT_ID)
    ReplyVO<VO> getRecordView(@PathVariable(PARAMETER_INT_ID) final Integer intId);

    /**
     * 添加记录操作 控制器
     *
     * @param modelCreate 用于添加操作的模型
     * @return {@link ReplyVO}<{@link VO}>
     */
    @PostMapping(value = URL_CREATE_ACTION, consumes = {MediaType.APPLICATION_JSON_VALUE})
    ReplyVO<VO> getCreateAction(@Valid @RequestBody final CDTO modelCreate);

    /**
     * 修改记录操作 控制器
     *
     * @param modelUpdate 用于修改操作的模型
     * @return {@link ReplyVO}<{@link VO}>
     */
    @PostMapping(value = URL_UPDATE_ACTION, consumes = {MediaType.APPLICATION_JSON_VALUE})
    ReplyVO<VO> getUpdateAction(@Valid @RequestBody final UDTO modelUpdate);

    /**
     * 返回待修改记录的信息 控制器
     *
     * @param intId 查询用主键
     * @return {@link ReplyVO}<{@link VO}>
     */
    @PostMapping(value = URL_UPDATE_VIEW_INT_ID)
    ReplyVO<VO> getUpdateView(@PathVariable(PARAMETER_INT_ID) final Integer intId);

    /**
     * 删除记录操作 控制器
     *
     * @param intsId 查询用主键数组
     * @return {@link ReplyVO}<{@link String}>
     */
    @PostMapping(value = URL_DELETE_ACTION_INTS_ID)
    ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId);

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询操作的模型
     * @return {@link ReplyVabListVO}<{@link VO}>
     */
    @PostMapping(value = URL_LIST_VIEW, consumes = {MediaType.APPLICATION_JSON_VALUE})
    ReplyVabListVO<VO> getListView(@RequestBody final RDTO modelRetrieve);

    /**
     * 显示分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询操作的模型
     * @return {@link ReplyVabListVO}<{@link VO}>
     */
    @PostMapping(value = URL_PAGE_VIEW, consumes = {MediaType.APPLICATION_JSON_VALUE})
    ReplyVabListVO<VO> getPageView(@RequestBody final RDTO modelRetrieve);
}
