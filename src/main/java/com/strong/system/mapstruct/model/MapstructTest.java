package com.strong.system.mapstruct.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MapstructTest {
    public static void main(String[] args) {
        User user = new User()
                .setId(1L)
                .setUsername("zhangsan")
                .setSex(1)
                .setPassword("abc123")
                .setCreateTime(LocalDateTime.now())
                .setBirthday(LocalDate.of(1999, 9, 27))
                .setConfig("[{\"field1\":\"Test Field1\",\"field2\":500}]");
        UserVo userVo = DoctorMapper.INSTANCE.toDTO1(user);
        System.out.println(userVo);
    }
}
