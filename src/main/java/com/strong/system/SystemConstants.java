package com.strong.system;

/**
 * 系统常量
 *
 * @author simen
 * @date 2022/05/23
 */
public class SystemConstants {

    /**
     * jakarta.servlet.error.status_code 异常字符串
     */
    public final static String JAKARTA_SERVLET_ERROR_STATUS_CODE = "jakarta.servlet.error.status_code";

    /**
     * jakarta.servlet.error.exception 异常字符串
     */
    public final static String JAKARTA_SERVLET_ERROR_EXCEPTION = "jakarta.servlet.error.exception";

    public final static String ORG_SPRINGFRAMEWORK_BOOT_WEB_SERVLET_ERROR_DEFAULTERRORATTRIBUTES_ERROR = "org.springframework.boot.web.servlet.error.DefaultErrorAttributes.ERROR";

    /**
     * 系统pid文件名
     */
    public static final String STR_FILE_APP_PID = "app.pid";

    /**
     * 静态目录名称
     */
    public final static String STR_DIRECTORY_STATICS = "statics";

    /**
     * git忽略目录
     */
    public static final String STR_DIRECTORY_GIT_IGNORE = "gitignore";

    /**
     * 数据目录
     */
    public static final String STR_DIRECTORY_DATA = "data";

    /**
     * +86时区
     */
    public static final String STR_TIME_ZONE_ASIA_SHANGHAI = "Asia/Shanghai";

}
