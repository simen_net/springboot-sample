package com.strong.config;

import com.strong.system.constroller.SystemErrorController;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 全局错误配置
 *
 * @author simen
 * @date 2022/11/18
 */
@Configuration
public class SystemErrorConfiguration {

    /**
     * 替换BasicErrorController
     *
     * @param errorAttributes            错误属性
     * @param serverProperties           服务器属性
     * @param errorViewResolversProvider 错误视图解析器供应商
     * @return {@link SystemErrorController}
     */
    @Bean
    public SystemErrorController basicErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties,
                                                      ObjectProvider<List<ErrorViewResolver>> errorViewResolversProvider) {
        return new SystemErrorController(errorAttributes, serverProperties.getError(), errorViewResolversProvider.getIfAvailable());
    }
}
