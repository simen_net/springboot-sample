package com.strong.config.security.handler;

import com.strong.utils.JSON;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.strong.utils.mvc.pojo.view.ReplyEnum.ERROR_ACCESS_IS_FORBIDDEN;

/**
 * JWT访问拒绝处理程序
 *
 * @author simen
 * @date 2024/08/07
 */
@Component("JwtAccessDeniedHandler")
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(JSON.toJSONString(new ReplyVO<>(ERROR_ACCESS_IS_FORBIDDEN)));
    }

}
