package com.strong.config;

import com.strong.utils.JSON;
import com.strong.utils.exception.StrongRuntimeException;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String MSG_ACCESS_DENIED_EXCEPTION = "权限受限无法访问";

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, @Nullable HttpHeaders headers, @Nullable HttpStatusCode status, @Nullable WebRequest request) {
        BindingResult bindingResult = ex.getBindingResult();
        if (bindingResult.hasErrors()) {
            Map<String, String> mapError = new HashMap<>();
            for (ObjectError error : bindingResult.getAllErrors()) {
                if (error instanceof FieldError fieldError) {
                    mapError.put(fieldError.getField(), fieldError.getDefaultMessage());
                } else {
                    mapError.put(error.getObjectName(), error.getDefaultMessage());
                }
            }
            return new ResponseEntity<>(new ReplyVO<>(mapError, ReplyEnum.ERROR_SERVER_ERROR), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(new ReplyVO<>("未知参数校验错误", ReplyEnum.ERROR_SERVER_ERROR), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ReplyVO<String>> accessDeniedException(AccessDeniedException ex) {
        return getResponseEntityReplyVO(MSG_ACCESS_DENIED_EXCEPTION, HttpStatus.PAYMENT_REQUIRED);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ReplyVO<String>> handleIllegalArgumentException(IllegalArgumentException ex) {
        return getResponseEntityReplyVO(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StrongRuntimeException.class)
    public ResponseEntity<ReplyVO<Map<String, String>>> handleStrongRuntimeException(StrongRuntimeException ex) {
        return new ResponseEntity<>(new ReplyVO<>(ex.getMapError(), ReplyEnum.ERROR_SERVER_ERROR), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * 获取响应实体回复VO
     *
     * @param strMessage 消息
     * @param httpStatus http状态
     * @return {@link ResponseEntity }<{@link ReplyVO }<{@link String }>>
     */
    private ResponseEntity<ReplyVO<String>> getResponseEntityReplyVO(String strMessage, HttpStatus httpStatus) {
        return new ResponseEntity<>(new ReplyVO<>(strMessage, ReplyEnum.ERROR_SERVER_ERROR), new HttpHeaders(), httpStatus);
    }
}