package com.strong.sample;

import com.strong.utils.H2Utils;
import com.strong.utils.StrongUtils;
import com.strong.utils.security.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.TimeZone;

import static com.strong.system.SystemConstants.STR_FILE_APP_PID;
import static com.strong.system.SystemConstants.STR_TIME_ZONE_ASIA_SHANGHAI;

@Slf4j
@EnableCaching
@EnableScheduling
@ServletComponentScan
@SpringBootApplication(scanBasePackages = {"com.strong"})
public class SpringbootSampleApplication {

    public static void main(String[] args) {
        // 测试加解密、签名字符串
        SecurityUtils.testPasswordEncode();
        // 启动H2服务器
        H2Utils.startH2Server();
        // 设置当前时区
        TimeZone.setDefault(TimeZone.getTimeZone(STR_TIME_ZONE_ASIA_SHANGHAI));
        // 获取系统对象
        SpringApplication application = new SpringApplication(SpringbootSampleApplication.class);
        // 系统pid记录文件
        String strPidFile = StrongUtils.getStaticPath(STR_FILE_APP_PID);
        // 将pid监听信息写入记录文件，使用 cat /xxxx/app.id | xargs kill 停止服务
        application.addListeners(new ApplicationPidFileWriter(strPidFile));
        log.info("将pid监听信息写入记录文件{}", strPidFile);
        // 启动系统
        application.run();
    }

}