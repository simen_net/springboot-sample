package com.strong.sample.table.t_test.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.sample.table.t_test.TableTestConstants;
import com.strong.sample.table.t_test.jpa.TableTestDAO;
import com.strong.sample.table.t_test.jpa.TableTestDO;
import com.strong.sample.table.t_test.jpa.TableTestDO_;
import com.strong.sample.table.t_test.model.TableTestCreateDTO;
import com.strong.sample.table.t_test.model.TableTestRetrieveDTO;
import com.strong.sample.table.t_test.model.TableTestUpdateDTO;
import com.strong.sample.table.t_test.model.TableTestVO;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.jpa.service.StrongServiceImpl;
import com.strong.utils.message.StrongMessageSource;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.strong.utils.jpa.JpaConstants.*;

/**
 * 测试表 数据处理Service实现类
 *
 * @author Simen
 * @date 2022-02-07 20:11:23
 */
@Slf4j
@Service
@CacheConfig(cacheNames = {TableTestConstants.TABLE_ENTITY})
public class TableTestServiceImpl extends StrongServiceImpl<TableTestDO, TableTestVO> implements TableTestService {

    /**
     * 注入的消息处理类
     */
    private final StrongMessageSource messageSource;

    /**
     * 注入的DAO类
     */
    private final TableTestDAO tableTestDAO;

    /**
     * 实例化
     *
     * @param messageSource 注入的消息处理类
     * @param tableTestDAO  注入的DAO类
     */
    @Autowired
    public TableTestServiceImpl(StrongMessageSource messageSource, TableTestDAO tableTestDAO) {
        this.messageSource = messageSource;
        this.tableTestDAO = tableTestDAO;
    }

    @Override
    @Cacheable(key = "#root.target + '_' + #root.methodName + '_' + #intId")
    public TableTestDO getRecord(final Integer intId) {
        log.debug(intId.toString());
        Assert.notNull(intId, MSG_QUERY_ID_EMPTY);
        return tableTestDAO.getReferenceById(intId);
    }

    @Override
    @CacheEvict(allEntries = true)
    public TableTestDO getCreateAction(final TableTestCreateDTO modelCreate) {
        log.debug(JSON.toJSONString(modelCreate));
        Assert.notNull(modelCreate, MSG_CONTENT_EMPTY);

        TableTestDO entity = BeanUtil.toBean(modelCreate, TableTestDO.class, CopyOptions.create().setIgnoreNullValue(true));
        // 初始化实体类ID
        entity.setTestId(StrongUtils.getNoRepeatId(tableTestDAO::getCountById));
        tableTestDAO.save(entity);
        return entity;
    }

    @Override
    @Cacheable(keyGenerator = "vabKeyGenerator")
    public List<TableTestDO> getAllList(final TableTestRetrieveDTO modelRetrieve) {
        log.debug(JSON.toJSONString(modelRetrieve));
        Assert.notNull(modelRetrieve, MSG_SEARCH_CONDITION_EMPTY);

        // 以表达式生成一个查询条件对象
        Specification<TableTestDO> specification = getSpecification(modelRetrieve);
        return tableTestDAO.findAll(specification, modelRetrieve.getSort());
    }

    @Override
    @Cacheable(keyGenerator = "vabKeyGenerator")
    public Page<TableTestDO> getPageList(final TableTestRetrieveDTO modelRetrieve) {
        log.debug(JSON.toJSONString(modelRetrieve, true));
        Assert.notNull(modelRetrieve, MSG_SEARCH_CONDITION_EMPTY);

        // 以表达式生成一个查询条件对象
        Specification<TableTestDO> specification = getSpecification(modelRetrieve);
        return tableTestDAO.findAll(specification, modelRetrieve.getPageable());
    }

    @Override
    @CacheEvict(allEntries = true)
    public TableTestDO getUpdateAction(final TableTestUpdateDTO modelUpdate) {
        log.debug(JSON.toJSONString(modelUpdate));
        Assert.notNull(modelUpdate, MSG_CONTENT_EMPTY);

        // 通过id字段获取待修改的实体类（id字段不存在，需修改为对应字段）
        TableTestDO entity = tableTestDAO.getReferenceById(modelUpdate.getTestId());
        Assert.notNull(entity, MSG_RECORD_NONENTITY);
        BeanUtil.copyProperties(modelUpdate, entity, CopyOptions.create().setIgnoreNullValue(true));
        tableTestDAO.save(entity);
        return entity;
    }

    @Override
    @Cacheable(key = "#root.target + '_' + #root.methodName + '_' + #intId")
    public TableTestDO getUpdateRecord(final Integer intId) {
        log.debug(intId.toString());
        Assert.notNull(intId, MSG_QUERY_ID_EMPTY);
        return tableTestDAO.getReferenceById(intId);
    }

    @Override
    @CacheEvict(allEntries = true)
    public Integer[] getDeleteAction(final Integer... intsId) {
        log.debug(ArrayUtil.toString(intsId));
        Assert.notEmpty(intsId, MSG_DELETE_RECORD_EMPTY);

        int[] intsTemp = new int[intsId.length];
        for (int i = 0; i < intsId.length; i++) {
            Assert.notNull(intsId[i], MSG_DELETE_RECORD_EMPTY);
            intsTemp[i] = intsId[i];
        }

        Integer intCount = tableTestDAO.getCountByIdIn(intsTemp);
        Assert.isTrue(intCount == intsId.length, MSG_DELETE_RECORD_BEEN_NULL);

        tableTestDAO.deleteAllByIdInBatch(Arrays.asList(intsId));
        return intsId;
    }

    // ===================内部=======================

    /**
     * 获取查询范式
     *
     * @return {@link Specification}<{@link TableTestDO}>
     */
    protected Specification<TableTestDO> getSpecification(final TableTestRetrieveDTO modelRetrieve) {
        return (root, query, criteriaBuilder) -> {
            Map<String, String> mapSearch = modelRetrieve.getMapSearch();
            if (MapUtil.isNotEmpty(mapSearch)) {
                List<Predicate> listPredicate = new ArrayList<>();

                String strLike = MapUtil.getStr(mapSearch, TableTestConstants.TEST_STR_ENTITY);
                if (StrUtil.isNotBlank(strLike)) {
                    listPredicate.add(criteriaBuilder.like(root.get(TableTestDO_.testStr), strLike));
                }

                return criteriaBuilder.and(ArrayUtil.toArray(listPredicate, Predicate.class));
            }
            return null;
        };
    }

    @Override
    @CacheEvict(value = TableTestConstants.TABLE_ENTITY, allEntries = true)
    public void cacheEvict() {

    }

    // ===================扩展=======================
}