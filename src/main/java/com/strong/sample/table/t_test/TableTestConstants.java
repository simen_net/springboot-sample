package com.strong.sample.table.t_test;

/**
 * 测试表 常量类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public class TableTestConstants {

    /**
     * 测试表 数据库表备注
     */
    public final static String TABLE_COMMENT = "测试表";

    /**
     * 测试表 数据库表名
     */
    public final static String TABLE_NAME = "T_TEST";

    /**
     * 测试表 表实体名
     */
    public final static String TABLE_ENTITY = "tableTest";

    /**
     * 测试表 表实体类模块名
     */
    public final static String TABLE_PACKAGE = "t_test";

    /**
     * 数据库字段名
     */
    public final static String TEST_ID_COLUMN = "TEST_ID";

    /**
     * 字段实体名
     */
    public final static String TEST_ID_ENTITY = "testId";

    /**
     * 字符串字段 数据库字段名
     */
    public final static String TEST_STR_COLUMN = "TEST_STR";

    /**
     * 字符串字段 字段实体名
     */
    public final static String TEST_STR_ENTITY = "testStr";

    /**
     * 整数字段 数据库字段名
     */
    public final static String TEST_INT_COLUMN = "TEST_INT";

    /**
     * 整数字段 字段实体名
     */
    public final static String TEST_INT_ENTITY = "testInt";

    /**
     * 日期字段 数据库字段名
     */
    public final static String TEST_DATE_COLUMN = "TEST_DATE";

    /**
     * 日期字段 字段实体名
     */
    public final static String TEST_DATE_ENTITY = "testDate";

    /**
     * 日期时间字段 数据库字段名
     */
    public final static String TEST_DATETIME_COLUMN = "TEST_DATETIME";

    /**
     * 日期时间字段 字段实体名
     */
    public final static String TEST_DATETIME_ENTITY = "testDatetime";

    /**
     * 浮点字段 数据库字段名
     */
    public final static String TEST_FLOAT_COLUMN = "TEST_FLOAT";

    /**
     * 浮点字段 字段实体名
     */
    public final static String TEST_FLOAT_ENTITY = "testFloat";

    /**
     * 布尔字段 数据库字段名
     */
    public final static String TEST_BOOL_COLUMN = "TEST_BOOL";

    /**
     * 布尔字段 字段实体名
     */
    public final static String TEST_BOOL_ENTITY = "testBool";

    /**
     * 长文本字段 数据库字段名
     */
    public final static String TEST_TEXT_COLUMN = "TEST_TEXT";

    /**
     * 长文本字段 字段实体名
     */
    public final static String TEST_TEXT_ENTITY = "testText";


// ===================扩展=======================
}
