package com.strong.sample.table.t_test.controller;

import com.strong.sample.table.t_test.TableTestConstants;
import com.strong.sample.table.t_test.jpa.TableTestDO;
import com.strong.sample.table.t_test.model.TableTestCreateDTO;
import com.strong.sample.table.t_test.model.TableTestRetrieveDTO;
import com.strong.sample.table.t_test.model.TableTestUpdateDTO;
import com.strong.sample.table.t_test.model.TableTestVO;
import com.strong.sample.table.t_test.service.TableTestService;
import com.strong.utils.JSON;
import com.strong.utils.mvc.ModuleJsonController;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.strong.utils.mvc.MvcConstants.PARAMETER_INTS_ID;
import static com.strong.utils.mvc.MvcConstants.PARAMETER_INT_ID;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_GENERAL;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_SUPER;

/**
 * 测试表 JSON控制器
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Slf4j
@RestController
@RequestMapping(value = TableTestConstants.TABLE_ENTITY)
public class TableTestJsonController implements ModuleJsonController<TableTestVO, TableTestCreateDTO, TableTestUpdateDTO, TableTestRetrieveDTO> {

    /**
     * 过滤排序条件
     */
    private final static String[] STRS_FILTER_SORT = {};

    /**
     * 过滤查询条件
     */
    private final static String[] STRS_FILTER_SEARCH = {};

    /**
     * 注入Service
     */
    private final TableTestService tableTestService;

    /**
     * 实例化
     *
     * @param tableTestService 注入的Service
     */
    @Autowired
    public TableTestJsonController(TableTestService tableTestService) {
        this.tableTestService = tableTestService;
    }

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVO<TableTestVO> getRecordView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableTestDO tableTestDO = tableTestService.getRecord(intId);
        TableTestVO tableTestVO = tableTestService.getModel(tableTestDO);
        return new ReplyVO<>(tableTestVO);
    }

    /**
     * 添加记录操作 控制器
     *
     * @param modeldCreate 待添加的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableTestVO> getCreateAction(@Valid @RequestBody final TableTestCreateDTO modeldCreate) {
        TableTestDO tableTestDO = tableTestService.getCreateAction(modeldCreate);
        TableTestVO tableTestVO = tableTestService.getModel(tableTestDO, TableTestVO.STRS_INCLUDE_PROPERTIES);
        System.out.println(JSON.toJSONString(tableTestVO));
        return new ReplyVO<>(tableTestVO, ReplyEnum.SUCCESS_CREATE_DATA);
    }

    /**
     * 执行修改的 控制器.
     *
     * @param modelUpdate 待修改的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableTestVO> getUpdateAction(@Valid @RequestBody final TableTestUpdateDTO modelUpdate) {
        TableTestDO tableTestDO = tableTestService.getUpdateAction(modelUpdate);
        TableTestVO tableTestVO = tableTestService.getModel(tableTestDO, TableTestVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(tableTestVO, ReplyEnum.SUCCESS_UPDATE_DATA);
    }

    /**
     * 显示待修改内容的 控制器.
     *
     * @param intId 显示对象的ID
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableTestVO> getUpdateView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableTestDO tableTestDO = tableTestService.getUpdateRecord(intId);
        TableTestVO tableTestVO = tableTestService.getModel(tableTestDO);
        return new ReplyVO<>(tableTestVO);
    }

    /**
     * 执行删除的 控制器.
     *
     * @param intsId 待删除id数组
     * @return 基于无数据的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId) {
        tableTestService.getDeleteAction(intsId);
        return new ReplyVO<>(ReplyEnum.SUCCESS_DELETE_DATA);
    }

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型队列的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableTestVO> getListView(@RequestBody final TableTestRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        List<TableTestDO> listTableTestDO = tableTestService.getAllList(modelRetrieve);
        List<TableTestVO> listTableTestVO = tableTestService.getModelList(listTableTestDO, TableTestVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableTestVO);
    }

    /**
     * 显示分页列表的 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型分页对象的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableTestVO> getPageView(@RequestBody final TableTestRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        System.out.println(JSON.toJSONString(modelRetrieve, true));
        Page<TableTestDO> pageTableTestDO = tableTestService.getPageList(modelRetrieve);
        List<TableTestVO> listTableTestVO = tableTestService.getModelList(pageTableTestDO.getContent(), TableTestVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableTestVO, pageTableTestDO.getTotalElements());
    }


    // ===================扩展=======================
}
