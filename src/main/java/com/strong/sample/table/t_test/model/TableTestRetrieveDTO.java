package com.strong.sample.table.t_test.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.strong.utils.mvc.pojo.retrieve.VabDTO;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 测试表 查询模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonPropertyOrder(alphabetic = true)
public class TableTestRetrieveDTO extends VabDTO<TableTestVO> {

}