package com.strong.sample.table.t_user.jpa;

import com.strong.sample.table.t_user.TableUserConstants;
import com.strong.utils.annotation.StrongRemark;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Objects;

/**
 * DO实体类
 *
 * @author Simen
 * @date 2024-10-22 10:21:39
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = TableUserConstants.TABLE_NAME)
public class TableUserDO implements java.io.Serializable {

    @Id
    @Column(name = TableUserConstants.USER_ID_COLUMN, unique = true, nullable = false)
    @StrongRemark("系统随机生成固定编号")
    private Integer userId;

    @Column(name = TableUserConstants.USER_NAME_COLUMN, length = 64, nullable = false)
    @StrongRemark("用户名")
    private String userName;

    @Column(name = TableUserConstants.USER_LOGIN_NAME_COLUMN, length = 64, unique = true, nullable = false)
    @StrongRemark("用户登录名")
    private String userLoginName;

    @Column(name = TableUserConstants.USER_LOGIN_PASSWORD_COLUMN, length = 1024, nullable = false)
    @StrongRemark("用户登录密码")
    private String userLoginPassword;

    @Column(name = TableUserConstants.USER_LOGIN_SALT_COLUMN, length = 1024, nullable = false)
    @StrongRemark("用户登录密码加盐")
    private String userLoginSalt;

    @Column(name = TableUserConstants.USER_AUTHORITY_COLUMN, length = 128)
    @StrongRemark("用户权限")
    private String userAuthority;

    @Column(name = TableUserConstants.USER_GROUP_COLUMN, length = 128)
    @StrongRemark("用户分组")
    private String userGroup;

    @Column(name = TableUserConstants.USER_TEAM_ORG_ID_COLUMN, length = 64, nullable = false)
    @StrongRemark("[外键]对应运动队编号-关联T_TEAM外键")
    private String userTeamOrgId;

    @Column(name = TableUserConstants.USER_UNIT_ID_COLUMN, nullable = false)
    @StrongRemark("[外键]用户所属单位-关联单位表主键")
    private Integer userUnitId;

    @Column(name = TableUserConstants.USER_ID_TYPE_COLUMN, length = 32)
    @StrongRemark("证件类型")
    private String userIdType;

    @Column(name = TableUserConstants.USER_ID_NUMBER_COLUMN, length = 32)
    @StrongRemark("证件号码")
    private String userIdNumber;

    @Column(name = TableUserConstants.USER_MOBILE_PHONE_COLUMN, length = 20)
    @StrongRemark("用户手机号码")
    private String userMobilePhone;

    @Column(name = TableUserConstants.USER_TELEPHONE_COLUMN, length = 20)
    @StrongRemark("用户固定电话")
    private String userTelephone;

    @Column(name = TableUserConstants.USER_EMAIL_COLUMN, length = 128)
    @StrongRemark("用户邮箱地址")
    private String userEmail;

    @Column(name = TableUserConstants.USER_QQ_CODE_COLUMN, length = 20)
    @StrongRemark("用户QQ号码")
    private String userQqCode;

    @Column(name = TableUserConstants.USER_WX_CODE_COLUMN, length = 20)
    @StrongRemark("用户微信号码")
    private String userWxCode;

    @Column(name = TableUserConstants.USER_LOGIN_RECORD_COLUMN)
    @StrongRemark("用户登录记录")
    private String userLoginRecord;

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Hibernate.getClass(this) != Hibernate.getClass(obj)) {
            return false;
        }
        TableUserDO that = (TableUserDO) obj;
        return userId != null && Objects.equals(userId, that.userId);
    }

    // ===================扩展=======================
}

