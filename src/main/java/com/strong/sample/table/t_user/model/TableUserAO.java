package com.strong.sample.table.t_user.model;

import com.strong.utils.annotation.StrongRemark;
import lombok.Data;

/**
 * 基础模型类
 * <p>
 * 分层领域模型规约：
 * DO（ Data Object）：与数据库表结构一一对应，通过DAO层向上传输数据源对象。
 * DTO（ Data Transfer Object）：数据传输对象，Service或Manager向外传输的对象。
 * BO（ Business Object）：业务对象。 由Service层输出的封装业务逻辑的对象。
 * AO（ Application Object）：应用对象。 在Web层与Service层之间抽象的复用对象模型，极为贴近展示层，复用度不高。
 * VO（ View Object）：显示层对象，通常是Web向模板渲染引擎层传输的对象。
 * POJO（ Plain Ordinary Java Object）：在本手册中， POJO专指只有setter/getter/toString的简单类，包括DO/DTO/BO/VO等。
 * Query：数据查询对象，各层接收上层的查询请求。 注意超过2个参数的查询封装，禁止使用Map类来传输。
 * <p>
 * 领域模型命名规约：
 * 数据对象：xxxDO，xxx即为数据表名。
 * 数据传输对象：xxxDTO，xxx为业务领域相关的名称。
 * 展示对象：xxxVO，xxx一般为网页名称。
 * POJO是DO/DTO/BO/VO的统称，禁止命名成xxxPOJO。
 *
 * @author Simen
 * @date 2024-10-22 10:21:39
 */
@Data
class TableUserAO {
    /**
     * <pre>
     *   @AssertFalse Boolean, boolean 验证注解的元素值是false
     *   @AssertTrue Boolean, boolean 验证注解的元素值是true
     *   @NotNull 任意类型  验证注解的元素值不是null
     *   @Null 任意类型  验证注解的元素值是null
     *   @Min(value=值) BigDecimal，BigInteger, byte,short, int, long，等任何Number或CharSequence（存储的是数字）子类型  验证注解的元素值大于等于@Min指定的value值
     *   @Max（value=值） 和@Min要求一样 验证注解的元素值小于等于@Max指定的value值
     *   @DecimalMin(value=值) 和@Min要求一样 验证注解的元素值大于等于@ DecimalMin指定的value值
     *   @DecimalMax(value=值) 和@Min要求一样 验证注解的元素值小于等于@ DecimalMax指定的value值
     *   @Digits(integer=整数位数, fraction=小数位数)  和@Min要求一样 验证注解的元素值的整数位数和小数位数上限
     *   @Size(min=下限, max=上限) 字符串、Collection、Map、数组等  验证注解的元素值的在min和max（包含）指定区间之内，如字符长度、集合大小
     *   @Past java.util.Date, java.util.Calendar;Joda Time类库的日期类型  验证注解的元素值（日期类型）比当前时间早
     *   @Future 与@Past要求一样  验证注解的元素值（日期类型）比当前时间晚
     *   @NotBlank CharSequence子类型 验证注解的元素值不为空（不为null、去除首位空格后长度为0），不同于@NotEmpty，@NotBlank只应用于字符串且在比较时会去除字符串的首位空格
     *   @Length(min=下限, max=上限) CharSequence子类型 验证注解的元素值长度在min和max区间内
     *   @NotEmpty CharSequence子类型、Collection、Map、数组 验证注解的元素值不为null且不为空（字符串长度不为0、集合大小不为0）
     *   @Range(min=最小值, max=最大值)  BigDecimal,BigInteger,CharSequence, byte, short, int, long等原子类型和包装类型  验证注解的元素值在最小值和最大值之间
     *   @Email(regexp=正则表达式,flag=标志的模式) CharSequence子类型（如String）  验证注解的元素值是Email，也可以通过regexp和flag指定自定义的email格式
     *   @Pattern(regexp=正则表达式,flag=标志的模式) String，任何CharSequence的子类型 验证注解的元素值与指定的正则表达式匹配
     *   @Valid 任何非原子类型 指定递归验证关联的对象；如用户对象中有个地址对象属性，如果想在验证用户对象时一起验证地址对象的话，在地址对象上加@Valid注解即可级联验证
     * </pre>
     */

    @StrongRemark("系统随机生成固定编号")
    private Integer userId;

    @StrongRemark("用户名")
    private String userName;

    @StrongRemark("用户登录名")
    private String userLoginName;

    @StrongRemark("用户登录密码")
    private String userLoginPassword;

    @StrongRemark("用户登录密码加盐")
    private String userLoginSalt;

    @StrongRemark("用户权限")
    private String userAuthority;

    @StrongRemark("用户分组")
    private String userGroup;

    @StrongRemark("[外键]对应运动队编号-关联T_TEAM外键")
    private String userTeamOrgId;

    @StrongRemark("[外键]用户所属单位-关联单位表主键")
    private Integer userUnitId;

    @StrongRemark("证件类型")
    private String userIdType;

    @StrongRemark("证件号码")
    private String userIdNumber;

    @StrongRemark("用户手机号码")
    private String userMobilePhone;

    @StrongRemark("用户固定电话")
    private String userTelephone;

    @StrongRemark("用户邮箱地址")
    private String userEmail;

    @StrongRemark("用户QQ号码")
    private String userQqCode;

    @StrongRemark("用户微信号码")
    private String userWxCode;

    @StrongRemark("用户登录记录")
    private String userLoginRecord;


    // ===================扩展=======================

}