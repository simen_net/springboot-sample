package com.strong.sample.table.t_user;

/**
 * 常量类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public class TableUserConstants {

    /**
     * 数据库表备注
     */
    public final static String TABLE_COMMENT = "";

    /**
     * 数据库表名
     */
    public final static String TABLE_NAME = "T_USER";

    /**
     * 表实体名
     */
    public final static String TABLE_ENTITY = "tableUser";

    /**
     * 表实体类模块名
     */
    public final static String TABLE_PACKAGE = "t_user";

    /**
     * 系统随机生成固定编号 字段备注
     */
    public final static String USER_ID_COMMENT = "系统随机生成固定编号";

    /**
     * 系统随机生成固定编号 数据库字段名
     */
    public final static String USER_ID_COLUMN = "USER_ID";

    /**
     * 系统随机生成固定编号 字段实体名
     */
    public final static String USER_ID_ENTITY = "userId";

    /**
     * 用户名 字段备注
     */
    public final static String USER_NAME_COMMENT = "用户名";

    /**
     * 用户名 数据库字段名
     */
    public final static String USER_NAME_COLUMN = "USER_NAME";

    /**
     * 用户名 字段实体名
     */
    public final static String USER_NAME_ENTITY = "userName";

    /**
     * 用户登录名 字段备注
     */
    public final static String USER_LOGIN_NAME_COMMENT = "用户登录名";

    /**
     * 用户登录名 数据库字段名
     */
    public final static String USER_LOGIN_NAME_COLUMN = "USER_LOGIN_NAME";

    /**
     * 用户登录名 字段实体名
     */
    public final static String USER_LOGIN_NAME_ENTITY = "userLoginName";

    /**
     * 用户登录密码 字段备注
     */
    public final static String USER_LOGIN_PASSWORD_COMMENT = "用户登录密码";

    /**
     * 用户登录密码 数据库字段名
     */
    public final static String USER_LOGIN_PASSWORD_COLUMN = "USER_LOGIN_PASSWORD";

    /**
     * 用户登录密码 字段实体名
     */
    public final static String USER_LOGIN_PASSWORD_ENTITY = "userLoginPassword";

    /**
     * 用户登录密码加盐 字段备注
     */
    public final static String USER_LOGIN_SALT_COMMENT = "用户登录密码加盐";

    /**
     * 用户登录密码加盐 数据库字段名
     */
    public final static String USER_LOGIN_SALT_COLUMN = "USER_LOGIN_SALT";

    /**
     * 用户登录密码加盐 字段实体名
     */
    public final static String USER_LOGIN_SALT_ENTITY = "userLoginSalt";

    /**
     * 用户权限 字段备注
     */
    public final static String USER_AUTHORITY_COMMENT = "用户权限";

    /**
     * 用户权限 数据库字段名
     */
    public final static String USER_AUTHORITY_COLUMN = "USER_AUTHORITY";

    /**
     * 用户权限 字段实体名
     */
    public final static String USER_AUTHORITY_ENTITY = "userAuthority";

    /**
     * 用户分组 字段备注
     */
    public final static String USER_GROUP_COMMENT = "用户分组";

    /**
     * 用户分组 数据库字段名
     */
    public final static String USER_GROUP_COLUMN = "USER_GROUP";

    /**
     * 用户分组 字段实体名
     */
    public final static String USER_GROUP_ENTITY = "userGroup";

    /**
     * [外键]对应运动队编号-关联T_TEAM外键 字段备注
     */
    public final static String USER_TEAM_ORG_ID_COMMENT = "[外键]对应运动队编号-关联T_TEAM外键";

    /**
     * [外键]对应运动队编号-关联T_TEAM外键 数据库字段名
     */
    public final static String USER_TEAM_ORG_ID_COLUMN = "USER_TEAM_ORG_ID";

    /**
     * [外键]对应运动队编号-关联T_TEAM外键 字段实体名
     */
    public final static String USER_TEAM_ORG_ID_ENTITY = "userTeamOrgId";

    /**
     * [外键]用户所属单位-关联单位表主键 字段备注
     */
    public final static String USER_UNIT_ID_COMMENT = "[外键]用户所属单位-关联单位表主键";

    /**
     * [外键]用户所属单位-关联单位表主键 数据库字段名
     */
    public final static String USER_UNIT_ID_COLUMN = "USER_UNIT_ID";

    /**
     * [外键]用户所属单位-关联单位表主键 字段实体名
     */
    public final static String USER_UNIT_ID_ENTITY = "userUnitId";

    /**
     * 证件类型 字段备注
     */
    public final static String USER_ID_TYPE_COMMENT = "证件类型";

    /**
     * 证件类型 数据库字段名
     */
    public final static String USER_ID_TYPE_COLUMN = "USER_ID_TYPE";

    /**
     * 证件类型 字段实体名
     */
    public final static String USER_ID_TYPE_ENTITY = "userIdType";

    /**
     * 证件号码 字段备注
     */
    public final static String USER_ID_NUMBER_COMMENT = "证件号码";

    /**
     * 证件号码 数据库字段名
     */
    public final static String USER_ID_NUMBER_COLUMN = "USER_ID_NUMBER";

    /**
     * 证件号码 字段实体名
     */
    public final static String USER_ID_NUMBER_ENTITY = "userIdNumber";

    /**
     * 用户手机号码 字段备注
     */
    public final static String USER_MOBILE_PHONE_COMMENT = "用户手机号码";

    /**
     * 用户手机号码 数据库字段名
     */
    public final static String USER_MOBILE_PHONE_COLUMN = "USER_MOBILE_PHONE";

    /**
     * 用户手机号码 字段实体名
     */
    public final static String USER_MOBILE_PHONE_ENTITY = "userMobilePhone";

    /**
     * 用户固定电话 字段备注
     */
    public final static String USER_TELEPHONE_COMMENT = "用户固定电话";

    /**
     * 用户固定电话 数据库字段名
     */
    public final static String USER_TELEPHONE_COLUMN = "USER_TELEPHONE";

    /**
     * 用户固定电话 字段实体名
     */
    public final static String USER_TELEPHONE_ENTITY = "userTelephone";

    /**
     * 用户邮箱地址 字段备注
     */
    public final static String USER_EMAIL_COMMENT = "用户邮箱地址";

    /**
     * 用户邮箱地址 数据库字段名
     */
    public final static String USER_EMAIL_COLUMN = "USER_EMAIL";

    /**
     * 用户邮箱地址 字段实体名
     */
    public final static String USER_EMAIL_ENTITY = "userEmail";

    /**
     * 用户QQ号码 字段备注
     */
    public final static String USER_QQ_CODE_COMMENT = "用户QQ号码";

    /**
     * 用户QQ号码 数据库字段名
     */
    public final static String USER_QQ_CODE_COLUMN = "USER_QQ_CODE";

    /**
     * 用户QQ号码 字段实体名
     */
    public final static String USER_QQ_CODE_ENTITY = "userQqCode";

    /**
     * 用户微信号码 字段备注
     */
    public final static String USER_WX_CODE_COMMENT = "用户微信号码";

    /**
     * 用户微信号码 数据库字段名
     */
    public final static String USER_WX_CODE_COLUMN = "USER_WX_CODE";

    /**
     * 用户微信号码 字段实体名
     */
    public final static String USER_WX_CODE_ENTITY = "userWxCode";

    /**
     * 用户登录记录 字段备注
     */
    public final static String USER_LOGIN_RECORD_COMMENT = "用户登录记录";

    /**
     * 用户登录记录 数据库字段名
     */
    public final static String USER_LOGIN_RECORD_COLUMN = "USER_LOGIN_RECORD";

    /**
     * 用户登录记录 字段实体名
     */
    public final static String USER_LOGIN_RECORD_ENTITY = "userLoginRecord";

    // ===================扩展=======================

}
