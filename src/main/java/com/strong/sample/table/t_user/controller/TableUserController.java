package com.strong.sample.table.t_user.controller;

import com.strong.sample.table.t_user.TableUserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * HTML控制器
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
@Slf4j
@Controller
@RequestMapping(value = TableUserConstants.TABLE_ENTITY)
public class TableUserController {

}
