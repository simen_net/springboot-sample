package com.strong.sample.table.t_user.model;

import com.strong.utils.mvc.pojo.retrieve.VabDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 查询模型类
 *
 * @author Simen
 * @date 2024-10-22 10:21:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TableUserRetrieveDTO extends VabDTO<TableUserVO> {

}