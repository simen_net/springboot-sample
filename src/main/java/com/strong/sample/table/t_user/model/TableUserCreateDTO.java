package com.strong.sample.table.t_user.model;

import com.strong.utils.annotation.StrongRemark;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import static cn.hutool.core.lang.RegexPool.MOBILE;
import static com.strong.sample.table.t_user.TableUserConstants.*;
import static com.strong.utils.constants.MessageConstants.*;
import static com.strong.utils.constants.RegexConstants.*;

/**
 * 添加模型类
 *
 * @author Simen
 * @date 2024-11-13 18:05:57
 */
@Data
public class TableUserCreateDTO {

    /**
     *
     <pre>
     *     @Null 被注释的元素必须为null
     *     @NotNull 被注释的元素不能为null
     *     @AssertTrue 被注释的元素必须为true
     *     @AssertFalse 被注释的元素必须为false
     *     @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @Size(max,min) 被注释的元素的大小必须在指定的范围内。
     *     @Digits(integer,fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
     *     @Past 被注释的元素必须是一个过去的日期
     *     @Future 被注释的元素必须是一个将来的日期
     *     @Pattern(value) 被注释的元素必须符合指定的正则表达式。
     *     @Email 被注释的元素必须是电子邮件地址
     *     @Length 被注释的字符串的大小必须在指定的范围内
     *     @NotEmpty 被注释的字符串必须非空
     *     @Range 被注释的元素必须在合适的范围内
     * </pre>
     */


    /**
     * 用户名 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 64)
    @StrongRemark(USER_NAME_COMMENT)
    private String userName;

    /**
     * 用户登录名 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 64)
    @Pattern(regexp = REGEX_USER_LOGIN_NAME, message = ERROR_LOGIN_NAME_NO_STANDARD)
    @StrongRemark(USER_LOGIN_NAME_COMMENT)
    private String userLoginName;

    /**
     * 用户登录密码 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 1024)
    @Pattern(regexp = REGEX_LETTERS_AND_NUMBERS, message = ERROR_PASSWORD_NO_STANDARD)
    @StrongRemark(USER_LOGIN_PASSWORD_COMMENT)
    private String userLoginPassword;

    /**
     * 用户登录密码加盐 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 1024)
    @StrongRemark(USER_LOGIN_SALT_COMMENT)
    private String userLoginSalt;

    /**
     * 用户权限 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, max = 1024)
    @StrongRemark(USER_AUTHORITY_COMMENT)
    private String userAuthority;

    /**
     * 用户分组 字段
     */
    @Size(message = ERROR_SIZE, max = 128)
    @StrongRemark(USER_GROUP_COMMENT)
    private String userGroup;

    /**
     * [外键]对应运动队编号-关联T_TEAM外键 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 64)
    @StrongRemark(USER_TEAM_ORG_ID_COMMENT)
    private String userTeamOrgId;

    /**
     * [外键]用户所属单位-关联单位表主键 字段
     */
    @StrongRemark(USER_UNIT_ID_COMMENT)
    private Integer userUnitId;

    /**
     * 证件类型 字段
     */
    @Size(message = ERROR_SIZE, max = 32)
    @StrongRemark(USER_ID_TYPE_COMMENT)
    private String userIdType;

    /**
     * 证件号码 字段
     */
    @Size(message = ERROR_SIZE, max = 32)
    @StrongRemark(USER_ID_NUMBER_COMMENT)
    private String userIdNumber;

    /**
     * 用户手机号码 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @Pattern(regexp = MOBILE, message = ERROR_MOBILE_NO_STANDARD)
    @StrongRemark(USER_MOBILE_PHONE_COMMENT)
    private String userMobilePhone;

    /**
     * 用户固定电话 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @Pattern(regexp = REGEX_TELEPHONE, message = ERROR_TELEPHONE_NO_STANDARD)
    @StrongRemark(USER_TELEPHONE_COMMENT)
    private String userTelephone;

    /**
     * 用户邮箱地址 字段
     */
    @Size(message = ERROR_SIZE, max = 128)
    @Pattern(regexp = REGEX_EMAIL, message = ERROR_EMAIL_NO_STANDARD)
    @StrongRemark(USER_EMAIL_COMMENT)
    private String userEmail;

    /**
     * 用户QQ号码 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @Pattern(regexp = REGEX_QQ, message = ERROR_QQ_NO_STANDARD)
    @StrongRemark(USER_QQ_CODE_COMMENT)
    private String userQqCode;

    /**
     * 用户微信号码 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @Pattern(regexp = REGEX_WX, message = ERROR_WX_NO_STANDARD)
    @StrongRemark(USER_WX_CODE_COMMENT)
    private String userWxCode;

    /**
     * 用户登录记录 字段
     */
    @StrongRemark(USER_LOGIN_RECORD_COMMENT)
    private String userLoginRecord;

    // ===================扩展=======================

}