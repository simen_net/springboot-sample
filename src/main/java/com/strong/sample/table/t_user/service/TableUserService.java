package com.strong.sample.table.t_user.service;

import com.strong.sample.table.t_user.jpa.TableUserDO;
import com.strong.sample.table.t_user.model.TableUserCreateDTO;
import com.strong.sample.table.t_user.model.TableUserRetrieveDTO;
import com.strong.sample.table.t_user.model.TableUserUpdateDTO;
import com.strong.sample.table.t_user.model.TableUserVO;
import com.strong.utils.jpa.service.StrongService;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 数据处理Service接口
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Transactional(rollbackFor = Exception.class)
public interface TableUserService extends StrongService<TableUserDO, TableUserVO> {

    /**
     * 事务隔离级别isolation：
     * @Transactional(isolation = Isolation.READ_UNCOMMITTED)：读取未提交数据(会出现脏读, 不可重复读) 基本不使用
     * @Transactional(isolation = Isolation.READ_COMMITTED)：读取已提交数据(会出现不可重复读和幻读)
     * @Transactional(isolation = Isolation.REPEATABLE_READ)：可重复读(会出现幻读)
     * @Transactional(isolation = Isolation.SERIALIZABLE)：串行化
     *
     * 事物传播行为propagation:
     * @Transactional(propagation=Propagation.REQUIRED) ：如果有事务, 那么加入事务, 没有的话新建一个(默认情况下)
     * @Transactional(propagation=Propagation.NOT_SUPPORTED) ：容器不为这个方法开启事务
     * @Transactional(propagation=Propagation.REQUIRES_NEW) ：不管是否存在事务,都创建一个新的事务,原来的挂起,新的执行完毕,继续执行老的事务
     * @Transactional(propagation=Propagation.MANDATORY) ：必须在一个已有的事务中执行,否则抛出异常
     * @Transactional(propagation=Propagation.NEVER) ：必须在一个没有的事务中执行,否则抛出异常(与Propagation.MANDATORY相反)
     * @Transactional(propagation=Propagation.SUPPORTS) ：如果其他bean调用这个方法,在其他bean中声明事务,那就用事务.如果其他bean没有声明事务,那就不用事务.
     * timeout:该属性用于设置事务的超时秒数，默认值为-1表示永不超时
     * readOnly：该属性用于设置当前事务是否为只读事务，设置为true表示只读，false则表示可读写，默认值为false。例如：@Transactional(readOnly=true)
     *
     * rollbackFor/rollbackForClassName：
     * 该属性用于设置需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，则进行事务回滚。例如：
     * 指定单一异常类：@Transactional(rollbackFor=RuntimeException.class)
     * 指定多个异常类：@Transactional(rollbackFor={RuntimeException.class, Exception.class})
     *
     * 该属性用于设置需要进行回滚的异常类名称数组，当方法中抛出指定异常名称数组中的异常时，则进行事务回滚。例如：
     * 指定单一异常类名称：@Transactional(rollbackForClassName="RuntimeException")
     * 指定多个异常类名称：@Transactional(rollbackForClassName={"RuntimeException","Exception"})
     *
     * noRollbackFor/noRollbackForClassName:
     * 该属性用于设置不需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，不进行事务回滚。例如：
     * 指定单一异常类：@Transactional(noRollbackFor=RuntimeException.class)
     * 指定多个异常类：@Transactional(noRollbackFor={RuntimeException.class, Exception.class})
     *
     * @Transactional 只能被应用到public方法上, 对于其它非public的方法,如果标记了@Transactional也不会报错,但方法没有事务功能
     */

    /**
     * 返回添加记录的模型.
     *
     * @param intId 待返回记录的ID
     * @return 操作结果模型（含数据模型对象）
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    TableUserDO getRecord(final Integer intId);

    /**
     * 返回添加记录的模型
     *
     * @param mtableUser 待保存的添加模型对象
     * @return 操作结果模型（默认只返回成功与否）
     */
    TableUserDO getCreateAction(final TableUserCreateDTO mtableUser);

    /**
     * 根据查询条件对象获取模型类的集合
     *
     * @param modelRetrieve 查询条件
     * @return 操作结果模型（含数据集合）
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    List<TableUserDO> getAllList(final TableUserRetrieveDTO modelRetrieve);

    /**
     * 根据查询条件对象获取模型类的分页集合
     *
     * @param modelRetrieve 查询条件
     * @return 操作结果模型（含分页对象）
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    Page<TableUserDO> getPageList(final TableUserRetrieveDTO modelRetrieve);

    /**
     * 返回修改记录的结果
     *
     * @param modelUpdate 待修改记录的查询条件
     * @return 操作结果模型（默认只返回成功与否）
     */
    TableUserDO getUpdateAction(final TableUserUpdateDTO modelUpdate);

    /**
     * 返回待修改的记录
     *
     * @param intId 待返回记录的ID
     * @return 操作结果模型（含待修改的模型对象）
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    TableUserDO getUpdateRecord(final Integer intId);

    /**
     * 执行批量删除操作.
     *
     * @param intsId 待删除的ID数组
     * @return 操作结果模型（默认只返回成功与否）
     */
    Integer[] getDeleteAction(final Integer... intsId);

    // ===================扩展=======================
}