package com.strong.sample.table.t_user.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * 数据库操作类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public interface TableUserDAO extends JpaRepository<TableUserDO, Integer>, JpaSpecificationExecutor<TableUserDO> {

    /**
     * <pre>
     *    And ---- findByLastnameAndFirstname ---- … where x.lastname = ?1 and x.firstname = ?2
     *    Or ---- findByLastnameOrFirstname ---- … where x.lastname = ?1 or x.firstname = ?2
     *    Is,Equals ---- findByFirstname,findByFirstnameIs,findByFirstnameEquals ---- … where x.firstname = 1?
     *    Between ---- findByStartDateBetween ---- … where x.startDate between 1? and ?2
     *    LessThan ---- findByAgeLessThan ---- … where x.age < ?1
     *    LessThanEqual ---- findByAgeLessThanEqual ---- … where x.age <= ?1
     *    GreaterThan ---- findByAgeGreaterThan ---- … where x.age > ?1
     *    GreaterThanEqual ---- findByAgeGreaterThanEqual ---- … where x.age >= ?1
     *    After ---- findByStartDateAfter ---- … where x.startDate > ?1
     *    Before ---- findByStartDateBefore ---- … where x.startDate < ?1
     *    IsNull ---- findByAgeIsNull ---- … where x.age is null
     *    IsNotNull,NotNull ---- findByAge(Is)NotNull ---- … where x.age not null
     *    Like ---- findByFirstnameLike ---- … where x.firstname like ?1
     *    NotLike ---- findByFirstnameNotLike ---- … where x.firstname not like ?1
     *    StartingWith ---- findByFirstnameStartingWith ---- … where x.firstname like ?1 (parameter bound with appended %)
     *    EndingWith ---- findByFirstnameEndingWith ---- … where x.firstname like ?1 (parameter bound with prepended %)
     *    Containing ---- findByFirstnameContaining ---- … where x.firstname like ?1 (parameter bound wrapped in %)
     *    OrderBy ---- findByAgeOrderByLastnameDesc ---- … where x.age = ?1 order by x.lastname desc
     *    Not ---- findByLastnameNot ---- … where x.lastname <> ?1
     *    In ---- findByAgeIn(Collection<Age> ages) ---- … where x.age in ?1
     *    NotIn ---- findByAgeNotIn(Collection<Age> age) ---- … where x.age not in ?1
     *    True ---- findByActiveTrue() ---- … where x.active = true
     *    False ---- findByActiveFalse() ---- … where x.active = false
     *    IgnoreCase ---- findByFirstnameIgnoreCase ---- … where UPPER(x.firstame) = UPPER(?1)
     * </pre>
     */

    /**
     * 查询ID在数组中的所有记录
     *
     * @param ids 实体类主键值数组
     * @return 实体类集合
     */
    @Query("select T from TableUserDO T where T.userId in ?1")
    List<TableUserDO> findAllByIdIn(Collection<Integer> ids);

    /**
     * 使用ID查询实体类
     *
     * @param id 实体类主键值
     * @return 单个实体类
     */
    TableUserDO getByUserIdEquals(Integer id);

    /**
     * 查询ID在数组中的所有记录
     *
     * @param ids 实体类主键值数组
     * @return 实体类集合
     */
    List<TableUserDO> findAllByUserIdIn(Integer[] ids);

    /**
     * 获取记录数量
     *
     * @return {@link Integer}
     */
    @Query("select count(T.userId) from TableUserDO T")
    Integer getCount();

    /**
     * 通过 主键id 获取记录数
     *
     * @param intId 实体类主键值
     * @return {@link Integer}
     */
    @Query("select count(T.userId) from TableUserDO T where T.userId = ?1")
    Integer getCountById(int intId);

    /**
     * 通过 主键id数组 获取记录数
     *
     * @param intsId 实体类主键值数组
     * @return {@link Integer}
     */
    @Query("select count(T.userId) from TableUserDO T where T.userId in ?1")
    Integer getCountByIdIn(int... intsId);

    // ===================扩展=======================

    /**
     * 通过 用户登录名 获取记录数
     *
     * @param strLoginName 用户登录名
     * @return {@link Integer}
     */
    @Query("select count(T.userId) from TableUserDO T where T.userLoginName = ?1")
    Integer getCountByLoginName(String strLoginName);

    /**
     * 通过用户登录名删除记录
     *
     * @param strUserLoginName STR用户登录名
     */
    @Modifying
    @Transactional
    @Query("delete from TableUserDO T where T.userLoginName = ?1")
    void deleteByUserLoginName(String strUserLoginName);
}