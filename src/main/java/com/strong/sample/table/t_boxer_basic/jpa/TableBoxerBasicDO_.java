package com.strong.sample.table.t_boxer_basic.jpa;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;

import java.util.Date;

/**
 * 运动员基本信息表 托管类
 *
 * @author Simen
 * @date 2024-11-20 11:05:53
 */
@StaticMetamodel(TableBoxerBasicDO.class)
public class TableBoxerBasicDO_ {
    /**
     * 系统随机生成固定编号
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, Integer> boxerId;

    /**
     * 运动员编号（不重复）
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerAthleteId;

    /**
     * 姓名
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerName;

    /**
     * 拼音首字母
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerPhoneticInitials;

    /**
     * 姓的拼音
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerPhoneticSurname;

    /**
     * 英文名
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerEnglishName;

    /**
     * [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerLocalRegisterInfoId;

    /**
     * [外键]运动员所在单位ID - 关联T_UNIT主键
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, Integer> boxerUnitId;

    /**
     * 身份证号码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerIdNumber;

    /**
     * 性别编码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGender;

    /**
     * 性别
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGenderName;

    /**
     * 民族编码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerNationality;

    /**
     * 民族
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerNationalityName;

    /**
     * 户籍所在地
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerNativePlace;

    /**
     * 出生日期
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, Date> boxerBirthday;

    /**
     * 文化程度编码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerEducation;

    /**
     * 文化程度
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerEducationName;

    /**
     * 监护人身份证号码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGuardianIdNumber;

    /**
     * 监护人手机号码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGuardianMobile;

    /**
     * 监护人姓名
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGuardianName;

    /**
     * 与被监护人关系
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGuardianRelation;

    /**
     * 健康状态代码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerHealth;

    /**
     * 健康状态内容
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerHealthContent;

    /**
     * 健康状态
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerHealthName;

    /**
     * 运动员状态编码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerStatus;

    /**
     * 运动员状态
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerStatusName;

    /**
     * 运动员级别编码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGrade;

    /**
     * 运动员级别
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerGradeName;

    /**
     *
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerType;

    /**
     * 电子邮箱
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerEmail;

    /**
     * 运动员手机号码
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, String> boxerMobile;

    /**
     * 是否有照片
     */
    public static volatile SingularAttribute<TableBoxerBasicDO, Boolean> boxerHasPhoto;

}