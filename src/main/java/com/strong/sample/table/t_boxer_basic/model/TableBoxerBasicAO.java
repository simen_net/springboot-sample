package com.strong.sample.table.t_boxer_basic.model;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.strong.utils.annotation.StrongRemark;
import lombok.Data;

import java.util.Date;

import static com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants.*;

/**
 * 运动员基本信息表 基础模型类
 * <p>
 * 分层领域模型规约：
 * DO（ Data Object）：与数据库表结构一一对应，通过DAO层向上传输数据源对象。
 * DTO（ Data Transfer Object）：数据传输对象，Service或Manager向外传输的对象。
 * BO（ Business Object）：业务对象。 由Service层输出的封装业务逻辑的对象。
 * AO（ Application Object）：应用对象。 在Web层与Service层之间抽象的复用对象模型，极为贴近展示层，复用度不高。
 * VO（ View Object）：显示层对象，通常是Web向模板渲染引擎层传输的对象。
 * POJO（ Plain Ordinary Java Object）：在本手册中， POJO专指只有setter/getter/toString的简单类，包括DO/DTO/BO/VO等。
 * Query：数据查询对象，各层接收上层的查询请求。 注意超过2个参数的查询封装，禁止使用Map类来传输。
 * <p>
 * 领域模型命名规约：
 * 数据对象：xxxDO，xxx即为数据表名。
 * 数据传输对象：xxxDTO，xxx为业务领域相关的名称。
 * 展示对象：xxxVO，xxx一般为网页名称。
 * POJO是DO/DTO/BO/VO的统称，禁止命名成xxxPOJO。
 *
 * @author Simen
 * @date 2024-11-20 11:05:53
 */
@Data
class TableBoxerBasicAO {
    /**
     * <pre>
     *   @AssertFalse Boolean, boolean 验证注解的元素值是false
     *   @AssertTrue Boolean, boolean 验证注解的元素值是true
     *   @NotNull 任意类型  验证注解的元素值不是null
     *   @Null 任意类型  验证注解的元素值是null
     *   @Min(value=值) BigDecimal，BigInteger, byte,short, int, long，等任何Number或CharSequence（存储的是数字）子类型  验证注解的元素值大于等于@Min指定的value值
     *   @Max（value=值） 和@Min要求一样 验证注解的元素值小于等于@Max指定的value值
     *   @DecimalMin(value=值) 和@Min要求一样 验证注解的元素值大于等于@ DecimalMin指定的value值
     *   @DecimalMax(value=值) 和@Min要求一样 验证注解的元素值小于等于@ DecimalMax指定的value值
     *   @Digits(integer=整数位数, fraction=小数位数)  和@Min要求一样 验证注解的元素值的整数位数和小数位数上限
     *   @Size(min=下限, max=上限) 字符串、Collection、Map、数组等  验证注解的元素值的在min和max（包含）指定区间之内，如字符长度、集合大小
     *   @Past java.util.Date, java.util.Calendar;Joda Time类库的日期类型  验证注解的元素值（日期类型）比当前时间早
     *   @Future 与@Past要求一样  验证注解的元素值（日期类型）比当前时间晚
     *   @NotBlank CharSequence子类型 验证注解的元素值不为空（不为null、去除首位空格后长度为0），不同于@NotEmpty，@NotBlank只应用于字符串且在比较时会去除字符串的首位空格
     *   @Length(min=下限, max=上限) CharSequence子类型 验证注解的元素值长度在min和max区间内
     *   @NotEmpty CharSequence子类型、Collection、Map、数组 验证注解的元素值不为null且不为空（字符串长度不为0、集合大小不为0）
     *   @Range(min=最小值, max=最大值)  BigDecimal,BigInteger,CharSequence, byte, short, int, long等原子类型和包装类型  验证注解的元素值在最小值和最大值之间
     *   @Email(regexp=正则表达式,flag=标志的模式) CharSequence子类型（如String）  验证注解的元素值是Email，也可以通过regexp和flag指定自定义的email格式
     *   @Pattern(regexp=正则表达式,flag=标志的模式) String，任何CharSequence的子类型 验证注解的元素值与指定的正则表达式匹配
     *   @Valid 任何非原子类型 指定递归验证关联的对象；如用户对象中有个地址对象属性，如果想在验证用户对象时一起验证地址对象的话，在地址对象上加@Valid注解即可级联验证
     * </pre>
     */

    @StrongRemark(BOXER_ID_COMMENT)
    private Integer boxerId;

    @StrongRemark(BOXER_ATHLETE_ID_COMMENT)
    private String boxerAthleteId;

    @StrongRemark(BOXER_NAME_COMMENT)
    private String boxerName;

    @StrongRemark(BOXER_PHONETIC_INITIALS_COMMENT)
    private String boxerPhoneticInitials;

    @StrongRemark(BOXER_PHONETIC_SURNAME_COMMENT)
    private String boxerPhoneticSurname;

    @StrongRemark(BOXER_ENGLISH_NAME_COMMENT)
    private String boxerEnglishName;

    @StrongRemark(BOXER_LOCAL_REGISTER_INFO_ID_COMMENT)
    private String boxerLocalRegisterInfoId;

    @StrongRemark(BOXER_UNIT_ID_COMMENT)
    private Integer boxerUnitId;

    @StrongRemark(BOXER_ID_NUMBER_COMMENT)
    private String boxerIdNumber;

    @StrongRemark(BOXER_GENDER_COMMENT)
    private String boxerGender;

    @StrongRemark(BOXER_GENDER_NAME_COMMENT)
    private String boxerGenderName;

    @StrongRemark(BOXER_NATIONALITY_COMMENT)
    private String boxerNationality;

    @StrongRemark(BOXER_NATIONALITY_NAME_COMMENT)
    private String boxerNationalityName;

    @StrongRemark(BOXER_NATIVE_PLACE_COMMENT)
    private String boxerNativePlace;

    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @StrongRemark(BOXER_BIRTHDAY_COMMENT)
    private Date boxerBirthday;

    @StrongRemark(BOXER_EDUCATION_COMMENT)
    private String boxerEducation;

    @StrongRemark(BOXER_EDUCATION_NAME_COMMENT)
    private String boxerEducationName;

    @StrongRemark(BOXER_GUARDIAN_ID_NUMBER_COMMENT)
    private String boxerGuardianIdNumber;

    @StrongRemark(BOXER_GUARDIAN_MOBILE_COMMENT)
    private String boxerGuardianMobile;

    @StrongRemark(BOXER_GUARDIAN_NAME_COMMENT)
    private String boxerGuardianName;

    @StrongRemark(BOXER_GUARDIAN_RELATION_COMMENT)
    private String boxerGuardianRelation;

    @StrongRemark(BOXER_HEALTH_COMMENT)
    private String boxerHealth;

    @StrongRemark(BOXER_HEALTH_CONTENT_COMMENT)
    private String boxerHealthContent;

    @StrongRemark(BOXER_HEALTH_NAME_COMMENT)
    private String boxerHealthName;

    @StrongRemark(BOXER_STATUS_COMMENT)
    private String boxerStatus;

    @StrongRemark(BOXER_STATUS_NAME_COMMENT)
    private String boxerStatusName;

    @StrongRemark(BOXER_GRADE_COMMENT)
    private String boxerGrade;

    @StrongRemark(BOXER_GRADE_NAME_COMMENT)
    private String boxerGradeName;

    @StrongRemark(BOXER_TYPE_COMMENT)
    private String boxerType;

    @StrongRemark(BOXER_EMAIL_COMMENT)
    private String boxerEmail;

    @StrongRemark(BOXER_MOBILE_COMMENT)
    private String boxerMobile;

    @StrongRemark(BOXER_HAS_PHOTO_COMMENT)
    private Boolean boxerHasPhoto;


    // ===================扩展=======================

}