package com.strong.sample.table.t_boxer_basic.controller;

import com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 运动员基本信息表 HTML控制器
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
@Slf4j
@Controller
@RequestMapping(value = TableBoxerBasicConstants.TABLE_ENTITY)
public class TableBoxerBasicController {

}
