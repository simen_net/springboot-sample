package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 级别 枚举
 *
 * @author simen
 * @date 2023/02/16
 */
@Getter
public enum BoxerGradeEnum {

    AG00("AG00", "未知"),
    AG01("AG01", "国际健"),
    AG02("AG02", "健将"),
    AG03("AG03", "一级"),
    AG04("AG04", "二级"),
    AG05("AG05", "三级"),
    AG06("AG06", "无");


    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回消息
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回值
     */
    BoxerGradeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
