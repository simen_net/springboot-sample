package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 运动员类型 枚举
 *
 * @author simen
 * @date 2023/02/16
 */
@Getter
public enum BoxerTypeEnum {

    NATIONAL_REG("NATIONAL_REG", "国家注册运动员"),
    UNIT_ADOLESCENT("UNIT_ADOLESCENT", "本单位青少年运动员");

    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回 值
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回消息
     */
    BoxerTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
