package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 民族 枚举
 *
 * @author simen
 * @date 2023/02/16
 */
@Getter
public enum BoxerNationalityEnum {
    N00("N00", "未知"),
    N01("N01", "汉"),
    N02("N02", "蒙古"),
    N03("N03", "回"),
    N04("N04", "藏"),
    N05("N05", "维吾尔"),
    N06("N06", "苗"),
    N07("N07", "彝"),
    N08("N08", "壮"),
    N09("N09", "布依"),
    N10("N10", "朝鲜"),
    N11("N11", "满"),
    N12("N12", "侗"),
    N13("N13", "瑶"),
    N14("N14", "白"),
    N15("N15", "土家"),
    N16("N16", "哈尼"),
    N17("N17", "哈萨克"),
    N18("N18", "傣"),
    N19("N19", "黎"),
    N20("N20", "傈僳"),
    N21("N21", "佤"),
    N22("N22", "畲"),
    N23("N23", "高山"),
    N24("N24", "拉祜"),
    N25("N25", "水"),
    N26("N26", "东乡"),
    N27("N27", "纳西"),
    N28("N28", "景颇"),
    N29("N29", "柯尔克孜"),
    N30("N30", "土"),
    N31("N31", "达斡尔"),
    N32("N32", "仫佬"),
    N33("N33", "羌"),
    N34("N34", "布朗"),
    N35("N35", "撒拉"),
    N36("N36", "毛南"),
    N37("N37", "仡佬"),
    N38("N38", "锡伯"),
    N39("N39", "阿昌"),
    N40("N40", "普米"),
    N41("N41", "塔吉克"),
    N42("N42", "怒"),
    N43("N43", "乌孜别克"),
    N44("N44", "俄罗斯"),
    N45("N45", "鄂温克"),
    N46("N46", "德昂"),
    N47("N47", "保安"),
    N48("N48", "裕固"),
    N49("N49", "京"),
    N50("N50", "塔塔尔"),
    N51("N51", "独龙"),
    N52("N52", "鄂伦春"),
    N53("N53", "赫哲"),
    N54("N54", "门巴"),
    N55("N55", "珞巴"),
    N56("N56", "基诺"),
    N57("N57", "穿青");

    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回消息
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回值
     */
    BoxerNationalityEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
