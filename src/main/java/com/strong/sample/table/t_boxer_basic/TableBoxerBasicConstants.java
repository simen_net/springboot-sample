package com.strong.sample.table.t_boxer_basic;

/**
 * 运动员基本信息表 常量类
 *
 * @author Simen
 * @date 2022-02-26 11:47:09
 */
public class TableBoxerBasicConstants {

    /**
     * 运动员基本信息表 数据库表备注
     */
    public final static String TABLE_COMMENT = "运动员基本信息表";

    /**
     * 运动员基本信息表 数据库表名
     */
    public final static String TABLE_NAME = "T_BOXER_BASIC";

    /**
     * 运动员基本信息表 表实体名
     */
    public final static String TABLE_ENTITY = "tableBoxerBasic";

    /**
     * 运动员基本信息表 表实体类模块名
     */
    public final static String TABLE_PACKAGE = "t_boxer_basic";

    /**
     * 系统随机生成固定编号 字段备注
     */
    public final static String BOXER_ID_COMMENT = "系统随机生成固定编号";

    /**
     * 系统随机生成固定编号 数据库字段名
     */
    public final static String BOXER_ID_COLUMN = "BOXER_ID";

    /**
     * 系统随机生成固定编号 字段实体名
     */
    public final static String BOXER_ID_ENTITY = "boxerId";

    /**
     * 运动员编号（不重复） 字段备注
     */
    public final static String BOXER_ATHLETE_ID_COMMENT = "运动员编号（不重复）";

    /**
     * 运动员编号（不重复） 数据库字段名
     */
    public final static String BOXER_ATHLETE_ID_COLUMN = "BOXER_ATHLETE_ID";

    /**
     * 运动员编号（不重复） 字段实体名
     */
    public final static String BOXER_ATHLETE_ID_ENTITY = "boxerAthleteId";

    /**
     * 姓名 字段备注
     */
    public final static String BOXER_NAME_COMMENT = "姓名";

    /**
     * 姓名 数据库字段名
     */
    public final static String BOXER_NAME_COLUMN = "BOXER_NAME";

    /**
     * 姓名 字段实体名
     */
    public final static String BOXER_NAME_ENTITY = "boxerName";

    /**
     * 拼音首字母 字段备注
     */
    public final static String BOXER_PHONETIC_INITIALS_COMMENT = "拼音首字母";

    /**
     * 拼音首字母 数据库字段名
     */
    public final static String BOXER_PHONETIC_INITIALS_COLUMN = "BOXER_PHONETIC_INITIALS";

    /**
     * 拼音首字母 字段实体名
     */
    public final static String BOXER_PHONETIC_INITIALS_ENTITY = "boxerPhoneticInitials";

    /**
     * 姓的拼音 字段备注
     */
    public final static String BOXER_PHONETIC_SURNAME_COMMENT = "姓的拼音";

    /**
     * 姓的拼音 数据库字段名
     */
    public final static String BOXER_PHONETIC_SURNAME_COLUMN = "BOXER_PHONETIC_SURNAME";

    /**
     * 姓的拼音 字段实体名
     */
    public final static String BOXER_PHONETIC_SURNAME_ENTITY = "boxerPhoneticSurname";

    /**
     * 英文名 字段备注
     */
    public final static String BOXER_ENGLISH_NAME_COMMENT = "英文名";

    /**
     * 英文名 数据库字段名
     */
    public final static String BOXER_ENGLISH_NAME_COLUMN = "BOXER_ENGLISH_NAME";

    /**
     * 英文名 字段实体名
     */
    public final static String BOXER_ENGLISH_NAME_ENTITY = "boxerEnglishName";

    /**
     * [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键 字段备注
     */
    public final static String BOXER_LOCAL_REGISTER_INFO_ID_COMMENT = "[外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键";

    /**
     * [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键 数据库字段名
     */
    public final static String BOXER_LOCAL_REGISTER_INFO_ID_COLUMN = "BOXER_LOCAL_REGISTER_INFO_ID";

    /**
     * [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键 字段实体名
     */
    public final static String BOXER_LOCAL_REGISTER_INFO_ID_ENTITY = "boxerLocalRegisterInfoId";

    /**
     * [外键]运动员所在单位ID - 关联T_UNIT主键 字段备注
     */
    public final static String BOXER_UNIT_ID_COMMENT = "[外键]运动员所在单位ID - 关联T_UNIT主键";

    /**
     * [外键]运动员所在单位ID - 关联T_UNIT主键 数据库字段名
     */
    public final static String BOXER_UNIT_ID_COLUMN = "BOXER_UNIT_ID";

    /**
     * [外键]运动员所在单位ID - 关联T_UNIT主键 字段实体名
     */
    public final static String BOXER_UNIT_ID_ENTITY = "boxerUnitId";

    /**
     * 身份证号码 字段备注
     */
    public final static String BOXER_ID_NUMBER_COMMENT = "身份证号码";

    /**
     * 身份证号码 数据库字段名
     */
    public final static String BOXER_ID_NUMBER_COLUMN = "BOXER_ID_NUMBER";

    /**
     * 身份证号码 字段实体名
     */
    public final static String BOXER_ID_NUMBER_ENTITY = "boxerIdNumber";

    /**
     * 性别编码 字段备注
     */
    public final static String BOXER_GENDER_COMMENT = "性别编码";

    /**
     * 性别编码 数据库字段名
     */
    public final static String BOXER_GENDER_COLUMN = "BOXER_GENDER";

    /**
     * 性别编码 字段实体名
     */
    public final static String BOXER_GENDER_ENTITY = "boxerGender";

    /**
     * 性别 字段备注
     */
    public final static String BOXER_GENDER_NAME_COMMENT = "性别";

    /**
     * 性别 数据库字段名
     */
    public final static String BOXER_GENDER_NAME_COLUMN = "BOXER_GENDER_NAME";

    /**
     * 性别 字段实体名
     */
    public final static String BOXER_GENDER_NAME_ENTITY = "boxerGenderName";

    /**
     * 民族编码 字段备注
     */
    public final static String BOXER_NATIONALITY_COMMENT = "民族编码";

    /**
     * 民族编码 数据库字段名
     */
    public final static String BOXER_NATIONALITY_COLUMN = "BOXER_NATIONALITY";

    /**
     * 民族编码 字段实体名
     */
    public final static String BOXER_NATIONALITY_ENTITY = "boxerNationality";

    /**
     * 民族 字段备注
     */
    public final static String BOXER_NATIONALITY_NAME_COMMENT = "民族";

    /**
     * 民族 数据库字段名
     */
    public final static String BOXER_NATIONALITY_NAME_COLUMN = "BOXER_NATIONALITY_NAME";

    /**
     * 民族 字段实体名
     */
    public final static String BOXER_NATIONALITY_NAME_ENTITY = "boxerNationalityName";

    /**
     * 户籍所在地 字段备注
     */
    public final static String BOXER_NATIVE_PLACE_COMMENT = "户籍所在地";

    /**
     * 户籍所在地 数据库字段名
     */
    public final static String BOXER_NATIVE_PLACE_COLUMN = "BOXER_NATIVE_PLACE";

    /**
     * 户籍所在地 字段实体名
     */
    public final static String BOXER_NATIVE_PLACE_ENTITY = "boxerNativePlace";

    /**
     * 出生日期 字段备注
     */
    public final static String BOXER_BIRTHDAY_COMMENT = "出生日期";

    /**
     * 出生日期 数据库字段名
     */
    public final static String BOXER_BIRTHDAY_COLUMN = "BOXER_BIRTHDAY";

    /**
     * 出生日期 字段实体名
     */
    public final static String BOXER_BIRTHDAY_ENTITY = "boxerBirthday";

    /**
     * 文化程度编码 字段备注
     */
    public final static String BOXER_EDUCATION_COMMENT = "文化程度编码";

    /**
     * 文化程度编码 数据库字段名
     */
    public final static String BOXER_EDUCATION_COLUMN = "BOXER_EDUCATION";

    /**
     * 文化程度编码 字段实体名
     */
    public final static String BOXER_EDUCATION_ENTITY = "boxerEducation";

    /**
     * 文化程度 字段备注
     */
    public final static String BOXER_EDUCATION_NAME_COMMENT = "文化程度";

    /**
     * 文化程度 数据库字段名
     */
    public final static String BOXER_EDUCATION_NAME_COLUMN = "BOXER_EDUCATION_NAME";

    /**
     * 文化程度 字段实体名
     */
    public final static String BOXER_EDUCATION_NAME_ENTITY = "boxerEducationName";

    /**
     * 监护人身份证号码 字段备注
     */
    public final static String BOXER_GUARDIAN_ID_NUMBER_COMMENT = "监护人身份证号码";

    /**
     * 监护人身份证号码 数据库字段名
     */
    public final static String BOXER_GUARDIAN_ID_NUMBER_COLUMN = "BOXER_GUARDIAN_ID_NUMBER";

    /**
     * 监护人身份证号码 字段实体名
     */
    public final static String BOXER_GUARDIAN_ID_NUMBER_ENTITY = "boxerGuardianIdNumber";

    /**
     * 监护人手机号码 字段备注
     */
    public final static String BOXER_GUARDIAN_MOBILE_COMMENT = "监护人手机号码";

    /**
     * 监护人手机号码 数据库字段名
     */
    public final static String BOXER_GUARDIAN_MOBILE_COLUMN = "BOXER_GUARDIAN_MOBILE";

    /**
     * 监护人手机号码 字段实体名
     */
    public final static String BOXER_GUARDIAN_MOBILE_ENTITY = "boxerGuardianMobile";

    /**
     * 监护人姓名 字段备注
     */
    public final static String BOXER_GUARDIAN_NAME_COMMENT = "监护人姓名";

    /**
     * 监护人姓名 数据库字段名
     */
    public final static String BOXER_GUARDIAN_NAME_COLUMN = "BOXER_GUARDIAN_NAME";

    /**
     * 监护人姓名 字段实体名
     */
    public final static String BOXER_GUARDIAN_NAME_ENTITY = "boxerGuardianName";

    /**
     * 与被监护人关系 字段备注
     */
    public final static String BOXER_GUARDIAN_RELATION_COMMENT = "与被监护人关系";

    /**
     * 与被监护人关系 数据库字段名
     */
    public final static String BOXER_GUARDIAN_RELATION_COLUMN = "BOXER_GUARDIAN_RELATION";

    /**
     * 与被监护人关系 字段实体名
     */
    public final static String BOXER_GUARDIAN_RELATION_ENTITY = "boxerGuardianRelation";

    /**
     * 健康状态代码 字段备注
     */
    public final static String BOXER_HEALTH_COMMENT = "健康状态代码";

    /**
     * 健康状态代码 数据库字段名
     */
    public final static String BOXER_HEALTH_COLUMN = "BOXER_HEALTH";

    /**
     * 健康状态代码 字段实体名
     */
    public final static String BOXER_HEALTH_ENTITY = "boxerHealth";

    /**
     * 健康状态内容 字段备注
     */
    public final static String BOXER_HEALTH_CONTENT_COMMENT = "健康状态内容";

    /**
     * 健康状态内容 数据库字段名
     */
    public final static String BOXER_HEALTH_CONTENT_COLUMN = "BOXER_HEALTH_CONTENT";

    /**
     * 健康状态内容 字段实体名
     */
    public final static String BOXER_HEALTH_CONTENT_ENTITY = "boxerHealthContent";

    /**
     * 健康状态 字段备注
     */
    public final static String BOXER_HEALTH_NAME_COMMENT = "健康状态";

    /**
     * 健康状态 数据库字段名
     */
    public final static String BOXER_HEALTH_NAME_COLUMN = "BOXER_HEALTH_NAME";

    /**
     * 健康状态 字段实体名
     */
    public final static String BOXER_HEALTH_NAME_ENTITY = "boxerHealthName";

    /**
     * 运动员状态编码 字段备注
     */
    public final static String BOXER_STATUS_COMMENT = "运动员状态编码";

    /**
     * 运动员状态编码 数据库字段名
     */
    public final static String BOXER_STATUS_COLUMN = "BOXER_STATUS";

    /**
     * 运动员状态编码 字段实体名
     */
    public final static String BOXER_STATUS_ENTITY = "boxerStatus";

    /**
     * 运动员状态 字段备注
     */
    public final static String BOXER_STATUS_NAME_COMMENT = "运动员状态";

    /**
     * 运动员状态 数据库字段名
     */
    public final static String BOXER_STATUS_NAME_COLUMN = "BOXER_STATUS_NAME";

    /**
     * 运动员状态 字段实体名
     */
    public final static String BOXER_STATUS_NAME_ENTITY = "boxerStatusName";

    /**
     * 运动员级别编码 字段备注
     */
    public final static String BOXER_GRADE_COMMENT = "运动员级别编码";

    /**
     * 运动员级别编码 数据库字段名
     */
    public final static String BOXER_GRADE_COLUMN = "BOXER_GRADE";

    /**
     * 运动员级别编码 字段实体名
     */
    public final static String BOXER_GRADE_ENTITY = "boxerGrade";

    /**
     * 运动员级别 字段备注
     */
    public final static String BOXER_GRADE_NAME_COMMENT = "运动员级别";

    /**
     * 运动员级别 数据库字段名
     */
    public final static String BOXER_GRADE_NAME_COLUMN = "BOXER_GRADE_NAME";

    /**
     * 运动员级别 字段实体名
     */
    public final static String BOXER_GRADE_NAME_ENTITY = "boxerGradeName";

    /**
     * 字段备注
     */
    public final static String BOXER_TYPE_COMMENT = "";

    /**
     * 数据库字段名
     */
    public final static String BOXER_TYPE_COLUMN = "BOXER_TYPE";

    /**
     * 字段实体名
     */
    public final static String BOXER_TYPE_ENTITY = "boxerType";

    /**
     * 电子邮箱 字段备注
     */
    public final static String BOXER_EMAIL_COMMENT = "电子邮箱";

    /**
     * 电子邮箱 数据库字段名
     */
    public final static String BOXER_EMAIL_COLUMN = "BOXER_EMAIL";

    /**
     * 电子邮箱 字段实体名
     */
    public final static String BOXER_EMAIL_ENTITY = "boxerEmail";

    /**
     * 运动员手机号码 字段备注
     */
    public final static String BOXER_MOBILE_COMMENT = "运动员手机号码";

    /**
     * 运动员手机号码 数据库字段名
     */
    public final static String BOXER_MOBILE_COLUMN = "BOXER_MOBILE";

    /**
     * 运动员手机号码 字段实体名
     */
    public final static String BOXER_MOBILE_ENTITY = "boxerMobile";

    /**
     * 是否有照片 字段备注
     */
    public final static String BOXER_HAS_PHOTO_COMMENT = "是否有照片";

    /**
     * 是否有照片 数据库字段名
     */
    public final static String BOXER_HAS_PHOTO_COLUMN = "BOXER_HAS_PHOTO";

    /**
     * 是否有照片 字段实体名
     */
    public final static String BOXER_HAS_PHOTO_ENTITY = "boxerHasPhoto";

    // ===================扩展=======================

}
