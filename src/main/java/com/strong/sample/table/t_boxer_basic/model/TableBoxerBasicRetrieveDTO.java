package com.strong.sample.table.t_boxer_basic.model;

import com.strong.utils.mvc.pojo.retrieve.VabDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 运动员基本信息表 查询模型类
 *
 * @author Simen
 * @date 2024-11-20 11:05:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TableBoxerBasicRetrieveDTO extends VabDTO<TableBoxerBasicVO> {

}