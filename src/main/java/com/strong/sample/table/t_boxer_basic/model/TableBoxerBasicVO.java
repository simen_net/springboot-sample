package com.strong.sample.table.t_boxer_basic.model;

import com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants;
import com.strong.sample.table.t_boxer_basic.jpa.TableBoxerBasicDO;
import com.strong.utils.CodeUtils;
import com.strong.utils.StrongUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 运动员基本信息表 显示模型类
 *
 * @author Simen
 * @date 2024-07-10 21:12:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TableBoxerBasicVO extends TableBoxerBasicAO {

    /**
     * 序列化包含的字段数组
     */
    public final static String[] STRS_INCLUDE_PROPERTIES = {
            // 系统随机生成固定编号 字段实体名
            TableBoxerBasicConstants.BOXER_ID_ENTITY,
            // 运动员编号（不重复） 字段实体名
            TableBoxerBasicConstants.BOXER_ATHLETE_ID_ENTITY,
            // 姓名 字段实体名
            TableBoxerBasicConstants.BOXER_NAME_ENTITY,
//            // 拼音首字母 字段实体名
//            TableBoxerBasicConstants.BOXER_PHONETIC_INITIALS_ENTITY,
//            // 姓的拼音 字段实体名
//            TableBoxerBasicConstants.BOXER_PHONETIC_SURNAME_ENTITY,
            // 英文名 字段实体名
            TableBoxerBasicConstants.BOXER_ENGLISH_NAME_ENTITY,
//            // [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键 字段实体名
//            TableBoxerBasicConstants.BOXER_LOCAL_REGISTER_INFO_ID_ENTITY,
//            // [外键]运动员所在单位ID - 关联T_UNIT主键 字段实体名
//            TableBoxerBasicConstants.BOXER_UNIT_ID_ENTITY,
//            // 身份证号码 字段实体名
//            TableBoxerBasicConstants.BOXER_ID_NUMBER_ENTITY,
//            // 性别编码 字段实体名
//            TableBoxerBasicConstants.BOXER_GENDER_ENTITY,
//            // 性别 字段实体名
//            TableBoxerBasicConstants.BOXER_GENDER_NAME_ENTITY,
//            // 民族编码 字段实体名
//            TableBoxerBasicConstants.BOXER_NATIONALITY_ENTITY,
//            // 民族 字段实体名
//            TableBoxerBasicConstants.BOXER_NATIONALITY_NAME_ENTITY,
            // 户籍所在地 字段实体名
            TableBoxerBasicConstants.BOXER_NATIVE_PLACE_ENTITY,
//            // 出生日期 字段实体名
//            TableBoxerBasicConstants.BOXER_BIRTHDAY_ENTITY,
//            // 文化程度编码 字段实体名
//            TableBoxerBasicConstants.BOXER_EDUCATION_ENTITY,
//            // 文化程度 字段实体名
//            TableBoxerBasicConstants.BOXER_EDUCATION_NAME_ENTITY,
//            // 监护人身份证号码 字段实体名
//            TableBoxerBasicConstants.BOXER_GUARDIAN_ID_NUMBER_ENTITY,
//            // 监护人手机号码 字段实体名
//            TableBoxerBasicConstants.BOXER_GUARDIAN_MOBILE_ENTITY,
            // 监护人姓名 字段实体名
            TableBoxerBasicConstants.BOXER_GUARDIAN_NAME_ENTITY,
            // 与被监护人关系 字段实体名
            TableBoxerBasicConstants.BOXER_GUARDIAN_RELATION_ENTITY,
//            // 健康状态代码 字段实体名
//            TableBoxerBasicConstants.BOXER_HEALTH_ENTITY,
//            // 健康状态内容 字段实体名
//            TableBoxerBasicConstants.BOXER_HEALTH_CONTENT_ENTITY,
//            // 健康状态 字段实体名
//            TableBoxerBasicConstants.BOXER_HEALTH_NAME_ENTITY,
//            // 运动员状态编码 字段实体名
//            TableBoxerBasicConstants.BOXER_STATUS_ENTITY,
//            // 运动员状态 字段实体名
//            TableBoxerBasicConstants.BOXER_STATUS_NAME_ENTITY,
//            // 运动员级别编码 字段实体名
//            TableBoxerBasicConstants.BOXER_GRADE_ENTITY,
            // 运动员级别 字段实体名
            TableBoxerBasicConstants.BOXER_GRADE_NAME_ENTITY,
            // 运动员类型 字段实体名
            TableBoxerBasicConstants.BOXER_TYPE_ENTITY,
//            // 电子邮箱 字段实体名
//            TableBoxerBasicConstants.BOXER_EMAIL_ENTITY,
//            // 运动员手机号码 字段实体名
//            TableBoxerBasicConstants.BOXER_MOBILE_ENTITY,
//            // 是否有照片 字段实体名
            TableBoxerBasicConstants.BOXER_HAS_PHOTO_ENTITY,
    };

    /**
     * 自动生成序列化不包含的字段数组
     */
    public final static String[] STRS_EXCLUSION_PROPERTIES = StrongUtils.getExclusionFieldNames(TableBoxerBasicVO.class);

    /**
     * 以实体类实例化
     *
     * @param tableBoxerBasicDO 实体类
     */
    public TableBoxerBasicVO(final TableBoxerBasicDO tableBoxerBasicDO) {
        this(tableBoxerBasicDO, STRS_INCLUDE_PROPERTIES);
    }

    /**
     * 以实体类实例化 仅包含strsIncludeProperties的属性
     *
     * @param tableBoxerBasicDO     实体类
     * @param strsIncludeProperties 包含的字段数组
     */
    public TableBoxerBasicVO(final TableBoxerBasicDO tableBoxerBasicDO, final String[] strsIncludeProperties) {
        StrongUtils.copyProperties(tableBoxerBasicDO, this, strsIncludeProperties);
    }

    /**
     * 主入口
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        String strClassName = Thread.currentThread().getStackTrace()[1].getClassName();
        CodeUtils.printTsModelCode(strClassName, STRS_INCLUDE_PROPERTIES);
    }

    // ===================扩展=======================

}
