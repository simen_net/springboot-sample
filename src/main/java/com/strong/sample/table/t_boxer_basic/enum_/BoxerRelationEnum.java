package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 关系枚举
 *
 * @author simen
 * @date 2024/11/20
 */
@Getter
public enum BoxerRelationEnum {

    MOTHER_AND_DAUGHTER("MOTHER_AND_DAUGHTER", "母女"),
    MOTHER_AND_SON("MOTHER_AND_SON", "母子"),
    FATHER_AND_DAUGHTER("FATHER_AND_DAUGHTER", "父女"),
    FATHER_AND_SON("FATHER_AND_SON", "父子"),
    GRANDPARENT_AND_GRANDCHILD("GRANDPARENT_AND_GRANDCHILD", "祖孙"),
    MOTHER_GRANDPARENT_AND_GRANDCHILD("MOTHER_GRANDPARENT_AND_GRANDCHILD", "母祖孙"),
    MOTHER_UNCLE_AND_NEPHEW("MOTHER_UNCLE_AND_NEPHEW", "舅甥"),
    UNCLE_AND_NEPHEW("UNCLE_AND_NEPHEW", "叔侄"),
    AUNT_AND_NEPHEW("AUNT_AND_NEPHEW", "姑侄"),
    MOTHER_AUNT_AND_NEPHEW("MOTHER_AUNT_AND_NEPHEW", "姨甥");

    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回消息
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回值
     */
    BoxerRelationEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
