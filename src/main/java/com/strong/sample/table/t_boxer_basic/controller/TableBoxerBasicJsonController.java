package com.strong.sample.table.t_boxer_basic.controller;

import com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants;
import com.strong.sample.table.t_boxer_basic.jpa.TableBoxerBasicDO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicCreateDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicRetrieveDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicUpdateDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicVO;
import com.strong.sample.table.t_boxer_basic.service.TableBoxerBasicService;
import com.strong.utils.JSON;
import com.strong.utils.mvc.ModuleJsonController;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.strong.utils.mvc.MvcConstants.PARAMETER_INTS_ID;
import static com.strong.utils.mvc.MvcConstants.PARAMETER_INT_ID;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_GENERAL;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_SUPER;

/**
 * 运动员基本信息表 JSON控制器
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Slf4j
@RestController
@RequestMapping(value = TableBoxerBasicConstants.TABLE_ENTITY)
public class TableBoxerBasicJsonController implements ModuleJsonController<TableBoxerBasicVO, TableBoxerBasicCreateDTO, TableBoxerBasicUpdateDTO, TableBoxerBasicRetrieveDTO> {

    /**
     * 过滤排序条件
     */
    private final static String[] STRS_FILTER_SORT = {};

    /**
     * 过滤查询条件
     */
    private final static String[] STRS_FILTER_SEARCH = {};

    /**
     * 注入Service
     */
    private final TableBoxerBasicService tableBoxerBasicService;

    /**
     * 实例化
     *
     * @param tableBoxerBasicService 注入的Service
     */
    @Autowired
    public TableBoxerBasicJsonController(TableBoxerBasicService tableBoxerBasicService) {
        this.tableBoxerBasicService = tableBoxerBasicService;
    }

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVO<TableBoxerBasicVO> getRecordView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableBoxerBasicDO tableBoxerBasicDO = tableBoxerBasicService.getRecord(intId);
        TableBoxerBasicVO tableBoxerBasicVO = tableBoxerBasicService.getModel(tableBoxerBasicDO);
        return new ReplyVO<>(tableBoxerBasicVO);
    }

    /**
     * 添加记录操作 控制器
     *
     * @param modeldCreate 待添加的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableBoxerBasicVO> getCreateAction(@Valid @RequestBody final TableBoxerBasicCreateDTO modeldCreate) {
        TableBoxerBasicDO tableBoxerBasicDO = tableBoxerBasicService.getCreateAction(modeldCreate);
        TableBoxerBasicVO tableBoxerBasicVO = tableBoxerBasicService.getModel(tableBoxerBasicDO, TableBoxerBasicVO.STRS_INCLUDE_PROPERTIES);
        System.out.println(JSON.toJSONString(tableBoxerBasicVO));
        return new ReplyVO<>(tableBoxerBasicVO, ReplyEnum.SUCCESS_CREATE_DATA);
    }

    /**
     * 执行修改的 控制器.
     *
     * @param modelUpdate 待修改的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableBoxerBasicVO> getUpdateAction(@Valid @RequestBody final TableBoxerBasicUpdateDTO modelUpdate) {
        TableBoxerBasicDO tableBoxerBasicDO = tableBoxerBasicService.getUpdateAction(modelUpdate);
        TableBoxerBasicVO tableBoxerBasicVO = tableBoxerBasicService.getModel(tableBoxerBasicDO, TableBoxerBasicVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(tableBoxerBasicVO, ReplyEnum.SUCCESS_UPDATE_DATA);
    }

    /**
     * 显示待修改内容的 控制器.
     *
     * @param intId 显示对象的ID
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableBoxerBasicVO> getUpdateView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableBoxerBasicDO tableBoxerBasicDO = tableBoxerBasicService.getUpdateRecord(intId);
        TableBoxerBasicVO tableBoxerBasicVO = tableBoxerBasicService.getModel(tableBoxerBasicDO);
        return new ReplyVO<>(tableBoxerBasicVO);
    }

    /**
     * 执行删除的 控制器.
     *
     * @param intsId 待删除id数组
     * @return 基于无数据的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId) {
        tableBoxerBasicService.getDeleteAction(intsId);
        return new ReplyVO<>(ReplyEnum.SUCCESS_DELETE_DATA);
    }

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型队列的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableBoxerBasicVO> getListView(@RequestBody final TableBoxerBasicRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        List<TableBoxerBasicDO> listTableBoxerBasicDO = tableBoxerBasicService.getAllList(modelRetrieve);
        List<TableBoxerBasicVO> listTableBoxerBasicVO = tableBoxerBasicService.getModelList(listTableBoxerBasicDO, TableBoxerBasicVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableBoxerBasicVO);
    }

    /**
     * 显示分页列表的 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型分页对象的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableBoxerBasicVO> getPageView(@RequestBody final TableBoxerBasicRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        Page<TableBoxerBasicDO> pageTableBoxerBasicDO = tableBoxerBasicService.getPageList(modelRetrieve);
        List<TableBoxerBasicVO> listTableBoxerBasicVO = tableBoxerBasicService.getModelList(pageTableBoxerBasicDO.getContent(), TableBoxerBasicVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableBoxerBasicVO, pageTableBoxerBasicDO.getTotalElements());
    }


    // ===================扩展=======================
}
