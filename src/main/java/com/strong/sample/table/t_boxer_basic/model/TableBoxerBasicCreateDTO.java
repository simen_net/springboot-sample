package com.strong.sample.table.t_boxer_basic.model;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.strong.utils.annotation.StrongRemark;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Date;

import static com.strong.sample.table.t_boxer_basic.TableBoxerBasicConstants.*;
import static com.strong.utils.constants.MessageConstants.ERROR_NOT_NULL;
import static com.strong.utils.constants.MessageConstants.ERROR_SIZE;

/**
 * 运动员基本信息表 添加模型类
 *
 * @author Simen
 * @date 2024-11-20 11:05:53
 */
@Data
public class TableBoxerBasicCreateDTO {

    /**
     *
     <pre>
     *     @Null 被注释的元素必须为null
     *     @NotNull 被注释的元素不能为null
     *     @AssertTrue 被注释的元素必须为true
     *     @AssertFalse 被注释的元素必须为false
     *     @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
     *     @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
     *     @Size(max,min) 被注释的元素的大小必须在指定的范围内。
     *     @Digits(integer,fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
     *     @Past 被注释的元素必须是一个过去的日期
     *     @Future 被注释的元素必须是一个将来的日期
     *     @Pattern(value) 被注释的元素必须符合指定的正则表达式。
     *     @Email 被注释的元素必须是电子邮件地址
     *     @Length 被注释的字符串的大小必须在指定的范围内
     *     @NotEmpty 被注释的字符串必须非空
     *     @Range 被注释的元素必须在合适的范围内
     * </pre>
     */


    /**
     * 运动员编号（不重复） 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 64)
    @StrongRemark(BOXER_ATHLETE_ID_COMMENT)
    private String boxerAthleteId;

    /**
     * 姓名 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 32)
    @StrongRemark(BOXER_NAME_COMMENT)
    private String boxerName;

    /**
     * 拼音首字母 字段
     */
    @Size(message = ERROR_SIZE, max = 32)
    @StrongRemark(BOXER_PHONETIC_INITIALS_COMMENT)
    private String boxerPhoneticInitials;

    /**
     * 姓的拼音 字段
     */
    @Size(message = ERROR_SIZE, max = 32)
    @StrongRemark(BOXER_PHONETIC_SURNAME_COMMENT)
    private String boxerPhoneticSurname;

    /**
     * 英文名 字段
     */
    @Size(message = ERROR_SIZE, max = 64)
    @StrongRemark(BOXER_ENGLISH_NAME_COMMENT)
    private String boxerEnglishName;

    /**
     * [外键]运动员当前注册信息INFO_ID（不重复） - 关联T_BOXER_REGISTER内键 字段
     */
    @Size(message = ERROR_SIZE, max = 64)
    @StrongRemark(BOXER_LOCAL_REGISTER_INFO_ID_COMMENT)
    private String boxerLocalRegisterInfoId;

    /**
     * [外键]运动员所在单位ID - 关联T_UNIT主键 字段
     */
    @StrongRemark(BOXER_UNIT_ID_COMMENT)
    private Integer boxerUnitId;

    /**
     * 身份证号码 字段
     */
    @NotNull(message = ERROR_NOT_NULL)
    @Size(message = ERROR_SIZE, min = 1, max = 30)
    @StrongRemark(BOXER_ID_NUMBER_COMMENT)
    private String boxerIdNumber;

    /**
     * 性别编码 字段
     */
    @Size(message = ERROR_SIZE, min = 1, max = 8)
    @StrongRemark(BOXER_GENDER_COMMENT)
    private String boxerGender;

    /**
     * 性别 字段
     */
    @Size(message = ERROR_SIZE, max = 8)
    @StrongRemark(BOXER_GENDER_NAME_COMMENT)
    private String boxerGenderName;

    /**
     * 民族编码 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_NATIONALITY_COMMENT)
    private String boxerNationality;

    /**
     * 民族 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_NATIONALITY_NAME_COMMENT)
    private String boxerNationalityName;

    /**
     * 户籍所在地 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_NATIVE_PLACE_COMMENT)
    private String boxerNativePlace;

    /**
     * 出生日期 字段
     */
    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @StrongRemark(BOXER_BIRTHDAY_COMMENT)
    private Date boxerBirthday;

    /**
     * 文化程度编码 字段
     */
    @Size(message = ERROR_SIZE, max = 16)
    @StrongRemark(BOXER_EDUCATION_COMMENT)
    private String boxerEducation;

    /**
     * 文化程度 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_EDUCATION_NAME_COMMENT)
    private String boxerEducationName;

    /**
     * 监护人身份证号码 字段
     */
    @Size(message = ERROR_SIZE, max = 30)
    @StrongRemark(BOXER_GUARDIAN_ID_NUMBER_COMMENT)
    private String boxerGuardianIdNumber;

    /**
     * 监护人手机号码 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @StrongRemark(BOXER_GUARDIAN_MOBILE_COMMENT)
    private String boxerGuardianMobile;

    /**
     * 监护人姓名 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_GUARDIAN_NAME_COMMENT)
    private String boxerGuardianName;

    /**
     * 与被监护人关系 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_GUARDIAN_RELATION_COMMENT)
    private String boxerGuardianRelation;

    /**
     * 健康状态代码 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_HEALTH_COMMENT)
    private String boxerHealth;

    /**
     * 健康状态内容 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_HEALTH_CONTENT_COMMENT)
    private String boxerHealthContent;

    /**
     * 健康状态 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_HEALTH_NAME_COMMENT)
    private String boxerHealthName;

    /**
     * 运动员状态编码 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_STATUS_COMMENT)
    private String boxerStatus;

    /**
     * 运动员状态 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_STATUS_NAME_COMMENT)
    private String boxerStatusName;

    /**
     * 运动员级别编码 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_GRADE_COMMENT)
    private String boxerGrade;

    /**
     * 运动员级别 字段
     */
    @Size(message = ERROR_SIZE, max = 100)
    @StrongRemark(BOXER_GRADE_NAME_COMMENT)
    private String boxerGradeName;

    /**
     * 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @StrongRemark(BOXER_TYPE_COMMENT)
    private String boxerType;

    /**
     * 电子邮箱 字段
     */
    @Size(message = ERROR_SIZE, max = 200)
    @StrongRemark(BOXER_EMAIL_COMMENT)
    private String boxerEmail;

    /**
     * 运动员手机号码 字段
     */
    @Size(message = ERROR_SIZE, max = 20)
    @StrongRemark(BOXER_MOBILE_COMMENT)
    private String boxerMobile;

    /**
     * 是否有照片 字段
     */
    @StrongRemark(BOXER_HAS_PHOTO_COMMENT)
    private Boolean boxerHasPhoto;

    // ===================扩展=======================

}