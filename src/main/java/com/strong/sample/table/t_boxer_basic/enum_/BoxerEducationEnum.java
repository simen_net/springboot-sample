package com.strong.sample.table.t_boxer_basic.enum_;

import lombok.Getter;

/**
 * 学历 枚举
 *
 * @author simen
 * @date 2023/02/16
 */
@Getter
public enum BoxerEducationEnum {
    E00("E00", "未知"),
    E01("E01", "小学"),
    E02("E02", "初中"),
    E03("E03", "高中"),
    E04("E04", "中专"),
    E05("E05", "大专"),
    E06("E06", "大学本科"),
    E07("E07", "硕士研究生");

    /**
     * 返回代码
     */
    private final String code;

    /**
     * 返回消息
     */
    private final String value;

    /**
     * 实例化方法
     *
     * @param code  返回代码
     * @param value 返回值
     */
    BoxerEducationEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
