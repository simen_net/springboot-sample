package com.strong.sample.table.t_unit.controller;

import com.strong.sample.table.t_unit.TableUnitConstants;
import com.strong.sample.table.t_unit.service.TableUnitService;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;
import com.strong.sample.table.t_unit.model.TableUnitCreateDTO;
import com.strong.sample.table.t_unit.model.TableUnitRetrieveDTO;
import com.strong.sample.table.t_unit.model.TableUnitUpdateDTO;
import com.strong.sample.table.t_unit.model.TableUnitVO;
import com.strong.utils.JSON;
import com.strong.utils.mvc.ModuleJsonController;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.strong.utils.mvc.MvcConstants.PARAMETER_INTS_ID;
import static com.strong.utils.mvc.MvcConstants.PARAMETER_INT_ID;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_GENERAL;
import static com.strong.utils.security.AuthorityConstants.STR_HAS_AUTHORITY_SUPER;

/**
 * 单位表 JSON控制器
 *
 * @author Simen
 * @date 2022-02-07 22:17:26
 */
@Slf4j
@RestController
@RequestMapping(value = TableUnitConstants.TABLE_ENTITY)
public class TableUnitJsonController implements ModuleJsonController<TableUnitVO, TableUnitCreateDTO, TableUnitUpdateDTO, TableUnitRetrieveDTO> {

    /**
     * 过滤排序条件
     */
    private final static String[] STRS_FILTER_SORT = {};

    /**
     * 过滤查询条件
     */
    private final static String[] STRS_FILTER_SEARCH = {};

    /**
     * 注入Service
     */
    private final TableUnitService tableUnitService;

    /**
     * 实例化
     *
     * @param tableUnitService 注入的Service
     */
    @Autowired
    public TableUnitJsonController(TableUnitService tableUnitService) {
        this.tableUnitService = tableUnitService;
    }

    /**
     * 返回记录信息 控制器
     *
     * @param intId 查询用主键
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVO<TableUnitVO> getRecordView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableUnitDO tableUnitDO = tableUnitService.getRecord(intId);
        TableUnitVO tableUnitVO = tableUnitService.getModel(tableUnitDO);
        return new ReplyVO<>(tableUnitVO);
    }

    /**
     * 添加记录操作 控制器
     *
     * @param modeldCreate 待添加的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUnitVO> getCreateAction(@Valid @RequestBody final TableUnitCreateDTO modeldCreate) {
        TableUnitDO tableUnitDO = tableUnitService.getCreateAction(modeldCreate);
        TableUnitVO tableUnitVO = tableUnitService.getModel(tableUnitDO, TableUnitVO.STRS_INCLUDE_PROPERTIES);
        System.out.println(JSON.toJSONString(tableUnitVO));
        return new ReplyVO<>(tableUnitVO, ReplyEnum.SUCCESS_CREATE_DATA);
    }

    /**
     * 执行修改的 控制器.
     *
     * @param modelUpdate 待修改的模型数据
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUnitVO> getUpdateAction(@Valid @RequestBody final TableUnitUpdateDTO modelUpdate) {
        TableUnitDO tableUnitDO = tableUnitService.getUpdateAction(modelUpdate);
        TableUnitVO tableUnitVO = tableUnitService.getModel(tableUnitDO, TableUnitVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVO<>(tableUnitVO, ReplyEnum.SUCCESS_UPDATE_DATA);
    }

    /**
     * 显示待修改内容的 控制器.
     *
     * @param intId 显示对象的ID
     * @return 用于与前端交互的统一泛型对象
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<TableUnitVO> getUpdateView(@PathVariable(PARAMETER_INT_ID) Integer intId) {
        TableUnitDO tableUnitDO = tableUnitService.getUpdateRecord(intId);
        TableUnitVO tableUnitVO = tableUnitService.getModel(tableUnitDO);
        return new ReplyVO<>(tableUnitVO);
    }

    /**
     * 执行删除的 控制器.
     *
     * @param intsId 待删除id数组
     * @return 基于无数据的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_SUPER)
    public ReplyVO<String> getDeleteAction(@PathVariable(PARAMETER_INTS_ID) final Integer[] intsId) {
        tableUnitService.getDeleteAction(intsId);
        return new ReplyVO<>(ReplyEnum.SUCCESS_DELETE_DATA);
    }

    /**
     * 显示无分页列表信息 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型队列的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableUnitVO> getListView(@RequestBody final TableUnitRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        List<TableUnitDO> listTableUnitDO = tableUnitService.getAllList(modelRetrieve);
        List<TableUnitVO> listTableUnitVO = tableUnitService.getModelList(listTableUnitDO, TableUnitVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableUnitVO);
    }

    /**
     * 显示分页列表的 控制器
     *
     * @param modelRetrieve 用于查询的模型类
     * @return 基于显示模型分页对象的泛型模型
     */
    @Override
    @PreAuthorize(STR_HAS_AUTHORITY_GENERAL)
    public ReplyVabListVO<TableUnitVO> getPageView(@RequestBody final TableUnitRetrieveDTO modelRetrieve) {
        // modelRetrieve.filterMapSort(STRS_FILTER_SORT);
        // modelRetrieve.filterMapSearch(STRS_FILTER_SEARCH);
        Page<TableUnitDO> pageTableUnitDO = tableUnitService.getPageList(modelRetrieve);
        List<TableUnitVO> listTableUnitVO = tableUnitService.getModelList(pageTableUnitDO.getContent(), TableUnitVO.STRS_INCLUDE_PROPERTIES);
        return new ReplyVabListVO<>(listTableUnitVO, pageTableUnitDO.getTotalElements());
    }


    // ===================扩展=======================
}
