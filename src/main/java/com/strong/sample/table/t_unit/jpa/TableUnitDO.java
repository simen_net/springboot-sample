package com.strong.sample.table.t_unit.jpa;

import com.strong.sample.table.t_unit.TableUnitConstants;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.strong.utils.annotation.StrongRemark;

import java.util.Date;
import java.math.BigDecimal;

import jakarta.persistence.*;
import java.util.Objects;

/**
* 单位表 DO实体类
*
* @author Simen
* @date 2024-11-19 17:20:57
*/
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = TableUnitConstants.TABLE_NAME)
public class TableUnitDO implements java.io.Serializable {

        @Id
    @Column(name = TableUnitConstants.UNIT_ID_COLUMN  , unique = true , nullable = false  )
    @StrongRemark("系统随机生成固定编号")
private Integer unitId;

    @Column(name = TableUnitConstants.UNIT_NAME_COLUMN , length = 200  , nullable = false  )
    @StrongRemark("单位名称")
private String unitName;

    @Column(name = TableUnitConstants.UNIT_CODE_COLUMN , length = 32  , nullable = false  )
    @StrongRemark("单位显示编码")
private String unitCode;

    @Column(name = TableUnitConstants.UNIT_TYPE_COLUMN , length = 100  , nullable = false  )
    @StrongRemark("单位类型编码")
private String unitType;

    @Column(name = TableUnitConstants.UNIT_NAME_KEYWORD_COLUMN , length = 50    )
    @StrongRemark("单位名称关键字")
private String unitNameKeyword;

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Hibernate.getClass(this) != Hibernate.getClass(obj)) {
                return false;
            }
            TableUnitDO that = (TableUnitDO) obj;
            return unitId != null && Objects.equals(unitId, that.unitId);
        }

    // ===================扩展=======================
}

