package com.strong.sample.table.t_unit.model;

import lombok.*;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.strong.utils.annotation.StrongRemark;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

import static com.strong.sample.table.t_unit.TableUnitConstants.*;
import static com.strong.utils.constants.MessageConstants.*;

/**
 * 单位表 修改模型类
 *
 * @author Simen
 * @date 2024-11-19 17:20:57
 */
@Data
public class TableUnitUpdateDTO{

    /**
    *
    <pre>
        *     @Null 被注释的元素必须为null
        *     @NotNull 被注释的元素不能为null
        *     @AssertTrue 被注释的元素必须为true
        *     @AssertFalse 被注释的元素必须为false
        *     @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
        *     @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
        *     @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
        *     @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
        *     @Size(max,min) 被注释的元素的大小必须在指定的范围内。
        *     @Digits(integer,fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
        *     @Past 被注释的元素必须是一个过去的日期
        *     @Future 被注释的元素必须是一个将来的日期
        *     @Pattern(value) 被注释的元素必须符合指定的正则表达式。
        *     @Email 被注释的元素必须是电子邮件地址
        *     @Length 被注释的字符串的大小必须在指定的范围内
        *     @NotEmpty 被注释的字符串必须非空
        *     @Range 被注释的元素必须在合适的范围内
        * </pre>
    */


        /**
        * 系统随机生成固定编号 字段
        */
            @NotNull(message = ERROR_NOT_NULL)
        @StrongRemark(UNIT_ID_COMMENT)
        private Integer unitId;

        /**
        * 单位名称 字段
        */
            @NotNull(message = ERROR_NOT_NULL)
            @Size(message = ERROR_SIZE ,min = 1, max = 200)
        @StrongRemark(UNIT_NAME_COMMENT)
        private String unitName;

        /**
        * 单位显示编码 字段
        */
            @NotNull(message = ERROR_NOT_NULL)
            @Size(message = ERROR_SIZE ,min = 1, max = 32)
        @StrongRemark(UNIT_CODE_COMMENT)
        private String unitCode;

        /**
        * 单位类型编码 字段
        */
            @NotNull(message = ERROR_NOT_NULL)
            @Size(message = ERROR_SIZE ,min = 1, max = 100)
        @StrongRemark(UNIT_TYPE_COMMENT)
        private String unitType;

        /**
        * 单位名称关键字 字段
        */
            @Size(message = ERROR_SIZE , max = 50)
        @StrongRemark(UNIT_NAME_KEYWORD_COMMENT)
        private String unitNameKeyword;

    // ===================扩展=======================

}