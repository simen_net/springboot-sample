package com.strong.sample.table.t_user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.IterUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_user.jpa.TableUserDAO;
import com.strong.sample.table.t_user.jpa.TableUserDO;
import com.strong.sample.table.t_user.model.TableUserCreateDTO;
import com.strong.sample.table.t_user.model.TableUserRetrieveDTO;
import com.strong.sample.table.t_user.model.TableUserUpdateDTO;
import com.strong.sample.table.t_user.model.TableUserVO;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.pojo.view.ReplyVO;
import com.strong.utils.mvc.pojo.view.vab.ReplyVabListVO;
import com.strong.utils.mvc.pojo.view.vab.VabList;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.strong.sample.table.t_user.TableUserServiceTest.getRandomCreateDTO;
import static com.strong.sample.table.t_user.TableUserServiceTest.getRandomUpdateDTO;
import static com.strong.sample.utils.MockMvcUtils.*;
import static com.strong.utils.mvc.MvcConstants.*;

/**
 * 表测试json控制器测试
 *
 * @author simen
 * @date 2024/08/20
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableUserJsonControllerTest extends SpringbootTests {

    /**
     * 测试用URL
     */
    public final static String STR_URL_RECORD_VIEW = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_RECORD_VIEW, "%s");
    public final static String STR_URL_CREATE_ACTION = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_CREATE_ACTION);
    public final static String STR_URL_UPDATE_ACTION = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_UPDATE_ACTION);
    public final static String STR_URL_UPDATE_VIEW = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_UPDATE_VIEW, "%s");
    public final static String STR_URL_DELETE_ACTION = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_DELETE_ACTION, "%s");
    public final static String STR_URL_LIST_VIEW = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_LIST_VIEW);
    public final static String STR_URL_PAGE_VIEW = StrUtil.join("/", TableUserConstants.TABLE_ENTITY, URL_PAGE_VIEW);
//
//    /**
//     * 拟测试记录数量
//     */
//    private static final Integer INT_RECORD_COUNT = RandomUtil.randomInt(100, 999);
//
//    /**
//     * 主键ID字段名
//     */
//    private static final String STR_ID_FILED_NAME = TableUserConstants.USER_ID_ENTITY;
//
//    /**
//     * 测试用文本字段名
//     */
//    private static final String STR_TEXT_FILED_NAME = TableUserConstants.USER_NAME_ENTITY;
//
//    /**
//     * 模拟mvc对象
//     */
//    @Autowired
//    public MockMvc mockMvc;
//
//    /**
//     * 数据库操作类
//     */
//    @Autowired
//    TableUserDAO tableUserDAO;
//
//    @Test
//    @Order(1)
//    @DisplayName("测试删除数据")
//    @Tag("BASIC_TEST")
//    public void testDeleteData() throws Exception {
//        postLogin();
//
//        // 通过DAO获取实际记录数
//        Integer intActualRecordCount = tableUserDAO.getCount();
//        // 如果初始记录为空，则先执行添加操作
//        if (intActualRecordCount == 0) {
//            testAddData();
//            intActualRecordCount = tableUserDAO.getCount();
//        }
//        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");
//
//        // 删除所有记录
//        deleteAllRecord(STR_TOKEN_ADMIN);
//    }
//
//    @Test
//    @Order(2)
//    @DisplayName("测试添加数据")
//    @Tag("BASIC_TEST")
//    public void testAddData() throws Exception {
//        postLogin();
//
//        // 通过DAO获取实际记录数
//        Integer intOriginalRecordCount = tableUserDAO.getCount();
//
//        // 循环添加记录
//        for (int i = 0; i < INT_RECORD_COUNT; i++) {
//            // 随机获取DTO用于生成创建请求的JSON
//            TableUserCreateDTO tableUserCreateDTO = getRandomCreateDTO();
//
//            {
//                // STR_URL_CREATE_ACTION 权限测试
//                // 测试匿名用户无权限
//                testResultByReplyVO_402(mockMvc.perform(
//                        postJsonRequestBuilder(STR_URL_CREATE_ACTION, null, tableUserCreateDTO)));
//                // 测试普通用户无权限
//                testResultByReplyVO_403(mockMvc.perform(
//                        postJsonRequestBuilder(STR_URL_CREATE_ACTION, STR_TOKEN_USER, tableUserCreateDTO)));
//            }
//
//            // 发送添加请求
//            TableUserVO tableUserVO = getCreateAction(tableUserCreateDTO);
//            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
//            StrongUtils.compareBean(tableUserCreateDTO, tableUserVO,
//                    ArrayUtil.append(TableUserVO.STRS_EXCLUSION_PROPERTIES, STR_ID_FILED_NAME));
//            // 断言排除字段为空
//            assertField(tableUserVO);
//        }
//
//        // 通过DAO获取实际记录数
//        int intActualRecordCount = tableUserDAO.getCount();
//        // 预期记录数量
//        int intPredictRecordCount = intOriginalRecordCount + INT_RECORD_COUNT;
//        Assert.isTrue(intPredictRecordCount == intActualRecordCount,
//                "添加记录数量【{}】与实际数量【{}】不一致", intPredictRecordCount, intActualRecordCount);
//    }
//
//    @Test
//    @Order(3)
//    @DisplayName("测试条件查询所有记录")
//    @Tag("BASIC_TEST")
//    public void testQueryAll() throws Exception {
//        postLogin();
//
//        // 通过DAO获取实际记录数
//        Integer intActualRecordCount = tableUserDAO.getCount();
//        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");
//
//        String strLike = "a";
//
//        TableUserRetrieveDTO tableUserRetrieveDTO = getTableUserRetrieveDTO();
//
//        {
//            // STR_URL_LIST_VIEW 权限测试
//            // 测试匿名用户无权限
//            testResultByReplyVO_402(mockMvc.perform(
//                    postJsonRequestBuilder(STR_URL_LIST_VIEW, null, tableUserRetrieveDTO)));
//            // 测试普通用户有权限
//            testResultByReplyVO_200(mockMvc.perform(
//                    postJsonRequestBuilder(STR_URL_LIST_VIEW, STR_TOKEN_USER, tableUserRetrieveDTO)));
//        }
//
//        { // ============ 完整查询记录测试 ============
//            // 发送获取完整记录请求
//            ReplyVabListVO<TableUserVO> replyVabListVO = getAllList(STR_TOKEN_ADMIN, tableUserRetrieveDTO);
//
//            // 获取所有记录的队列，断言记录数量
//            List<TableUserVO> listVO = replyVabListVO.getData().getList();
//            Assert.notEmpty(listVO, "返回记录空");
//            Assert.equals(listVO.size(), intActualRecordCount,
//                    "返回的记录数[{}]与实际记录数[{}]不一致", listVO.size(), intActualRecordCount);
//            // 断言对象字段符合要求
//            assertField(listVO);
//        }
//
//        { // ============ 条件查询记录测试 ============
//            tableUserRetrieveDTO.addSearch(STR_TEXT_FILED_NAME, String.format("%%%s%%", strLike));
//            // 发送获取完整记录请求
//            ReplyVabListVO<TableUserVO> replyVabListVO = getAllList(STR_TOKEN_ADMIN, tableUserRetrieveDTO);
//
//            // 获取所有记录的队列，断言记录数量
//            List<TableUserVO> listVO = replyVabListVO.getData().getList();
//            Assert.notEmpty(listVO, "返回记录空");
//            Assert.isTrue(listVO.size() <= intActualRecordCount,
//                    "返回的记录数[{}] 大于 实际记录数[{}]", listVO.size(), intActualRecordCount);
//            for (TableUserVO tableUserVO : listVO) {
//                // TODO VO记录可能因未实例化造成无法获取需要的值
//                String strTextValue = getTextValue(tableUserVO);
//                Assert.isTrue(StrUtil.contains(strTextValue, strLike),
//                        "查询记录【{}】不匹配【{}】：", strTextValue, strLike);
//            }
//            // 断言对象字段符合要求
//            assertField(listVO);
//        }
//
//
//    }
//
//    @Test
//    @Order(4)
//    @DisplayName("测试分页查询")
//    @Tag("BASIC_TEST")
//    public void testQueryPage() throws Exception {
//        postLogin();
//
//        // 构造查询条件对象
//        TableUserRetrieveDTO tableUserRetrieveDTO = getTableUserRetrieveDTO();
//        tableUserRetrieveDTO.setIntPageSize(RandomUtil.randomInt(5, 15));
//
//        // 通过DAO获取实际记录数
//        Integer intActualRecordCount = tableUserDAO.getCount();
//        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");
//
//        {
//            // STR_URL_LIST_VIEW 权限测试
//            // 测试匿名用户无权限
//            testResultByReplyVO_402(mockMvc.perform(
//                    postJsonRequestBuilder(STR_URL_PAGE_VIEW, null, tableUserRetrieveDTO)));
//            // 测试普通用户有权限
//            testResultByReplyVO_200(mockMvc.perform(
//                    postJsonRequestBuilder(STR_URL_PAGE_VIEW, STR_TOKEN_USER, tableUserRetrieveDTO)));
//        }
//
//        // 页码总数
//        int intPageCount = intActualRecordCount / tableUserRetrieveDTO.getIntPageSize();
//        // 分页后的余数
//        int intPageRemainder = intActualRecordCount % tableUserRetrieveDTO.getIntPageSize();
//        // 如果分页余数大于0，则页码总数加1
//        if (intPageRemainder > 0) {
//            intPageCount++;
//        }
//
//        // 已存在的ID队列，用于验证记录是否重复
//        List<Integer> listExistId = new ArrayList<>();
//        // 模拟VAB从1开始查询所有分页
//        for (int i = 1; i <= intPageCount; i++) {
//            // 该分页预期的记录数
//            Integer intExpectedNumber = tableUserRetrieveDTO.getIntPageSize();
//            // 如果是否到最后一页，且余数大于0，则预期记录数为分页后的余数
//            if (i == intPageCount && intPageRemainder > 0) {
//                intExpectedNumber = intPageRemainder;
//            }
//
//            // 设置查询页码
//            tableUserRetrieveDTO.setIntPageNo(i - 1);
//
//            // 发送分页查询请求
//            VabList<TableUserVO> vabList = getPageVabList(tableUserRetrieveDTO);
//            List<TableUserVO> listVO = vabList.getList();
//            int intTotalCount = vabList.getTotal().intValue();
//
//            System.out.printf("总记录数【%d】当前页【%d/%d】本页记录数【预期%d/实际%s】\n",
//                    intTotalCount, tableUserRetrieveDTO.getIntPageNo() + 1,
//                    intPageCount, intExpectedNumber, listVO.size());
//
//            Assert.notEmpty(listVO, "返回记录空");
//            Assert.isTrue(intActualRecordCount == intTotalCount,
//                    "实际记录数量【{}】与查询到的总记录数【{}】不一致", intActualRecordCount, intTotalCount);
//            Assert.equals(listVO.size(), intExpectedNumber,
//                    "返回的记录数[{}]与页码[{}]不一致", listVO.size(), intExpectedNumber);
//
//            // 断言对象字段符合要求
//            assertField(listVO);
//
//            // 断言分页查询的记录不重复
//            assertNoRepeat(listVO, listExistId);
//        }
//    }
//
//    @Test
//    @Order(5)
//    @DisplayName("测试查询单条记录")
//    @Tag("BASIC_TEST")
//    public void testQueryRecord() throws Exception {
//        postLogin();
//
//        // 通过DAO获取所有记录
//        List<TableUserDO> listAllRecord = tableUserDAO.findAll();
//
//        // 循环获取单条记录
//        for (TableUserDO tableUserDO : listAllRecord) {
//            String strId = getIdValue(tableUserDO).toString();
//
//            {
//                // STR_URL_RECORD_VIEW
//                // 测试匿名用户无权限
//                testResultByReplyVO_402(mockMvc.perform(
//                        postJsonRequestBuilder(String.format(STR_URL_RECORD_VIEW, strId), null))
//                );
//                // 测试普通用户有权限
//                testResultByReplyVO_200(mockMvc.perform(
//                        postJsonRequestBuilder(String.format(STR_URL_RECORD_VIEW, strId), STR_TOKEN_USER))
//                );
//            }
//            // 发送获取单条记录请求
//            TableUserVO recordView = getRecordView(String.format(STR_URL_RECORD_VIEW, strId));
//            StrongUtils.compareBean(tableUserDO, recordView);
//
//            {
//                // STR_URL_UPDATE_VIEW
//                // 测试匿名用户无权限
//                testResultByReplyVO_402(mockMvc.perform(
//                        postJsonRequestBuilder(String.format(STR_URL_UPDATE_VIEW, strId), null))
//                );
//                // 测试普通用户无权限
//                testResultByReplyVO_403(mockMvc.perform(postJsonRequestBuilder(
//                        String.format(STR_URL_UPDATE_VIEW, strId), STR_TOKEN_USER))
//                );
//            }
//            // 发送获取单条记录请求
//            TableUserVO updateView = getRecordView(String.format(STR_URL_UPDATE_VIEW, strId));
//            StrongUtils.compareBean(tableUserDO, updateView);
//        }
//    }
//
//    @Test
//    @Order(6)
//    @DisplayName("测试修改数据")
//    @Tag("BASIC_TEST")
//    public void testUpdateData() throws Exception {
//        postLogin();
//
//        // 通过DAO获取所有记录
//        List<TableUserDO> listAllRecord = tableUserDAO.findAll();
//
//        // 循环修改记录
//        for (TableUserDO tableUserDO : listAllRecord) {
//            // 随机获取DTO用于生成修改请求的JSON
//            TableUserUpdateDTO tableUserUpdateDTO = getRandomUpdateDTO();
//            tableUserUpdateDTO.setUserId(getIdValue(tableUserDO));
//
//            {
//                // STR_URL_UPDATE_ACTION
//                // 测试匿名用户无权限
//                testResultByReplyVO_402(mockMvc.perform(
//                        postJsonRequestBuilder(STR_URL_UPDATE_ACTION, null, tableUserUpdateDTO))
//                );
//                // 测试普通用户无权限
//                testResultByReplyVO_403(mockMvc.perform(
//                        postJsonRequestBuilder(STR_URL_UPDATE_ACTION, STR_TOKEN_USER, tableUserUpdateDTO))
//                );
//            }
//            // 发送修改请求
//            TableUserVO tableUserVO = getUpdateAction(tableUserUpdateDTO);
//
//            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
//            StrongUtils.compareBean(tableUserUpdateDTO, tableUserVO, TableUserVO.STRS_EXCLUSION_PROPERTIES);
//
//            // 断言排除字段为空
//            assertField(tableUserVO);
//        }
//    }
//
//    /**
//     * 获取创建操作
//     *
//     * @param tableUserCreateDTO 表测试创建dto
//     * @return {@link TableUserVO }
//     * @throws Exception 异常
//     */
//    private TableUserVO getCreateAction(TableUserCreateDTO tableUserCreateDTO) throws Exception {
//        // 发送添加记录请求
//        MvcResult mvcResult = testResultByReplyVO_200(mockMvc.perform(
//                postJsonRequestBuilder(STR_URL_CREATE_ACTION, STR_TOKEN_ADMIN, tableUserCreateDTO)))
//                .andExpect(isReplyModel(TableUserVO.class))
//                .andReturn();
//
//        // 获取返回的ReplyVO对象
//        ReplyVO<TableUserVO> replyVO = JSON.parseReply(mvcResult.getResponse().getContentAsString(), TableUserVO.class);
//        return replyVO.getData();
//    }
//
//    /**
//     * 获取所有列表
//     *
//     * @param strTokenAdmin jwt令牌
//     * @return {@link ReplyVabListVO }<{@link Map }<{@link String }, {@link Object }>>
//     * @throws Exception 异常
//     */
//    private ReplyVabListVO<TableUserVO> getAllList(String strTokenAdmin) throws Exception {
//        return getAllList(strTokenAdmin, getTableUserRetrieveDTO());
//    }
//
//    /**
//     * 获取所有列表
//     *
//     * @param strTokenAdmin        jwt令牌
//     * @param tableUserRetrieveDTO 表测试检索
//     * @return {@link ReplyVabListVO }<{@link Map }<{@link String }, {@link Object }>>
//     * @throws Exception 异常
//     */
//    private ReplyVabListVO<TableUserVO> getAllList(String strTokenAdmin, TableUserRetrieveDTO tableUserRetrieveDTO) throws Exception {
//        // 获取所有记录的队列
//        MvcResult mvcResult = testResultByReplyVO_200(mockMvc.perform(
//                postJsonRequestBuilder(STR_URL_LIST_VIEW, strTokenAdmin, tableUserRetrieveDTO)))
//                .andExpect(isReplyList(TableUserVO.class))
//                .andReturn();
//
//        return JSON.parseReplyVabList(mvcResult.getResponse().getContentAsString(), TableUserVO.class);
//    }
//
//    /**
//     * 获取分页查询对象
//     *
//     * @param tableUserRetrieveDTO 表测试检索
//     * @return {@link VabList }<{@link TableUserVO }>
//     * @throws Exception 异常
//     */
//    private VabList<TableUserVO> getPageVabList(TableUserRetrieveDTO tableUserRetrieveDTO) throws Exception {
//        // 获取所有记录的队列，断言记录数量
//        MvcResult mvcResult = testResultByReplyVO_200(mockMvc.perform(
//                postJsonRequestBuilder(STR_URL_PAGE_VIEW, STR_TOKEN_ADMIN, tableUserRetrieveDTO)))
//                .andExpect(isReplyList(VabList.class))
//                .andReturn();
//        ReplyVabListVO<TableUserVO> replyVabListVO = JSON.parseReplyVabList(mvcResult.getResponse().getContentAsString(), TableUserVO.class);
//        return replyVabListVO.getData();
//    }
//
//    /**
//     * 获取记录视图
//     *
//     * @param strUrl str url
//     * @return {@link TableUserVO }
//     * @throws Exception 异常
//     */
//    private TableUserVO getRecordView(String strUrl) throws Exception {
//        // 发送获取单条记录请求
//        MvcResult mvcResultRecord = testResultByReplyVO_200(
//                mockMvc.perform(postJsonRequestBuilder(strUrl, STR_TOKEN_ADMIN))
//        ).andExpect(isReplyModel()).andReturn();
//        ReplyVO<TableUserVO> replyRecord = JSON.parseReply(mvcResultRecord.getResponse().getContentAsString(), TableUserVO.class);
//        return replyRecord.getData();
//    }
//
//    /**
//     * 获取更新操作
//     *
//     * @param tableUserUpdateDTO 表测试更新
//     * @return {@link TableUserVO }
//     * @throws Exception 异常
//     */
//    private TableUserVO getUpdateAction(TableUserUpdateDTO tableUserUpdateDTO) throws Exception {
//        // 发送添加记录请求
//        MvcResult mvcResult = testResultByReplyVO_200(mockMvc.perform(
//                postJsonRequestBuilder(STR_URL_UPDATE_ACTION, STR_TOKEN_ADMIN, tableUserUpdateDTO)))
//                .andExpect(isReplyModel(TableUserVO.class))
//                .andReturn();
//
//        // 获取返回的ReplyVO对象
//        ReplyVO<TableUserVO> replyVO = JSON.parseReply(mvcResult.getResponse().getContentAsString(), TableUserVO.class);
//        return replyVO.getData();
//    }
//
//    /**
//     * 删除所有记录
//     *
//     * @param strTokenAdmin jwt令牌
//     * @throws Exception 异常
//     */
//    private void deleteAllRecord(String strTokenAdmin) throws Exception {
//        deleteRecord(getAllList(strTokenAdmin), strTokenAdmin);
//    }
//
//    /**
//     * 删除所有记录
//     *
//     * @param replyList     返回的Reply队列
//     * @param strTokenAdmin jwt令牌
//     * @throws Exception 异常
//     */
//    private void deleteRecord(ReplyVabListVO<TableUserVO> replyList, String strTokenAdmin) throws Exception {
//        if (ObjUtil.hasNull(replyList, replyList.getData())) {
//            return;
//        }
//        List<TableUserVO> listTableUserVO = replyList.getData().getList();
//        if (IterUtil.isNotEmpty(listTableUserVO)) {
//            int intRecordCount = listTableUserVO.size();
//            // 遍历一半记录，逐条删除
//            int intSplit = intRecordCount / 2;
//            for (int i = 0; i < intSplit; i++) {
//                TableUserVO tableUserVO = listTableUserVO.get(i);
//                Integer intId = getIdValue(tableUserVO);
//                testResultByReplyVO_200(mockMvc.perform(
//                        postJsonRequestBuilder(
//                                String.format(STR_URL_DELETE_ACTION, intId),
//                                strTokenAdmin)
//                )).andExpect(isReplyModel(TableUserVO.class));
//
//                // 根据ID查询DO，断言其为空
//                Optional<TableUserDO> optional = tableUserDAO.findById(intId);
//                Assert.isTrue(optional.isEmpty(), "已删除的DO对象依旧存在：【ID：{}】", intId);
//
//                // 断言记录数
//                Integer intRecordCountTemp = tableUserDAO.getCount();
//                intRecordCount--;
//                Assert.equals(intRecordCount, intRecordCountTemp,
//                        "预期记录数{} 不等于当前记录数{}", intRecordCount, intRecordCountTemp);
//            }
//
//            // 遍历后半记录，合并删除
//            List<Integer> listID = new ArrayList<>();
//            for (int i = intSplit; i < intSplit + intRecordCount; i++) {
//                TableUserVO tableUserVO = listTableUserVO.get(i);
//                listID.add(getIdValue(tableUserVO));
//            }
//
//            Integer[] intsID = ArrayUtil.toArray(listID, Integer.class);
//            String strIds = ArrayUtil.join(intsID, ",");
//            testResultByReplyVO_200(mockMvc.perform(
//                    postJsonRequestBuilder(String.format(STR_URL_DELETE_ACTION, strIds), strTokenAdmin)))
//                    .andExpect(isReplyModel(TableUserVO.class));
//
//            List<TableUserDO> listTableUserDO = tableUserDAO.findAllByIdIn(listID);
//            Assert.isTrue(CollUtil.isEmpty(listTableUserDO), "删除的记录依旧存在{}", strIds);
//
//            // 断言记录数
//            Integer intRecordCountTemp = tableUserDAO.getCount();
//            Assert.equals(0, intRecordCountTemp,
//                    "预期记录数{} 不等于当前记录数{}", 0, intRecordCountTemp);
//        }
//    }
//
//    /**
//     * 获取查询条件
//     *
//     * @return {@link TableUserRetrieveDTO }
//     */
//    private TableUserRetrieveDTO getTableUserRetrieveDTO() {
//        // 构建查询条件
//        TableUserRetrieveDTO tableUserRetrieveDTO = new TableUserRetrieveDTO();
//        tableUserRetrieveDTO.setSort(Sort.by(STR_TEXT_FILED_NAME));
//        return tableUserRetrieveDTO;
//    }
//
//    /**
//     * 获取主键id内容
//     *
//     * @param objBean obj豆
//     * @return {@link Integer }
//     */
//    private Integer getIdValue(Object objBean) {
//        return StrongUtils.getIntByFiledName(objBean, STR_ID_FILED_NAME);
//    }
//
//    /**
//     * 获取测试用文本字段内容
//     *
//     * @param objBean obj豆
//     * @return {@link Integer }
//     */
//    private String getTextValue(Object objBean) {
//        return StrongUtils.getStringByFiledName(objBean, STR_TEXT_FILED_NAME);
//    }
//
//    /**
//     * 遍历队列 断言对象字段符合要求
//     *
//     * @param listVO 签证官列表
//     */
//    private void assertField(List<TableUserVO> listVO) {
//        Integer intId = null;
//        for (TableUserVO tableUserVO : listVO) {
//            Integer intIdTemp = getIdValue(tableUserVO);
//            if (ObjUtil.isNull(intId)) {
//                intId = intIdTemp;
//            } else {
//                Assert.isTrue(intIdTemp >= intId,
//                        "当前记录【{}】与前一条记录【{}】,排序规则不匹配", intIdTemp, intId);
//            }
//            assertField(tableUserVO);
//        }
//    }
//
//    /**
//     * 断言对象字段符合要求
//     *
//     * @param tableUserVO 签证官列表
//     */
//    private void assertField(TableUserVO tableUserVO) {
//        // 遍历断言tableUserVO的排除字段都为空
//        for (String strProperty : TableUserVO.STRS_EXCLUSION_PROPERTIES) {
//            Assert.isNull(BeanUtil.getProperty(tableUserVO, strProperty),
//                    "{}字段非空\n{}", strProperty, JSON.toJSONString(tableUserVO));
//        }
//    }
//
//    /**
//     * 断言分页查询的记录不重复
//     *
//     * @param listVO      VO队列
//     * @param listExistId 已存在的ID队列
//     */
//    private void assertNoRepeat(List<TableUserVO> listVO, List<Integer> listExistId) {
//        // 对比ID队列，断言分页的所有记录为唯一
//        for (TableUserVO tableUserVO : listVO) {
//            Integer intId = getIdValue(tableUserVO);
//            Assert.isTrue(!CollUtil.contains(listExistId, intId), "ID【{}】重复取出", intId);
//
//            // 断言排除字段为空
//            assertField(tableUserVO);
//        }
//
//        // 将单条记录的ID加入到队列，用于对比是否重复
//        listExistId.addAll(listVO.stream().map(this::getIdValue).toList());
//    }

}