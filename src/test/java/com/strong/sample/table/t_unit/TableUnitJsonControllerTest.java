package com.strong.sample.table.t_unit;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.config.GlobalExceptionHandler;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_unit.jpa.TableUnitDAO;
import com.strong.sample.table.t_unit.jpa.TableUnitDO;
import com.strong.sample.table.t_unit.model.TableUnitCreateDTO;
import com.strong.sample.table.t_unit.model.TableUnitRetrieveDTO;
import com.strong.sample.table.t_unit.model.TableUnitUpdateDTO;
import com.strong.sample.table.t_unit.model.TableUnitVO;
import com.strong.sample.utils.MockMvcUtils;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.vab.VabList;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.strong.sample.utils.MockMatcher.isReplyString;
import static com.strong.utils.mvc.MvcConstants.*;

/**
 * 单位表json控制器测试
 *
 * @author simen
 * @date 2024/08/20
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableUnitJsonControllerTest extends SpringbootTests {

    /**
     * 测试用URL
     */
    public final static String STR_URL_RECORD_VIEW = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_RECORD_VIEW, "%s");
    public final static String STR_URL_CREATE_ACTION = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_CREATE_ACTION);
    public final static String STR_URL_UPDATE_ACTION = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_UPDATE_ACTION);
    public final static String STR_URL_UPDATE_VIEW = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_UPDATE_VIEW, "%s");
    public final static String STR_URL_DELETE_ACTION = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_DELETE_ACTION, "%s");
    public final static String STR_URL_LIST_VIEW = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_LIST_VIEW);
    public final static String STR_URL_PAGE_VIEW = StrUtil.join("/", TableUnitConstants.TABLE_ENTITY, URL_PAGE_VIEW);

    /**
     * 拟测试记录数量
     */
    private static final Integer INT_RECORD_COUNT = RandomUtil.randomInt(100, 999);

    /**
     * 主键ID字段名
     */
    private static final String STR_ID_FILED_NAME = TableUnitConstants.UNIT_ID_ENTITY;

    /**
     * 测试用文本字段名
     */
    private static final String STR_TEXT_FILED_NAME = TableUnitConstants.UNIT_NAME_ENTITY;

    /**
     * 模拟mvc对象
     */
    @Autowired
    public MockMvc mockMvc;

    /**
     * 模拟mvc工具对象
     */
    @Autowired
    public MockMvcUtils mockMvcUtils;

    /**
     * 数据库操作类
     */
    @Autowired
    TableUnitDAO tableUnitDAO;

    @Test
    @Order(1)
    @DisplayName("测试删除数据")
    @Tag("BASIC_TEST")
    public void testDeleteData() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableUnitDAO.getCount();
        // 如果初始记录为空，则先执行添加操作
        if (intActualRecordCount == 0) {
            testAddData();
            intActualRecordCount = tableUnitDAO.getCount();
        }
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        // 删除所有记录
        deleteRecord(postListReplyVO(STR_URL_LIST_VIEW, getTableUnitRetrieveDTO()).getList());
    }

    @Test
    @Order(2)
    @DisplayName("测试添加数据")
    @Tag("BASIC_TEST")
    public void testAddData() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intOriginalRecordCount = tableUnitDAO.getCount();

        // 循环添加记录
        for (int i = 0; i < INT_RECORD_COUNT; i++) {
            // 随机获取DTO用于生成创建请求的JSON
            TableUnitCreateDTO tableUnitCreateDTO = TableUnitServiceTest.getRandomCreateDTO();

            {
                // STR_URL_CREATE_ACTION 权限测试
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_CREATE_ACTION, null, tableUnitCreateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_CREATE_ACTION, STR_TOKEN_USER, tableUnitCreateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }

            // 发送添加请求
            TableUnitVO tableUnitVO = postReturnReplyVO(STR_URL_CREATE_ACTION, tableUnitCreateDTO);
            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(tableUnitCreateDTO, tableUnitVO,
                    ArrayUtil.append(TableUnitVO.STRS_EXCLUSION_PROPERTIES, STR_ID_FILED_NAME));
            // 断言排除字段为空
            assertField(tableUnitVO);
        }

        // 通过DAO获取实际记录数
        int intActualRecordCount = tableUnitDAO.getCount();
        // 预期记录数量
        int intPredictRecordCount = intOriginalRecordCount + INT_RECORD_COUNT;
        Assert.isTrue(intPredictRecordCount == intActualRecordCount,
                "添加记录数量【{}】与实际数量【{}】不一致", intPredictRecordCount, intActualRecordCount);
    }

    @Test
    @Order(3)
    @DisplayName("测试条件查询所有记录")
    @Tag("BASIC_TEST")
    public void testQueryAll() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableUnitDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        String strLike = "a";

        TableUnitRetrieveDTO tableUnitRetrieveDTO = getTableUnitRetrieveDTO();

        {
            // STR_URL_LIST_VIEW 权限测试
            // 测试匿名用户无权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_LIST_VIEW, null, tableUnitRetrieveDTO, ReplyEnum.ERROR_SERVER_ERROR)
                    .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            // 测试普通用户有权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_LIST_VIEW, STR_TOKEN_USER, tableUnitRetrieveDTO, ReplyEnum.SUCCESS_RETURN_DATA);
        }

        { // ============ 完整查询记录测试 ============
            // 发送获取完整记录请求
            List<TableUnitVO> listVO = postListReplyVO(STR_URL_LIST_VIEW, tableUnitRetrieveDTO).getList();
            Assert.notEmpty(listVO, "返回记录空");
            Assert.equals(listVO.size(), intActualRecordCount,
                    "返回的记录数[{}]与实际记录数[{}]不一致", listVO.size(), intActualRecordCount);
            // 断言对象字段符合要求
            assertField(listVO);
        }

        { // ============ 条件查询记录测试 ============
            tableUnitRetrieveDTO.addSearch(STR_TEXT_FILED_NAME, String.format("%%%s%%", strLike));
            // 发送获取完整记录请求
            List<TableUnitVO> listVO = postListReplyVO(STR_URL_LIST_VIEW, tableUnitRetrieveDTO).getList();
            Assert.notEmpty(listVO, "返回记录空");
            Assert.isTrue(listVO.size() <= intActualRecordCount,
                    "返回的记录数[{}] 大于 实际记录数[{}]", listVO.size(), intActualRecordCount);
            for (TableUnitVO tableUnitVO : listVO) {
                // TODO VO记录可能因未实例化造成无法获取需要的值
                String strTextValue = getTextValue(tableUnitVO);
                Assert.isTrue(StrUtil.contains(strTextValue, strLike),
                        "查询记录【{}】不匹配【{}】：", strTextValue, strLike);
            }
            // 断言对象字段符合要求
            assertField(listVO);
        }
    }

    @Test
    @Order(4)
    @DisplayName("测试分页查询")
    @Tag("BASIC_TEST")
    public void testQueryPage() throws Exception {
        postLogin();

        // 构造查询条件对象
        TableUnitRetrieveDTO tableUnitRetrieveDTO = getTableUnitRetrieveDTO();
        tableUnitRetrieveDTO.setIntPageSize(RandomUtil.randomInt(5, 15));

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableUnitDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        {
            // STR_URL_PAGE_VIEW 权限测试
            // 测试匿名用户无权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_PAGE_VIEW, null, tableUnitRetrieveDTO, ReplyEnum.ERROR_SERVER_ERROR)
                    .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            // 测试普通用户有权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_PAGE_VIEW, STR_TOKEN_USER, tableUnitRetrieveDTO, ReplyEnum.SUCCESS_RETURN_DATA);
        }

        // 页码总数
        int intPageCount = intActualRecordCount / tableUnitRetrieveDTO.getIntPageSize();
        // 分页后的余数
        int intPageRemainder = intActualRecordCount % tableUnitRetrieveDTO.getIntPageSize();
        // 如果分页余数大于0，则页码总数加1
        if (intPageRemainder > 0) {
            intPageCount++;
        }

        // 已存在的ID队列，用于验证记录是否重复
        List<Integer> listExistId = new ArrayList<>();
        // 模拟VAB从1开始查询所有分页
        for (int i = 1; i <= intPageCount; i++) {
            // 该分页预期的记录数
            Integer intExpectedNumber = tableUnitRetrieveDTO.getIntPageSize();
            // 如果是否到最后一页，且余数大于0，则预期记录数为分页后的余数
            if (i == intPageCount && intPageRemainder > 0) {
                intExpectedNumber = intPageRemainder;
            }

            // 设置查询页码
            tableUnitRetrieveDTO.setIntPageNo(i - 1);

            // 发送分页查询请求
            VabList<TableUnitVO> vabList = postListReplyVO(STR_URL_PAGE_VIEW, tableUnitRetrieveDTO);
            List<TableUnitVO> listVO = vabList.getList();
            int intTotalCount = vabList.getTotal().intValue();

            System.out.printf("总记录数【%d】当前页【%d/%d】本页记录数【预期%d/实际%s】\n",
                    intTotalCount, tableUnitRetrieveDTO.getIntPageNo() + 1,
                    intPageCount, intExpectedNumber, listVO.size());

            Assert.notEmpty(listVO, "返回记录空");
            Assert.isTrue(intActualRecordCount == intTotalCount,
                    "实际记录数量【{}】与查询到的总记录数【{}】不一致", intActualRecordCount, intTotalCount);
            Assert.equals(listVO.size(), intExpectedNumber,
                    "返回的记录数[{}]与页码[{}]不一致", listVO.size(), intExpectedNumber);

            // 断言对象字段符合要求
            assertField(listVO);

            // 断言分页查询的记录不重复
            assertNoRepeat(listVO, listExistId);
        }
    }

    @Test
    @Order(5)
    @DisplayName("测试查询单条记录")
    @Tag("BASIC_TEST")
    public void testQueryRecord() throws Exception {
        postLogin();

        // 通过DAO获取所有记录
        List<TableUnitDO> listAllRecord = tableUnitDAO.findAll();
        System.out.println(listAllRecord.size());

        // 循环获取单条记录
        for (TableUnitDO tableUnitDO : listAllRecord) {
            String strId = getIdValue(tableUnitDO).toString();
            String strUrlRecordId = String.format(STR_URL_RECORD_VIEW, strId);
            String strUrlUpdateId = String.format(STR_URL_UPDATE_VIEW, strId);

            {
                // STR_URL_RECORD_VIEW
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(strUrlRecordId, null, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户有权限
                mockMvcUtils.postReturnResultReplyVO(strUrlRecordId, STR_TOKEN_USER, null, ReplyEnum.SUCCESS_RETURN_DATA);
            }
            // 发送获取单条记录请求
            TableUnitVO recordView = mockMvcUtils.postReturnReplyVO(strUrlRecordId, STR_TOKEN_ADMIN, null,
                    ReplyEnum.SUCCESS_RETURN_DATA, TableUnitVO.class, StrongUtils.getFiledNames(TableUnitVO.class));
            StrongUtils.compareBean(tableUnitDO, recordView);

            {
                // STR_URL_UPDATE_VIEW
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(strUrlUpdateId, null, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户有权限
                mockMvcUtils.postReturnResultReplyVO(strUrlUpdateId, STR_TOKEN_USER, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }
            // 发送获取单条记录请求
            TableUnitVO updateView = mockMvcUtils.postReturnReplyVO(strUrlUpdateId, STR_TOKEN_ADMIN, null,
                    ReplyEnum.SUCCESS_RETURN_DATA, TableUnitVO.class, StrongUtils.getFiledNames(TableUnitVO.class));
            StrongUtils.compareBean(tableUnitDO, updateView);
        }
    }

    @Test
    @Order(6)
    @DisplayName("测试修改数据")
    @Tag("BASIC_TEST")
    public void testUpdateData() throws Exception {
        postLogin();

        // 通过DAO获取所有记录
        List<TableUnitDO> listAllRecord = tableUnitDAO.findAll();

        // 循环修改记录
        for (TableUnitDO tableUnitDO : listAllRecord) {
            // 随机获取DTO用于生成修改请求的JSON
            TableUnitUpdateDTO tableUnitUpdateDTO = TableUnitServiceTest.getRandomUpdateDTO();
            tableUnitUpdateDTO.setUnitId(getIdValue(tableUnitDO));

            {
                // STR_URL_CREATE_ACTION 权限测试
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_UPDATE_ACTION, null, tableUnitUpdateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_UPDATE_ACTION, STR_TOKEN_USER, tableUnitUpdateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }
            // 发送修改请求
            TableUnitVO tableUnitVO = postReturnReplyVO(STR_URL_UPDATE_ACTION, tableUnitUpdateDTO);

            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(tableUnitUpdateDTO, tableUnitVO, TableUnitVO.STRS_EXCLUSION_PROPERTIES);

            // 断言排除字段为空
            assertField(tableUnitVO);
        }
    }

    /**
     * 删除所有记录
     *
     * @throws Exception 异常
     */
    private void deleteRecord(List<TableUnitVO> listTableUnitVO) throws Exception {
        Assert.notEmpty(listTableUnitVO, "待删除记录队列为空");
        int intRecordCount = listTableUnitVO.size();
        // 遍历一半记录，逐条删除
        int intSplit = intRecordCount / 2;
        for (int i = 0; i < intSplit; i++) {
            TableUnitVO tableUnitVO = listTableUnitVO.get(i);
            Integer intId = getIdValue(tableUnitVO);

            postReturnReplyVO(String.format(STR_URL_DELETE_ACTION, intId), null);

            // 根据ID查询DO，断言其为空
            Optional<TableUnitDO> optional = tableUnitDAO.findById(intId);
            Assert.isTrue(optional.isEmpty(), "已删除的DO对象依旧存在：【ID：{}】", intId);

            // 断言记录数
            Integer intRecordCountTemp = tableUnitDAO.getCount();
            intRecordCount--;
            Assert.equals(intRecordCount, intRecordCountTemp,
                    "预期记录数{} 不等于当前记录数{}", intRecordCount, intRecordCountTemp);
        }

        // 遍历后半记录，合并删除
        List<Integer> listID = new ArrayList<>();
        for (int i = intSplit; i < intSplit + intRecordCount; i++) {
            TableUnitVO tableUnitVO = listTableUnitVO.get(i);
            listID.add(getIdValue(tableUnitVO));
        }

        Integer[] intsID = ArrayUtil.toArray(listID, Integer.class);
        String strIds = ArrayUtil.join(intsID, ",");

        postReturnReplyVO(String.format(STR_URL_DELETE_ACTION, strIds), null);

        List<TableUnitDO> listTableUnitDO = tableUnitDAO.findAllByIdIn(listID);
        Assert.isTrue(CollUtil.isEmpty(listTableUnitDO), "删除的记录依旧存在{}", strIds);

        // 断言记录数
        Integer intRecordCountTemp = tableUnitDAO.getCount();
        Assert.equals(0, intRecordCountTemp,
                "预期记录数{} 不等于当前记录数{}", 0, intRecordCountTemp);
    }

    /**
     * 获取查询条件
     *
     * @return {@link TableUnitRetrieveDTO }
     */
    private TableUnitRetrieveDTO getTableUnitRetrieveDTO() {
        // 构建查询条件
        TableUnitRetrieveDTO tableUnitRetrieveDTO = new TableUnitRetrieveDTO();
        tableUnitRetrieveDTO.setSort(Sort.by(STR_TEXT_FILED_NAME));
        return tableUnitRetrieveDTO;
    }

    /**
     * 获取post提交返回的视图
     *
     * @param strURL   URL地址
     * @param objModel 提交的数据模型
     * @return {@link TableUnitVO }
     * @throws Exception 异常
     */
    private TableUnitVO postReturnReplyVO(String strURL, Object objModel) throws Exception {
        return mockMvcUtils.postReturnReplyVO(strURL, STR_TOKEN_ADMIN, objModel, TableUnitVO.class);
    }

    /**
     * 获取post提交返回的列表视图
     *
     * @param strURL   URL地址
     * @param objModel 提交的数据模型
     * @return {@link VabList }<{@link TableUnitVO }>
     * @throws Exception 异常
     */
    private VabList<TableUnitVO> postListReplyVO(String strURL, Object objModel) throws Exception {
        return mockMvcUtils.postReturnListReplyVO(strURL, STR_TOKEN_ADMIN, objModel, TableUnitVO.class);
    }

    /**
     * 获取主键id内容
     *
     * @param objBean obj豆
     * @return {@link Integer }
     */
    private Integer getIdValue(Object objBean) {
        return StrongUtils.getIntByFiledName(objBean, STR_ID_FILED_NAME);
    }

    /**
     * 获取测试用文本字段内容
     *
     * @param objBean obj豆
     * @return {@link Integer }
     */
    private String getTextValue(Object objBean) {
        return StrongUtils.getStringByFiledName(objBean, STR_TEXT_FILED_NAME);
    }

    /**
     * 遍历队列 断言对象字段符合要求
     *
     * @param listVO 签证官列表
     */
    private void assertField(List<TableUnitVO> listVO) {
        Integer intId = null;
        for (TableUnitVO tableUnitVO : listVO) {
            Integer intIdTemp = getIdValue(tableUnitVO);
            if (ObjUtil.isNull(intId)) {
                intId = intIdTemp;
            } else {
                Assert.isTrue(intIdTemp >= intId,
                        "当前记录【{}】与前一条记录【{}】,排序规则不匹配", intIdTemp, intId);
            }
            assertField(tableUnitVO);
        }
    }

    /**
     * 断言对象字段符合要求
     *
     * @param tableUnitVO 签证官列表
     */
    private void assertField(TableUnitVO tableUnitVO) {
        // 遍历断言tableUnitVO的排除字段都为空
        for (String strProperty : TableUnitVO.STRS_EXCLUSION_PROPERTIES) {
            Assert.isNull(BeanUtil.getProperty(tableUnitVO, strProperty),
                    "{}字段非空\n{}", strProperty, JSON.toJSONString(tableUnitVO));
        }
    }

    /**
     * 断言分页查询的记录不重复
     *
     * @param listVO      VO队列
     * @param listExistId 已存在的ID队列
     */
    private void assertNoRepeat(List<TableUnitVO> listVO, List<Integer> listExistId) {
        // 对比ID队列，断言分页的所有记录为唯一
        for (TableUnitVO tableUnitVO : listVO) {
            Integer intId = getIdValue(tableUnitVO);
            Assert.isTrue(!CollUtil.contains(listExistId, intId), "ID【{}】重复取出", intId);

            // 断言排除字段为空
            assertField(tableUnitVO);
        }

        // 将单条记录的ID加入到队列，用于对比是否重复
        listExistId.addAll(listVO.stream().map(this::getIdValue).toList());
    }

}