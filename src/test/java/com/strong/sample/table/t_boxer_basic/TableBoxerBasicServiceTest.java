package com.strong.sample.table.t_boxer_basic;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.Faker;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_boxer_basic.jpa.TableBoxerBasicDAO;
import com.strong.sample.table.t_boxer_basic.jpa.TableBoxerBasicDO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicCreateDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicRetrieveDTO;
import com.strong.sample.table.t_boxer_basic.model.TableBoxerBasicUpdateDTO;
import com.strong.sample.table.t_boxer_basic.service.TableBoxerBasicService;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.cache.CacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

/**
 * 运动员基本信息表Service测试
 *
 * @author simen
 * @date 2024/11/19
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableBoxerBasicServiceTest extends SpringbootTests {

    /**
     * 数据库操作类
     */
    @Autowired
    TableBoxerBasicDAO tableBoxerBasicDAO;

    /**
     * Service
     */
    @Autowired
    TableBoxerBasicService tableBoxerBasicService;

    /**
     * 缓存工具类
     */
    @Autowired
    CacheUtils cacheUtils;

    /**
     * 拟测试记录数量
     */
    private static final Integer INT_RECORD_COUNT = RandomUtil.randomInt(100, 999);

    /**
     * 循环次数
     */
    private static final Integer INT_LOOP_COUNT = RandomUtil.randomInt(10, 99);

    /**
     * 主键ID字段名
     */
    private static final String STR_ID_FILED_NAME = TableBoxerBasicConstants.BOXER_ID_ENTITY;

    /**
     * 测试用文本字段名
     */
    private static final String STR_TEXT_FILED_NAME = TableBoxerBasicConstants.BOXER_NAME_ENTITY;

    static {
        Faker.initialize();
    }

    @Test
    @Order(1)
    @DisplayName("测试添加数据")
    @Tag("BASIC_TEST")
    public void testAddData() {
        // 清空所有记录
        tableBoxerBasicDAO.deleteAllInBatch();

        // 循环添加记录
        for (int i = 0; i < INT_RECORD_COUNT; i++) {
            TableBoxerBasicCreateDTO modelCreate = getRandomCreateDTO();
            tableBoxerBasicService.getCreateAction(modelCreate);
        }
    }

    @Test
    @Order(2)
    @DisplayName("测试条件查询所有记录")
    @Tag("BASIC_TEST")
    public void testQueryAll() {
        // 清空缓存
        tableBoxerBasicService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        String strLike = "a";

        // 构建查询条件
        TableBoxerBasicRetrieveDTO tableBoxerBasicRetrieveDTO = getTableBoxerBasicRetrieveDTO();
        tableBoxerBasicRetrieveDTO.addSearch(STR_TEXT_FILED_NAME, String.format("%%%s%%", strLike));

        // 根据查询条件获取所有记录
        List<TableBoxerBasicDO> listTableBoxerBasicDO = tableBoxerBasicService.getAllList(tableBoxerBasicRetrieveDTO);
        for (TableBoxerBasicDO tableBoxerBasicDO : listTableBoxerBasicDO) {
            String strTextValue = getTextValue(tableBoxerBasicDO);
            Assert.isTrue(StrUtil.contains(strTextValue, strLike),
                    "查询记录【{}】不匹配【{}】：", strTextValue, strLike);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableBoxerBasicConstants.TABLE_ENTITY, "getAllList", listTableBoxerBasicDO.size());
    }

    @Test
    @Order(3)
    @DisplayName("测试分页查询")
    @Tag("BASIC_TEST")
    public void testQueryPage() {
        // 清空缓存
        tableBoxerBasicService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        // 构建查询条件
        TableBoxerBasicRetrieveDTO retrievePageDTO = getTableBoxerBasicRetrieveDTO();
        retrievePageDTO.setIntPageSize(RandomUtil.randomInt(5, 15));

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableBoxerBasicDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        // 页码总数
        int intPageCount = intActualRecordCount / retrievePageDTO.getIntPageSize();
        // 分页后的余数
        int intPageRemainder = intActualRecordCount % retrievePageDTO.getIntPageSize();
        // 如果分页余数大于0，则页码总数加1
        if (intPageRemainder > 0) {
            intPageCount++;
        }

        // 循环查询分页记录
        for (int i = 1; i <= intPageCount; i++) {
            // 该分页预期的记录数
            Integer intExpectedNumber = retrievePageDTO.getIntPageSize();
            // 如果是否到最后一页，且余数大于0，则预期记录数为分页后的余数
            if (i == intPageCount && intPageRemainder > 0) {
                intExpectedNumber = intPageRemainder;
            }

            // 设置查询页码
            retrievePageDTO.setIntPageNo(i - 1);

            // 查询获取分页对象
            Page<TableBoxerBasicDO> pageTableBoxerBasicDO = tableBoxerBasicService.getPageList(retrievePageDTO);
            Assert.notNull(pageTableBoxerBasicDO, "返回对象为空");

            // 获取分页队列
            List<TableBoxerBasicDO> listPageContent = pageTableBoxerBasicDO.getContent();
            Assert.notEmpty(listPageContent, "返回记录队列为空");

            System.out.printf("总记录数【%d】当前页【%d/%d】本页记录数【预期%d/实际%s】\n",
                    intActualRecordCount, retrievePageDTO.getIntPageNo() + 1,
                    intPageCount, intExpectedNumber, listPageContent.size());

            Assert.isTrue(intActualRecordCount == pageTableBoxerBasicDO.getTotalElements(),
                    "实际记录数量【{}】与查询到的总记录数【{}】不一致",
                    intActualRecordCount, pageTableBoxerBasicDO.getTotalElements());
            Assert.equals(listPageContent.size(), intExpectedNumber,
                    "返回的记录数[{}]与页码[{}]不一致", listPageContent.size(), intExpectedNumber);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableBoxerBasicConstants.TABLE_ENTITY, "getPageList", intActualRecordCount);
    }

    @Test
    @Order(4)
    @DisplayName("测试查询单条记录")
    @Tag("BASIC_TEST")
    public void testQueryRecord() {
        // 清空缓存
        tableBoxerBasicService.cacheEvict();
        // 断言缓存被强制清空
        cacheUtils.assertsCacheEvicted();

        List<TableBoxerBasicDO> listTableBoxerBasicDO = tableBoxerBasicDAO.findAll();
        // 遍历记录断言每条记录非空
        for (TableBoxerBasicDO tableBoxerBasicDO : listTableBoxerBasicDO) {
            Integer intId = getIdValue(tableBoxerBasicDO);
            TableBoxerBasicDO tableBoxerBasicDORecord = tableBoxerBasicService.getRecord(intId);
            Assert.notNull(tableBoxerBasicDORecord, "查询到的记录为空：ID={}", intId);
        }

        // 断言缓存数量
        cacheUtils.assertHasCache(TableBoxerBasicConstants.TABLE_ENTITY, "getRecord", listTableBoxerBasicDO.size());
    }

    @Test
    @Order(5)
    @DisplayName("测试修改数据")
    @Tag("BASIC_TEST")
    public void TestUpdateData() {
        // 编修修改所有记录
        TableBoxerBasicRetrieveDTO tableBoxerBasicRetrieveDTO = getTableBoxerBasicRetrieveDTO();
        List<TableBoxerBasicDO> listTableBoxerBasicDO = tableBoxerBasicService.getAllList(tableBoxerBasicRetrieveDTO);

        for (TableBoxerBasicDO tableBoxerBasicDO : listTableBoxerBasicDO) {
            Integer intId = getIdValue(tableBoxerBasicDO);

            TableBoxerBasicUpdateDTO modelUpdate = getRandomUpdateDTO();
            modelUpdate.setBoxerId(intId);

            TableBoxerBasicDO updateDO = tableBoxerBasicService.getUpdateAction(modelUpdate);

            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(modelUpdate, updateDO);
        }
    }

    @Test
    @Order(6)
    @DisplayName("测试删除数据")
    @Tag("BASIC_TEST")
    public void testDeleteData() {
        // 构建查询条件
        TableBoxerBasicRetrieveDTO tableBoxerBasicRetrieveDTO = getTableBoxerBasicRetrieveDTO();
        // 获取所有记录
        List<TableBoxerBasicDO> listTableBoxerBasicVO = tableBoxerBasicService.getAllList(tableBoxerBasicRetrieveDTO);

        int intSplit = listTableBoxerBasicVO.size() / 2;
        Integer[] intsId = new Integer[listTableBoxerBasicVO.size() - intSplit];
        for (int i = 0; i < listTableBoxerBasicVO.size(); i++) {
            TableBoxerBasicDO tableBoxerBasicVO = listTableBoxerBasicVO.get(i);
            Integer intId = getIdValue(tableBoxerBasicVO);

            // 遍历记录，前一半逐条删除，后一半合并数组一次性删除
            if (i < intSplit) {
                // 发送删除请求
                tableBoxerBasicService.getDeleteAction(intId);
                // 根据ID查询DO，断言其为空
                Optional<TableBoxerBasicDO> optional = tableBoxerBasicDAO.findById(intId);
                Assert.isTrue(optional.isEmpty(), "已删除的DO对象依旧存在：【{}】", JSON.toJSONString(optional));
            } else {
                intsId[i - intSplit] = intId;
            }
        }

        // 删除剩余记录
        tableBoxerBasicService.getDeleteAction(intsId);
        Integer intCount = tableBoxerBasicDAO.getCount();
        Assert.isTrue(intCount == 0, "未完全删除所有记录");
    }

    /**
     * 获取随机 修改dto
     *
     * @return {@link TableBoxerBasicUpdateDTO }
     */
    public static TableBoxerBasicUpdateDTO getRandomUpdateDTO() {
        TableBoxerBasicUpdateDTO model = new TableBoxerBasicUpdateDTO();
        return model;
    }

    /**
     * 获取随机 创建dto
     *
     * @return {@link TableBoxerBasicCreateDTO }
     */
    public static TableBoxerBasicCreateDTO getRandomCreateDTO() {
        TableBoxerBasicCreateDTO model = new TableBoxerBasicCreateDTO();
        return model;
    }

    /**
     * 获取查询条件
     *
     * @return {@link TableBoxerBasicRetrieveDTO }
     */
    private static TableBoxerBasicRetrieveDTO getTableBoxerBasicRetrieveDTO() {
        // 构建查询条件
        TableBoxerBasicRetrieveDTO tableBoxerBasicRetrieveDTO = new TableBoxerBasicRetrieveDTO();
        tableBoxerBasicRetrieveDTO.setSort(Sort.by(STR_ID_FILED_NAME));
        return tableBoxerBasicRetrieveDTO;
    }

    /**
     * 获取主键id内容
     *
     * @param objBean 对象
     * @return {@link Integer }
     */
    private Integer getIdValue(Object objBean) {
        return StrongUtils.getIntByFiledName(objBean, STR_ID_FILED_NAME);
    }

    /**
     * 获取测试用文本字段内容
     *
     * @param objBean 对象
     * @return {@link Integer }
     */
    private String getTextValue(Object objBean) {
        return StrongUtils.getStringByFiledName(objBean, STR_TEXT_FILED_NAME);
    }

}