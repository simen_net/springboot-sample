package com.strong.sample.table;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import com.alibaba.druid.pool.DruidDataSource;
import com.strong.Faker;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.utils.MockMvcLoginUtils;
import com.strong.utils.H2Utils;
import com.strong.utils.StrongUtils;
import com.strong.utils.security.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.io.File;

@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SpringbootTests {

    /**
     * 数据库名称
     */
    @Value("${strong.database-name}")
    protected String databaseName;

    /**
     * 初始化时建立物理连接的个数
     */
    @Value("${spring.datasource.druid.initialSize}")
    protected Integer initialSize;

    /**
     * 最大连接池数量
     */
    @Value("${spring.datasource.druid.maxActive}")
    protected Integer maxActive;

    /**
     * 数据源
     */
    @Autowired
    protected DataSource dataSource;

    /**
     * 模式登录测试工具
     */
    @Autowired
    private MockMvcLoginUtils mockMvcLoginUtils;

    @BeforeAll
    static void beforeAll() {
        String strStaticPath = StrongUtils.getLocalStaticPath();
        for (File file : FileUtil.loopFiles(strStaticPath + File.separator + "log")) {
            FileUtil.del(file);
        }

        Faker.initialize();
        H2Utils.startH2Server();
    }

    @AfterAll
    static void afterAll() {
        H2Utils.stopH2Server();
    }

    /**
     * admin用户的token
     */
    protected static String STR_TOKEN_ADMIN;

    /**
     * user用户的token
     */
    protected static String STR_TOKEN_USER;

    @Test
    @Order(0)
    @DisplayName("测试系统加载过程")
    void contextLoads() {
        log.info("\n数据库名：{}\n项目UUID：{}\n公钥：{}\n私钥：{}",
                databaseName, SecurityUtils.STR_UUID,
                Base64.encode(SecurityUtils.BYTES_PUBLIC_KEY),
                Base64.encode(SecurityUtils.BYTES_PRIVATE_KEY)
        );

        DruidDataSource druidDataSource = (DruidDataSource) dataSource;

        Assert.isTrue(initialSize == druidDataSource.getInitialSize(),
                "【初始化时建立物理连接的个数】与yaml中的配置不一致");
        Assert.isTrue(maxActive == druidDataSource.getMaxActive(),
                "【最大连接池数量】与yaml中的配置不一致");

        System.out.println("===================================");
        System.out.println("druidDataSource 数据源驱动名称：" + druidDataSource.getDriverClassName());
        System.out.println("druidDataSource 数据源最大连接数：" + druidDataSource.getMaxActive());
        System.out.println("druidDataSource 数据源初始化连接数：" + druidDataSource.getInitialSize());
        System.out.println("druidDataSource 数据库用户名：" + druidDataSource.getUsername());
        System.out.println("druidDataSource 数据库密码：" + druidDataSource.getPassword());
        System.out.println("===================================");
    }

    /**
     * 测试执行登录
     */
    protected void postLogin() {
        try {
            STR_TOKEN_ADMIN = mockMvcLoginUtils.postLoginAdmin();
            STR_TOKEN_USER = mockMvcLoginUtils.postLoginUser();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
