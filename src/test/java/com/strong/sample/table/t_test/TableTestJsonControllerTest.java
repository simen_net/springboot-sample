package com.strong.sample.table.t_test;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.strong.config.GlobalExceptionHandler;
import com.strong.sample.SpringbootSampleApplication;
import com.strong.sample.table.SpringbootTests;
import com.strong.sample.table.t_test.jpa.TableTestDAO;
import com.strong.sample.table.t_test.jpa.TableTestDO;
import com.strong.sample.table.t_test.model.TableTestCreateDTO;
import com.strong.sample.table.t_test.model.TableTestRetrieveDTO;
import com.strong.sample.table.t_test.model.TableTestUpdateDTO;
import com.strong.sample.table.t_test.model.TableTestVO;
import com.strong.sample.utils.MockMvcUtils;
import com.strong.utils.JSON;
import com.strong.utils.StrongUtils;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.mvc.pojo.view.vab.VabList;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.strong.sample.utils.MockMatcher.isReplyString;
import static com.strong.utils.mvc.MvcConstants.*;

/**
 * 测试表json控制器测试
 *
 * @author simen
 * @date 2024/08/20
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TableTestJsonControllerTest extends SpringbootTests {

    /**
     * 测试用URL
     */
    public final static String STR_URL_RECORD_VIEW = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_RECORD_VIEW, "%s");
    public final static String STR_URL_CREATE_ACTION = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_CREATE_ACTION);
    public final static String STR_URL_UPDATE_ACTION = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_UPDATE_ACTION);
    public final static String STR_URL_UPDATE_VIEW = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_UPDATE_VIEW, "%s");
    public final static String STR_URL_DELETE_ACTION = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_DELETE_ACTION, "%s");
    public final static String STR_URL_LIST_VIEW = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_LIST_VIEW);
    public final static String STR_URL_PAGE_VIEW = StrUtil.join("/", TableTestConstants.TABLE_ENTITY, URL_PAGE_VIEW);

    /**
     * 拟测试记录数量
     */
    private static final Integer INT_RECORD_COUNT = RandomUtil.randomInt(100, 999);

    /**
     * 主键ID字段名
     */
    private static final String STR_ID_FILED_NAME = TableTestConstants.TEST_ID_ENTITY;

    /**
     * 测试用文本字段名
     */
    private static final String STR_TEXT_FILED_NAME = TableTestConstants.TEST_STR_ENTITY;

    /**
     * 模拟mvc对象
     */
    @Autowired
    public MockMvc mockMvc;

    /**
     * 模拟mvc工具对象
     */
    @Autowired
    public MockMvcUtils mockMvcUtils;

    /**
     * 数据库操作类
     */
    @Autowired
    TableTestDAO tableTestDAO;

    @Test
    @Order(1)
    @DisplayName("测试删除数据")
    @Tag("BASIC_TEST")
    public void testDeleteData() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableTestDAO.getCount();
        // 如果初始记录为空，则先执行添加操作
        if (intActualRecordCount == 0) {
            testAddData();
            intActualRecordCount = tableTestDAO.getCount();
        }
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        // 删除所有记录
        deleteRecord(postListReplyVO(STR_URL_LIST_VIEW, getTableTestRetrieveDTO()).getList());
    }

    @Test
    @Order(2)
    @DisplayName("测试添加数据")
    @Tag("BASIC_TEST")
    public void testAddData() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intOriginalRecordCount = tableTestDAO.getCount();

        // 循环添加记录
        for (int i = 0; i < INT_RECORD_COUNT; i++) {
            // 随机获取DTO用于生成创建请求的JSON
            TableTestCreateDTO tableTestCreateDTO = TableTestServiceTest.getRandomCreateDTO();

            {
                // STR_URL_CREATE_ACTION 权限测试
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_CREATE_ACTION, null, tableTestCreateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_CREATE_ACTION, STR_TOKEN_USER, tableTestCreateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }

            // 发送添加请求
            TableTestVO tableTestVO = postReturnReplyVO(STR_URL_CREATE_ACTION, tableTestCreateDTO);
            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(tableTestCreateDTO, tableTestVO,
                    ArrayUtil.append(TableTestVO.STRS_EXCLUSION_PROPERTIES, STR_ID_FILED_NAME));
            // 断言排除字段为空
            assertField(tableTestVO);
        }

        // 通过DAO获取实际记录数
        int intActualRecordCount = tableTestDAO.getCount();
        // 预期记录数量
        int intPredictRecordCount = intOriginalRecordCount + INT_RECORD_COUNT;
        Assert.isTrue(intPredictRecordCount == intActualRecordCount,
                "添加记录数量【{}】与实际数量【{}】不一致", intPredictRecordCount, intActualRecordCount);
    }

    @Test
    @Order(3)
    @DisplayName("测试条件查询所有记录")
    @Tag("BASIC_TEST")
    public void testQueryAll() throws Exception {
        postLogin();

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableTestDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        String strLike = "a";

        TableTestRetrieveDTO tableTestRetrieveDTO = getTableTestRetrieveDTO();

        {
            // STR_URL_LIST_VIEW 权限测试
            // 测试匿名用户无权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_LIST_VIEW, null, tableTestRetrieveDTO, ReplyEnum.ERROR_SERVER_ERROR)
                    .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            // 测试普通用户有权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_LIST_VIEW, STR_TOKEN_USER, tableTestRetrieveDTO, ReplyEnum.SUCCESS_RETURN_DATA);
        }

        { // ============ 完整查询记录测试 ============
            // 发送获取完整记录请求
            List<TableTestVO> listVO = postListReplyVO(STR_URL_LIST_VIEW, tableTestRetrieveDTO).getList();
            Assert.notEmpty(listVO, "返回记录空");
            Assert.equals(listVO.size(), intActualRecordCount,
                    "返回的记录数[{}]与实际记录数[{}]不一致", listVO.size(), intActualRecordCount);
            // 断言对象字段符合要求
            assertField(listVO);
        }

        { // ============ 条件查询记录测试 ============
            tableTestRetrieveDTO.addSearch(STR_TEXT_FILED_NAME, String.format("%%%s%%", strLike));
            // 发送获取完整记录请求
            List<TableTestVO> listVO = postListReplyVO(STR_URL_LIST_VIEW, tableTestRetrieveDTO).getList();
            Assert.notEmpty(listVO, "返回记录空");
            Assert.isTrue(listVO.size() <= intActualRecordCount,
                    "返回的记录数[{}] 大于 实际记录数[{}]", listVO.size(), intActualRecordCount);
            for (TableTestVO tableTestVO : listVO) {
                // TODO VO记录可能因未实例化造成无法获取需要的值
                String strTextValue = getTextValue(tableTestVO);
                Assert.isTrue(StrUtil.contains(strTextValue, strLike),
                        "查询记录【{}】不匹配【{}】：", strTextValue, strLike);
            }
            // 断言对象字段符合要求
            assertField(listVO);
        }
    }

    @Test
    @Order(4)
    @DisplayName("测试分页查询")
    @Tag("BASIC_TEST")
    public void testQueryPage() throws Exception {
        postLogin();

        // 构造查询条件对象
        TableTestRetrieveDTO tableTestRetrieveDTO = getTableTestRetrieveDTO();
        tableTestRetrieveDTO.setIntPageSize(RandomUtil.randomInt(5, 15));

        // 通过DAO获取实际记录数
        Integer intActualRecordCount = tableTestDAO.getCount();
        Assert.isTrue(intActualRecordCount > 0, "数据库记录为空，无法进行测试");

        {
            // STR_URL_PAGE_VIEW 权限测试
            // 测试匿名用户无权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_PAGE_VIEW, null, tableTestRetrieveDTO, ReplyEnum.ERROR_SERVER_ERROR)
                    .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            // 测试普通用户有权限
            mockMvcUtils.postReturnResultReplyVO(STR_URL_PAGE_VIEW, STR_TOKEN_USER, tableTestRetrieveDTO, ReplyEnum.SUCCESS_RETURN_DATA);
        }

        // 页码总数
        int intPageCount = intActualRecordCount / tableTestRetrieveDTO.getIntPageSize();
        // 分页后的余数
        int intPageRemainder = intActualRecordCount % tableTestRetrieveDTO.getIntPageSize();
        // 如果分页余数大于0，则页码总数加1
        if (intPageRemainder > 0) {
            intPageCount++;
        }

        // 已存在的ID队列，用于验证记录是否重复
        List<Integer> listExistId = new ArrayList<>();
        // 模拟VAB从1开始查询所有分页
        for (int i = 1; i <= intPageCount; i++) {
            // 该分页预期的记录数
            Integer intExpectedNumber = tableTestRetrieveDTO.getIntPageSize();
            // 如果是否到最后一页，且余数大于0，则预期记录数为分页后的余数
            if (i == intPageCount && intPageRemainder > 0) {
                intExpectedNumber = intPageRemainder;
            }

            // 设置查询页码
            tableTestRetrieveDTO.setIntPageNo(i - 1);

            // 发送分页查询请求
            VabList<TableTestVO> vabList = postListReplyVO(STR_URL_PAGE_VIEW, tableTestRetrieveDTO);
            List<TableTestVO> listVO = vabList.getList();
            int intTotalCount = vabList.getTotal().intValue();

            System.out.printf("总记录数【%d】当前页【%d/%d】本页记录数【预期%d/实际%s】\n",
                    intTotalCount, tableTestRetrieveDTO.getIntPageNo() + 1,
                    intPageCount, intExpectedNumber, listVO.size());

            Assert.notEmpty(listVO, "返回记录空");
            Assert.isTrue(intActualRecordCount == intTotalCount,
                    "实际记录数量【{}】与查询到的总记录数【{}】不一致", intActualRecordCount, intTotalCount);
            Assert.equals(listVO.size(), intExpectedNumber,
                    "返回的记录数[{}]与页码[{}]不一致", listVO.size(), intExpectedNumber);

            // 断言对象字段符合要求
            assertField(listVO);

            // 断言分页查询的记录不重复
            assertNoRepeat(listVO, listExistId);
        }
    }

    @Test
    @Order(5)
    @DisplayName("测试查询单条记录")
    @Tag("BASIC_TEST")
    public void testQueryRecord() throws Exception {
        postLogin();

        // 通过DAO获取所有记录
        List<TableTestDO> listAllRecord = tableTestDAO.findAll();
        System.out.println(listAllRecord.size());

        // 循环获取单条记录
        for (TableTestDO tableTestDO : listAllRecord) {
            String strId = getIdValue(tableTestDO).toString();
            String strUrlRecordId = String.format(STR_URL_RECORD_VIEW, strId);
            String strUrlUpdateId = String.format(STR_URL_UPDATE_VIEW, strId);

            {
                // STR_URL_RECORD_VIEW
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(strUrlRecordId, null, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户有权限
                mockMvcUtils.postReturnResultReplyVO(strUrlRecordId, STR_TOKEN_USER, null, ReplyEnum.SUCCESS_RETURN_DATA);
            }
            // 发送获取单条记录请求
            TableTestVO recordView = mockMvcUtils.postReturnReplyVO(strUrlRecordId, STR_TOKEN_ADMIN, null,
                    ReplyEnum.SUCCESS_RETURN_DATA, TableTestVO.class, StrongUtils.getFiledNames(TableTestVO.class));
            StrongUtils.compareBean(tableTestDO, recordView);

            {
                // STR_URL_UPDATE_VIEW
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(strUrlUpdateId, null, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户有权限
                mockMvcUtils.postReturnResultReplyVO(strUrlUpdateId, STR_TOKEN_USER, null, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }
            // 发送获取单条记录请求
            TableTestVO updateView = mockMvcUtils.postReturnReplyVO(strUrlUpdateId, STR_TOKEN_ADMIN, null,
                    ReplyEnum.SUCCESS_RETURN_DATA, TableTestVO.class, StrongUtils.getFiledNames(TableTestVO.class));
            StrongUtils.compareBean(tableTestDO, updateView);
        }
    }

    @Test
    @Order(6)
    @DisplayName("测试修改数据")
    @Tag("BASIC_TEST")
    public void testUpdateData() throws Exception {
        postLogin();

        // 通过DAO获取所有记录
        List<TableTestDO> listAllRecord = tableTestDAO.findAll();

        // 循环修改记录
        for (TableTestDO tableTestDO : listAllRecord) {
            // 随机获取DTO用于生成修改请求的JSON
            TableTestUpdateDTO tableTestUpdateDTO = TableTestServiceTest.getRandomUpdateDTO();
            tableTestUpdateDTO.setTestId(getIdValue(tableTestDO));

            {
                // STR_URL_CREATE_ACTION 权限测试
                // 测试匿名用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_UPDATE_ACTION, null, tableTestUpdateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
                // 测试普通用户无权限
                mockMvcUtils.postReturnResultReplyVO(STR_URL_UPDATE_ACTION, STR_TOKEN_USER, tableTestUpdateDTO, ReplyEnum.ERROR_SERVER_ERROR)
                        .andExpect(isReplyString(GlobalExceptionHandler.MSG_ACCESS_DENIED_EXCEPTION));
            }
            // 发送修改请求
            TableTestVO tableTestVO = postReturnReplyVO(STR_URL_UPDATE_ACTION, tableTestUpdateDTO);

            // 比较DTO和VO的内容是否一致（此处需要忽略部分字段）
            StrongUtils.compareBean(tableTestUpdateDTO, tableTestVO, TableTestVO.STRS_EXCLUSION_PROPERTIES);

            // 断言排除字段为空
            assertField(tableTestVO);
        }
    }

    /**
     * 删除所有记录
     *
     * @throws Exception 异常
     */
    private void deleteRecord(List<TableTestVO> listTableTestVO) throws Exception {
        Assert.notEmpty(listTableTestVO, "待删除记录队列为空");
        int intRecordCount = listTableTestVO.size();
        // 遍历一半记录，逐条删除
        int intSplit = intRecordCount / 2;
        for (int i = 0; i < intSplit; i++) {
            TableTestVO tableTestVO = listTableTestVO.get(i);
            Integer intId = getIdValue(tableTestVO);

            postReturnReplyVO(String.format(STR_URL_DELETE_ACTION, intId), null);

            // 根据ID查询DO，断言其为空
            Optional<TableTestDO> optional = tableTestDAO.findById(intId);
            Assert.isTrue(optional.isEmpty(), "已删除的DO对象依旧存在：【ID：{}】", intId);

            // 断言记录数
            Integer intRecordCountTemp = tableTestDAO.getCount();
            intRecordCount--;
            Assert.equals(intRecordCount, intRecordCountTemp,
                    "预期记录数{} 不等于当前记录数{}", intRecordCount, intRecordCountTemp);
        }

        // 遍历后半记录，合并删除
        List<Integer> listID = new ArrayList<>();
        for (int i = intSplit; i < intSplit + intRecordCount; i++) {
            TableTestVO tableTestVO = listTableTestVO.get(i);
            listID.add(getIdValue(tableTestVO));
        }

        Integer[] intsID = ArrayUtil.toArray(listID, Integer.class);
        String strIds = ArrayUtil.join(intsID, ",");

        postReturnReplyVO(String.format(STR_URL_DELETE_ACTION, strIds), null);

        List<TableTestDO> listTableTestDO = tableTestDAO.findAllByIdIn(listID);
        Assert.isTrue(CollUtil.isEmpty(listTableTestDO), "删除的记录依旧存在{}", strIds);

        // 断言记录数
        Integer intRecordCountTemp = tableTestDAO.getCount();
        Assert.equals(0, intRecordCountTemp,
                "预期记录数{} 不等于当前记录数{}", 0, intRecordCountTemp);
    }

    /**
     * 获取查询条件
     *
     * @return {@link TableTestRetrieveDTO }
     */
    private TableTestRetrieveDTO getTableTestRetrieveDTO() {
        // 构建查询条件
        TableTestRetrieveDTO tableTestRetrieveDTO = new TableTestRetrieveDTO();
        tableTestRetrieveDTO.setSort(Sort.by(STR_TEXT_FILED_NAME));
        return tableTestRetrieveDTO;
    }

    /**
     * 获取post提交返回的视图
     *
     * @param strURL   URL地址
     * @param objModel 提交的数据模型
     * @return {@link TableTestVO }
     * @throws Exception 异常
     */
    private TableTestVO postReturnReplyVO(String strURL, Object objModel) throws Exception {
        return mockMvcUtils.postReturnReplyVO(strURL, STR_TOKEN_ADMIN, objModel, TableTestVO.class);
    }

    /**
     * 获取post提交返回的列表视图
     *
     * @param strURL   URL地址
     * @param objModel 提交的数据模型
     * @return {@link VabList }<{@link TableTestVO }>
     * @throws Exception 异常
     */
    private VabList<TableTestVO> postListReplyVO(String strURL, Object objModel) throws Exception {
        return mockMvcUtils.postReturnListReplyVO(strURL, STR_TOKEN_ADMIN, objModel, TableTestVO.class);
    }

    /**
     * 获取主键id内容
     *
     * @param objBean obj豆
     * @return {@link Integer }
     */
    private Integer getIdValue(Object objBean) {
        return StrongUtils.getIntByFiledName(objBean, STR_ID_FILED_NAME);
    }

    /**
     * 获取测试用文本字段内容
     *
     * @param objBean obj豆
     * @return {@link Integer }
     */
    private String getTextValue(Object objBean) {
        return StrongUtils.getStringByFiledName(objBean, STR_TEXT_FILED_NAME);
    }

    /**
     * 遍历队列 断言对象字段符合要求
     *
     * @param listVO 签证官列表
     */
    private void assertField(List<TableTestVO> listVO) {
        Integer intId = null;
        for (TableTestVO tableTestVO : listVO) {
            Integer intIdTemp = getIdValue(tableTestVO);
            if (ObjUtil.isNull(intId)) {
                intId = intIdTemp;
            } else {
                Assert.isTrue(intIdTemp >= intId,
                        "当前记录【{}】与前一条记录【{}】,排序规则不匹配", intIdTemp, intId);
            }
            assertField(tableTestVO);
        }
    }

    /**
     * 断言对象字段符合要求
     *
     * @param tableTestVO 签证官列表
     */
    private void assertField(TableTestVO tableTestVO) {
        // 遍历断言tableTestVO的排除字段都为空
        for (String strProperty : TableTestVO.STRS_EXCLUSION_PROPERTIES) {
            Assert.isNull(BeanUtil.getProperty(tableTestVO, strProperty),
                    "{}字段非空\n{}", strProperty, JSON.toJSONString(tableTestVO));
        }
    }

    /**
     * 断言分页查询的记录不重复
     *
     * @param listVO      VO队列
     * @param listExistId 已存在的ID队列
     */
    private void assertNoRepeat(List<TableTestVO> listVO, List<Integer> listExistId) {
        // 对比ID队列，断言分页的所有记录为唯一
        for (TableTestVO tableTestVO : listVO) {
            Integer intId = getIdValue(tableTestVO);
            Assert.isTrue(!CollUtil.contains(listExistId, intId), "ID【{}】重复取出", intId);

            // 断言排除字段为空
            assertField(tableTestVO);
        }

        // 将单条记录的ID加入到队列，用于对比是否重复
        listExistId.addAll(listVO.stream().map(this::getIdValue).toList());
    }

}