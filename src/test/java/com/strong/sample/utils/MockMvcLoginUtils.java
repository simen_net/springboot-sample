package com.strong.sample.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.strong.utils.JSON;
import com.strong.utils.mvc.pojo.view.ReplyEnum;
import com.strong.utils.security.jwt.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.strong.sample.utils.MockMatcher.isReplyModel;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Slf4j
@Component
public class MockMvcLoginUtils {

    /**
     * 普通用户 用户名
     */
    public final static String STR_USER_NAME = "simen";

    /**
     * 普通用户 密码
     */
    public final static String STR_USER_PASSWORD = "123456";

    /**
     * 管理员 用户名
     */
    public final static String STR_ADMIN_NAME = "admin";

    /**
     * 管理员 密码
     */
    public final static String STR_ADMIN_PASSWORD = "123456";

    /**
     * 登出URL地址
     */
    public static final String URL_LOGOUT = "/logout";

    @Autowired
    public MockMvcUtils mockMvcUtils;

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    /**
     * 登录管理员
     *
     * @return {@link String }
     * @throws Exception 异常
     */
    public String postLoginAdmin() throws Exception {
        return postLoginReturnToken(STR_ADMIN_NAME, STR_ADMIN_PASSWORD);
    }

    /**
     * 登录普通用户
     *
     * @return {@link String }
     * @throws Exception 异常
     */
    public String postLoginUser() throws Exception {
        return postLoginReturnToken(STR_USER_NAME, STR_USER_PASSWORD);
    }

    /**
     * post登出
     *
     * @throws Exception 异常
     */
    public void postLogout() throws Exception {
        mockMvcUtils.postReturnResult(URL_LOGOUT, null, null);
    }

    /**
     * post登录并返回token
     *
     * @param strName     str名字
     * @param strPassword str密码
     * @throws Exception 异常
     */
    public String postLoginReturnToken(String strName, String strPassword) throws Exception {
        // 测试登录成功
        MvcResult resultLogin = mockMvc.perform(formLogin().user(strName).password(strPassword))
                .andExpect(isReplyModel(ReplyEnum.SUCCESS_RETURN_DATA.getCode(), "token", "date"))
                .andExpect(authenticated())
                .andExpect(jsonPath("$.data.token").isString())
                .andReturn();

        String strJson = resultLogin.getResponse().getContentAsString();
        JsonNode jsonNode = JSON.parseJsonNode(strJson);

        // 获取返回的token并校验
        String strToken = jsonNode.get("data").get("token").asText();
        jwtTokenUtils.verifyToken(strToken);
        return strToken;
    }
}
