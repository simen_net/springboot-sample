//package com.strong.sample;
//
//import cn.hutool.core.lang.Assert;
//import cn.hutool.core.util.ArrayUtil;
//import cn.hutool.core.util.RandomUtil;
//import com.strong.sample.table.SpringbootTests;
//import com.strong.sample.utils.MockMvcLoginUtils;
//import com.strong.utils.exception.StrongRuntimeException;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static com.strong.sample.utils.MockMvcLoginUtils.STR_USER_NAME;
//import static com.strong.sample.utils.MockMvcLoginUtils.STR_USER_PASSWORD;
//import static com.strong.sample.utils.MockMvcUtils.*;
//import static com.strong.system.constroller.SystemJsonController.*;
//import static org.junit.Assert.assertThrows;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
//import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
// * Springboot样例应用程序测试
// *
// * @author simen
// * @date 2024/08/03
// */
//@Slf4j
//@AutoConfigureMockMvc
//@SpringBootTest(classes = SpringbootSampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//public class SpringbootSampleApplicationTests extends SpringbootTests {
//
//    @Autowired
//    public MockMvc mockMvc;
//
//    @Autowired
//    private MockMvcLoginUtils mockMvcLoginUtils;
//
//    @Test
//    @Order(1)
//    @DisplayName("测试登录失败等异常")
//    public void testLoginFailure() throws Exception {
//        // 测试登录失败
//        mockMvc.perform(formLogin()
//                        .user(STR_USER_NAME)
//                        .password(STR_USER_PASSWORD + RandomUtil.randomString(5)))
//                .andExpect(unauthenticated());
//
//        // 验证抛出自定义异常
//        mockMvc.perform(getJsonRequestBuilder(URL_PERSON_JSON_ERROR, null, null))
//                .andExpect(status().isBadRequest())
//                .andExpect(result -> Assert.isTrue(result.getResolvedException() instanceof StrongRuntimeException));
//
//        // 验证错误JWT代码返回异常
//        for (String strUrl : URLS_TEST_200) {
//            assertThrows(BadCredentialsException.class,
//                    () -> mockMvc.perform(getJsonRequestBuilder(strUrl, RandomUtil.randomString(10), null)));
//        }
//    }
//
//    @Test
//    @Order(2)
//    @DisplayName("测试匿名用户")
//    public void testAnonymousUsers() throws Exception {
//        // 登出
//        mockMvcLoginUtils.postLogout();
//
//        // 验证有权限访问
//        for (String strUrl : URLS_ANONYMOUS_TEST) {
//            testResultByReplyVO_200(mockMvc.perform(
//                    getJsonRequestBuilder(strUrl, null, null).with(anonymous())));
//        }
//
//        // 验证无权限访问
//        for (String strUrl : URLS_TEST_200) {
//            if (!ArrayUtil.contains(URLS_ANONYMOUS_TEST, strUrl)) {
//                testResultByReplyVO_402(mockMvc.perform(
//                        getJsonRequestBuilder(strUrl, null, null).with(anonymous())));
//            }
//        }
//    }
//
//    @Test
//    @Order(3)
//    @DisplayName("测试管理员登录")
//    public void testAdminLogin() throws Exception {
//        // 登录管理员
//        String strToken = mockMvcLoginUtils.postLoginAdmin();
//
//        // 验证有权限访问
//        for (String strUrl : URLS_ADMIN_TEST) {
//            testResultByReplyVO_200(mockMvc.perform(getJsonRequestBuilder(strUrl, strToken, null)));
//        }
//
//        // 验证无权限访问
//        for (String strUrl : URLS_TEST_200) {
//            if (!ArrayUtil.contains(URLS_ADMIN_TEST, strUrl)) {
//                testResultByReplyVO_402(mockMvc.perform(getJsonRequestBuilder(strUrl, null, null)));
//            }
//        }
//    }
//
//    @Test
//    @Order(4)
//    @DisplayName("测试普通用户登录")
//    public void testUserLogin() throws Exception {
//        // 登录普通用户
//        String strToken = mockMvcLoginUtils.postLoginUser();
//
//        // 验证有权限访问
//        for (String strUrl : URLS_USER_TEST) {
//            testResultByReplyVO_200(mockMvc.perform(getJsonRequestBuilder(strUrl, strToken, null)));
//        }
//
//        // 验证无权限访问
//        for (String strUrl : URLS_TEST_200) {
//            if (!ArrayUtil.contains(URLS_USER_TEST, strUrl)) {
//                testResultByReplyVO_402(mockMvc.perform(getJsonRequestBuilder(strUrl, null, null)));
//            }
//        }
//    }
//
//
//}
