create table PUBLIC.T_TEST
(
    TEST_ID       INTEGER auto_increment primary key unique,
    TEST_STR      CHARACTER VARYING(32),
    TEST_INT      INTEGER,
    TEST_DATE     DATE,
    TEST_DATETIME TIMESTAMP,
    TEST_FLOAT    DOUBLE PRECISION,
    TEST_BOOL     BOOLEAN,
    TEST_TEXT     CHARACTER VARYING,
    TEST_DOUBLE   DOUBLE PRECISION
);

comment on table PUBLIC.T_TEST is '测试表';

comment on column PUBLIC.T_TEST.TEST_ID is '主键字段';

comment on column PUBLIC.T_TEST.TEST_STR is '字符串字段';

comment on column PUBLIC.T_TEST.TEST_INT is '整数字段';

comment on column PUBLIC.T_TEST.TEST_DATE is '日期字段';

comment on column PUBLIC.T_TEST.TEST_DATETIME is '日期时间字段';

comment on column PUBLIC.T_TEST.TEST_FLOAT is '浮点字段';

comment on column PUBLIC.T_TEST.TEST_BOOL is '布尔字段';

comment on column PUBLIC.T_TEST.TEST_TEXT is '长文本字段';

comment on column PUBLIC.T_TEST.TEST_DOUBLE is '双精度字段';

